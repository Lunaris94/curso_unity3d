﻿using System;

namespace Leccion10_Ejercicio05
{
    /*
     * Realizar un programa que lea los lados de n triángulos, e informar:
     * a) De cada uno de ellos, qué tipo de triángulo es: equilátero (tres lados iguales), isósceles (dos lados iguales), o escaleno (ningún lado igual)
     * b) Cantidad de triángulos de cada tipo.
     * c) Tipo de triángulo que posee menor cantidad. 
     */
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("¿Cuántos triángulos va a introducir?");
            string linea = Console.ReadLine();
            int n = Int32.Parse(linea);

            int lado1, lado2, lado3;
            int equilateros = 0;
            int isosceles = 0;
            int escalenos = 0;
            for(int i=0; i < n; i++)
            {
                Console.WriteLine("Introduzca primer lado");
                linea = Console.ReadLine();
                lado1 = Int32.Parse(linea);
                Console.WriteLine("Introduzca segundo lado");
                linea = Console.ReadLine();
                lado2 = Int32.Parse(linea);
                Console.WriteLine("Introduzca tercer lado");
                linea = Console.ReadLine();
                lado3 = Int32.Parse(linea);

                if (lado1 == lado2 && lado1 == lado3)
                {
                    Console.WriteLine("El triángulo es equilátero");
                    equilateros = equilateros + 1;
                }else if (lado1 == lado2 || lado1 == lado3 || lado2 == lado3)
                {
                    Console.WriteLine("El triángulo es isósceles");
                    isosceles = isosceles + 1;
                }else if (lado1 != lado2 && lado1 != lado3 && lado2 != lado3)
                {
                    Console.WriteLine("El triángulo es escaleno");
                    escalenos = escalenos + 1;
                }
            }
            Console.WriteLine();
            Console.WriteLine("Triángulos equiláteros: " + equilateros);
            Console.WriteLine("Triángulos isósceles: "+ isosceles);
            Console.WriteLine("Triángulos escalenos: " + escalenos);

            if (equilateros < isosceles)
            {
                if (equilateros < escalenos)
                {
                    Console.WriteLine("Hay menos triángulos equiláteros");
                }
            }
            else if (isosceles < escalenos)
            {
                Console.WriteLine("Hay menos triángulos isósceles");
            }
            else
            {
                Console.WriteLine("Hay menos triángulos escalenos");
            }
        }  
    }
}
