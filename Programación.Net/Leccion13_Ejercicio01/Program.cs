﻿using System;

namespace Leccion13_Ejercicio01
{
    /*
     * Confeccionar una clase que represente un empleado. Definir como atributos su nombre y su sueldo. Confeccionar los métodos para la carga, otro para imprimir sus datos y por último uno que imprima un mensaje si debe pagar impuestos (si el sueldo supera a 3000)
     */
    class Empleado
    {
        string nombre;
        int sueldo;
        static void Main(string[] args)
        {
            Empleado empleado1 = new Empleado();
            empleado1.IntroducirDatos();
            empleado1.MostrarDatos();
            empleado1.PagarImpuestos();
        }

        public void IntroducirDatos()
        {
            Console.WriteLine("Introduzca nombre");
            nombre = Console.ReadLine();

            Console.WriteLine("Introduzca sueldo");
            string linea;
            linea = Console.ReadLine();
            sueldo = Int32.Parse(linea);
        }

        public void MostrarDatos()
        {
            Console.WriteLine("Datos personales: ");
            Console.WriteLine(nombre + " tiene un sueldo de: " + sueldo + " euros");
        }
        public void PagarImpuestos()
        {
            if (sueldo > 3000)
            {
                Console.WriteLine("Su renta supera los 3000 euros");
                Console.WriteLine("Debe pagar impuestos");
            }
        }
    }
}
