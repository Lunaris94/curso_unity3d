using System;

public class EjercicioArrays 
{
	public static void Main()
	{
		EjercicioArraysAtaque();
		/*EjercicioArrays2
		Generar un nuevo array que tenga tantos enemigos NO cercanos
		en el siguiente formato: 
		"Enemigo 0: 3.2 puntos" "Enemigo 3: 5.0 puntos"
		
		EjercicioArrays3
		Crear una funcion que calcule el ataque total de los que 
		tengan ataque >3. Este tope debe pasarse por parámetro
		
		EjercicioArrays4
		Crear otra funcion que devuelva el ataque máximo del array
		
		EjercicioArrays5
		Crear una funcion que calcule la media de todos los ataques
		
		EjercicioArrays6
		Crear una funcion que calcule el minimo de los ataques de los enemigos CERCANOS.
		
		EjercicioArrays7
		Crear una función que devuelva el 

		*Arrays ataques:
			3.2	False
			1.7	True
			7.4	True
			5.0	False
			7.1	True
			4.8	True 
		*/
	}
	public static void EjercicioArraysAtaque()
	//Crear una función que calcule la suma de los ataques enemigos cercanos
	{
		float[] ataques = {3.2f, 1.7f, 2.4f, 5.0f, 7.1f, 4.8f};
		bool[] siCercanos ={false, true, true, false, true, true};
		bool[] siCercanosJug2 ={true, true, false, false, true, false};
		
		float totalAtaques = CalAtaqueTotalCeranos( ataques, siCercanos);
		float totalAtaqJug2 = CalAtaqueTotalCeranos( ataques, siCercanosJug2);
		
		Console.WriteLine("Total = " + totalAtaques);
		Console.WriteLine("Total = " + totalAtaqJug2);
		
		//Ejercicio 3:
		float totalMay_3 = CalAtaqueMayoresQueTope (ataques, 3f);
		Console.WriteLine("Total de > 3 = " + totalMay_3);
		
		//Ejercicio 4:
		Console.WriteLine("El valor máximo es " + CalcAtaqueMax(ataques));
		
		//Ejercicio 5:
		Console.WriteLine("La media de ataques es " + CalcAtaqueMedia(ataques));
		
		//Ejercicio 6:
		Console.WriteLine("El valor minimo es " + CalcMinCerc (ataques, siCercanos));
	}
	
	public static float CalAtaqueTotalCeranos(float[] ataques, bool[] siCercanos)
	{
		
		float total = 0; // Iniciar variables!!
		
		// Para cuando los ataques que sumen sean los verdaderos
		for (int i = 0; i < ataques.Length; i = i + 1)
		{
			Console.WriteLine("Valor de " + i + "es " + ataques[i]);
			if (siCercanos[i])
			{
			total = total + ataques[i];
			}
		}
		return total;
	}
	public static float CalAtaqueMayoresQueTope(float[] ataques, float tope)
	{
		float total = 0;
		
		for (int i = 0; i < ataques.Length; i = i + 1)
		{
			if (ataques[i] > tope)
			{
			total = total + ataques[i];
			}
		}
		return total;
	}
	public static float CalcAtaqueMax(float[] ataques)
	{
		//decimos lo que vamos a devolver (aqui a lo que queremos llamar)
		float valorMax = ataques [0];
		//primero ponemos la casilla que queremos que guarde con el valor 
		//maximo, que de momento, al empezar, es el primer valor
		
		//después le decimos que queremos que recorra el array, desde el
		//principio, hasta el principio menos la longitud del array, de 
		// de uno al siguiente sumando uno; aqui 
		for (int i = 0; i < ataques.Length; i = i + 1)
		{
			if ( ataques [i] > valorMax )
			{
				valorMax = ataques[i];
			}
			//despues le decimos que nos compare el valor de ataques en el
			//momento actual, por eso es ataques[i], con el valor maximo
			//guardado antes, de forma que si es true, es decir, que si es
			//mayor, le decimos que nos cambie el valorMax por el valor actual
		}
		return valorMax;
		//finalmente, le decimos que nos devuelva el valorMax final
	}
	public static float CalcAtaqueMedia(float[] ataques)
	{
		float sumaTotal = 0;
		
		for (int i = 0; i < ataques.Length; i = i + 1)
		{
			sumaTotal = sumaTotal + ataques[i];
		}
		return sumaTotal/ataques.Length;
	}
	public static float CalcMinCerc (float[] ataques, bool[] siCercanos)
	{
	
		float valorMinCer = ataques[0];
		
		for (int i = 0; i < ataques.Length; i = i + 1)
		{
			if (siCercanos)
			{
				
			}
			if (ataques[i] < valorMinCer)
			{
				valorMinCer = ataques[i];
			}
		}
		return valorMinCer;
	}
	
	
	
	
	
	
	
	
}