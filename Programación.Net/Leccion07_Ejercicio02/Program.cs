﻿using System;

namespace Leccion07_Ejercicio02
{
    /**
     * Se ingresa por teclado un valor entero, mostrar una leyenda que indique si el número es positivo, nulo o negativo. 
     */
    class Program
    {
        static void Main(string[] args)
        {
            int numero;
            string linea;

            Console.WriteLine("Introduzca un valor entero");
            linea = Console.ReadLine();
            numero = Int32.Parse(linea);

            if (numero == 0)
            {
                Console.WriteLine("El número es nulo");
            }
            else if (numero > 0)
            {
                Console.WriteLine("El número es positivo");
            }
            else
            {
                Console.WriteLine("El número es negativo");
            }

            Console.ReadKey();
        }
    }
}
