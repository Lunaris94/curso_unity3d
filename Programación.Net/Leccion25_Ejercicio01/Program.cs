﻿using System;

namespace Leccion25_Ejercicio01
{
    class Empleado
    {
        /*
         * Confeccionar una clase que represente un empleado. Definir como atributos su nombre y su sueldo. En el constructor cargar los atributos y luego en otro método imprimir sus datos y por último uno que imprima un mensaje si debe pagar impuestos (si el sueldo supera a 3000) 
         */

        public string nombre;
        public float sueldo;

        public Empleado()
        {
            Console.WriteLine("Introduzca su nombre:");
            nombre = Console.ReadLine();
            Console.WriteLine("Introduzca sueldo percibido:");
            sueldo = float.Parse(Console.ReadLine());
        }
        public void Mostrar()
        {
            Console.WriteLine(this.nombre + " " +this.sueldo);
        }
        public void PagoImpuestos()
        {
            if (this.sueldo >= 3000)
            {
                Console.WriteLine("Tiene que pagar impuestos");
            }
        }
        static void Main(string[] args)
        {
            Empleado empleado1 = new Empleado();
            Empleado empleado2 = new Empleado();

            empleado1.Mostrar();
            empleado1.PagoImpuestos();
            empleado2.Mostrar();
            empleado2.PagoImpuestos();
        }
    }
}
