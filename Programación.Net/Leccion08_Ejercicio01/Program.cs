﻿using System;

namespace Leccion08_Ejercicio01
{/**
  * Realizar un programa que pida cargar una fecha cualquiera, luego verificar si dicha fecha corresponde a Navidad. 
  */
    class Program
    {
        static void Main(string[] args)
        {
            int dia, mes;
            string linea;
            Console.WriteLine("Ingrese el día en número");
            linea = Console.ReadLine();
            dia = Int32.Parse(linea);
            Console.WriteLine("Ingrese el mes en número");
            linea = Console.ReadLine();
            mes = Int32.Parse(linea);

            if (dia == 25 && mes == 12)
            {
                Console.WriteLine("La fecha indicada corresponde con Navidad");
            }
            else
            {
                Console.WriteLine("La fecha indicada no corresponde con Navidad");
            }
        }
    }
}
