﻿using System;
using System.Numerics;

namespace Leccion15_Ejercicio02
{
    /*
     * Realizar un programa que pida la carga de dos vectores numéricos enteros de 4 elementos. Obtener la suma de los dos vectores, dicho resultado guardarlo en un tercer vector del mismo tamaño. Sumar componente a componente. 
     */
    class PruebaVector2
    {
        int[] vector1;
        int[] vector2;
        int[] vector3;
        static void Main(string[] args)
        {
            PruebaVector2 pruebaV = new PruebaVector2();
            pruebaV.CargarDatos();
            pruebaV.Suma();
            pruebaV.Mostrar();
        }
        public void CargarDatos()
        {
            int valor;
            Console.WriteLine("Carga del primer vector");
            vector1 = new int[4];
            for(int i=0; i < vector1.Length; i++)
            {
                valor = Int32.Parse(Console.ReadLine());
                vector1[i] = valor;
            }
            Console.WriteLine("Carga del segundo vector");
            vector2 = new int[4];
            for (int i = 0; i < vector2.Length; i++)
            {
                valor = Int32.Parse(Console.ReadLine());
                vector2[i] = valor;
            }
        }
        public void Suma()
        {
            vector3 = new int[4];
            for(int i = 0; i < 4; i++)
            {
                vector3[i] = vector1[i] + vector2[i];
            }
        }
        public void Mostrar()
        {
            Console.WriteLine("Tercer vector:");
            for (int i = 0; i < 4; i++)
            {
                Console.WriteLine(vector3[i]);
            }
        }
    }
}
