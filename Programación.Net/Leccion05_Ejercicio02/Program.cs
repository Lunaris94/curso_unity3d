﻿using System;

namespace Leccion05_Ejercicio02
{
    /**
     * Escribir un programa en el cual se ingresen cuatro números, calcular e informar la suma de los dos primeros y el producto del tercero y el cuarto. 
     */
    class Program
    {
        static void Main(string[] args)
        {
            int num1, num2, num3, num4;
            string linea;

            Console.WriteLine("Ingrese el primer número");
            linea = Console.ReadLine();
            num1 = Int32.Parse(linea);
            Console.WriteLine("Ingrese el segundo número");
            linea = Console.ReadLine();
            num2 = Int32.Parse(linea);
            Console.WriteLine("Ingrese el tercer número");
            linea = Console.ReadLine();
            num3 = Int32.Parse(linea);
            Console.WriteLine("Ingrese el cuarto número");
            linea = Console.ReadLine();
            num4 = Int32.Parse(linea);

            int suma = num1 + num2;
            Console.WriteLine("La suma de los dos primeros numeros es: " + suma);
            int producto = num3 * num4;
            Console.WriteLine("El producto de los dos últimos numeros es: " + producto);

            Console.ReadKey();

        }
    }
}
