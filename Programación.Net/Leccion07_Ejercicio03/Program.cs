﻿using System;

namespace Leccion07_Ejercicio03
{
    /**Confeccionar un programa que permita cargar un número entero positivo de hasta tres cifras y muestre un mensaje indicando si tiene 1, 2, o 3 cifras. Mostrar un mensaje de error si el número de cifras es mayor.
     */
    class Program
    {
        static void Main(string[] args)
        {
            int numero;
            string linea;

            Console.WriteLine("Introduzca un número entero positivo");
            linea = Console.ReadLine();
            numero = Int32.Parse(linea);

            //¿COMO SERÍA CON TAMBIÉN CON NUM NEGATIVOS?

            if (numero <=9)
            {
                Console.WriteLine("El número tiene una cifra");
            }else if (numero <= 99) 
            {
                Console.WriteLine("El número tiene dos cifras");
            }else if (numero <= 999)
            {
                Console.WriteLine("El número tiene tres cifras");
            }
            else
            {
                Console.WriteLine("ERROR. El número tiene más de tres cifras");
            }
        }
    }
}
