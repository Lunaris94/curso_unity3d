﻿using System;

public class PruebaPasoPorValorPorReferenciaPatri
{
	static void Main()
	{
		Console.WriteLine("Paso por valor");
		int y = 10;
		PasoPorValor(y);
		Console.WriteLine("Valor de Y = " +y);

		//Console.WriteLine("Paso por referencia)";
	}

	static void PasoPorValor(int x)
	{
		x = 20;
	}

	static void PasoPorReferencia() 
	{
		
	}
}
