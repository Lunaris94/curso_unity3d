﻿using System;

namespace Leccion10_Ejercicio02
{
    /*
     * Desarrollar un programa que solicite la carga de 10 números e imprima la suma de los últimos 5 valores ingresados. 
     */
    class Program
    {
        static void Main(string[] args)
        {
            string linea;
            int valor, suma;
            suma = 0;
            for(int i = 0; i < 10; i++)
            {
                Console.WriteLine("Ingrese valor");
                linea = Console.ReadLine();
                valor = Int32.Parse(linea);

                if (i >= 5)
                {
                    suma = suma + valor;
                }
            }
            Console.WriteLine("La suma de los últimos 5 valores introducidos es: " + suma);
        }
    }
}
