﻿using System;
using System.Collections.Generic;

namespace Leccion09_Ejercicio04
{
    /*
     * Realizar un programa que imprima 25 términos de la serie 11 - 22 - 33 - 44, etc. (No se ingresan valores por teclado) 
     */
    class Program
    {
        static void Main(string[] args)
        {
            List<int> num = new List<int>();
            int i = 0;
            int n = 11;
            num.Add(n);
            while (i < 24)
            {
                n = n + 11;
                num.Add(n);
                i++;
            }

            for(int j = 0; j < num.Count; j++)
            {
                Console.WriteLine(num[j]);
            }
        }
    }
}
