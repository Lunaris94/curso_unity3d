﻿using System;
using System.Collections.Generic;
using System.Xml.Schema;

namespace Leccion09_Ejercicio07
{
    /*
     * Desarrollar un programa que permita cargar n números enteros y luego nos informe cuántos valores fueron pares y cuántos impares.
     * Emplear el operador “%” en la condición de la estructura condicional:
     * if (valor%2==0)         //Si el if da verdadero luego es par.
     */
    class Program
    {
        static void Main(string[] args)
        {
            List<int> numeros = new List<int>();
            List<int> numerosPares = new List<int>();
            List<int> numerosImpares = new List<int>();

            Console.WriteLine("¿Cuántos números enteros va a introducir?");
            string linea = Console.ReadLine();
            int n = Int32.Parse(linea);

            numeros=AñadirValores(n);
            numerosPares = Pares(numeros);
            numerosImpares = Impares(numeros);
            MostrarLista(numerosPares);
            MostrarLista(numerosImpares);
            Console.WriteLine("Hay " + numerosPares.Count + " números pares");
            Console.WriteLine("Hay " + numerosImpares.Count + " números impares");
        }

        public static void MostrarLista(List<int> lista)
        {
            Console.WriteLine("LISTA");
            for (int i = 0; i < lista.Count; i++)
            {
                Console.WriteLine(lista[i]);
            }
        }
        public static List<int> AñadirValores(int n)
        {
            List<int> lista = new List<int>();
            string linea;
            int valor;
            int i = 0;
            while (i < n)
            {
                Console.WriteLine("Ingrese valor");
                linea = Console.ReadLine();
                valor = Int32.Parse(linea);
                lista.Add(valor);
                i++;
            }
            return lista;
        }
        public static List<int> Pares(List<int> lista)
        {
            List<int> listaPares = new List<int>();

            for(int i = 0; i < lista.Count; i++)
            {
                if (lista[i] % 2 == 0)
                {
                    listaPares.Add(lista[i]);
                }
            }
            return listaPares;
        }
        public static List<int> Impares(List<int> lista)
        {
            List<int> listaImpares = new List<int>();

            for (int i = 0; i < lista.Count; i++)
            {
                if (lista[i] % 2 == 0)
                {
                }
                else
                {
                    listaImpares.Add(lista[i]);
                }
            }
            return listaImpares;
        }
    }
}
