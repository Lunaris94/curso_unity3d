﻿using System;
using System.Collections.Generic;

public class OrdenarComoHumano
{
	public static void Main()
	{
		List<int> numeros = new List<int>();
		numeros.Add(2);
		numeros.Add(1);
		numeros.Add(5);
		numeros.Add(4);
		numeros.Add(3);

		MostrarLista(numeros);
		Prueba(numeros);
	}

	public static void MostrarLista(List<int> lista)
	{
		Console.WriteLine("Inicio");
		for (int i = 0; i < lista.Count; i++)
		{
			Console.WriteLine(lista[i]);
		}
		Console.WriteLine("Fin");
	}

	public static void Prueba(List<int> lista)
	{
		for (int j = 0; j < lista.Count; j++)
		{
			int posj = j;
			int menor = lista[0];
			while (posj< lista.Count)
			{
				int i = 0;
				int posi = i;
				Console.WriteLine("I es: " + posi);
				if (lista[i] < menor)
				{
					menor = lista[i];
				}
				i++;
			}
			Console.WriteLine("J es: " + posj);
			Console.WriteLine("El menor es: " + menor);
		}
	}
}
