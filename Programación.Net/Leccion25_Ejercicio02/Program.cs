﻿using System;

namespace Leccion25_Ejercicio02
{
    /*
     * Implementar la clase operaciones. Se deben cargar dos valores enteros en el constructor, calcular su suma, resta, multiplicación y división, cada una en un método, imprimir dichos resultados. 
     */
    class Operaciones
    {
        int valor1, valor2;
        public Operaciones()
        {
            Console.WriteLine("Introduzca primer valor");
            valor1 = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Introduzca segundo valor");
            valor2 = Int32.Parse(Console.ReadLine());
        }
        public int Suma()
        {
            int suma = this.valor1 + this.valor2;
            return suma;
        }
        public int Resta()
        {
            int resta = valor1 - valor2;
            return resta;
        }
        public int Multiplicacion()
        {
            int producto = valor1 * valor2;
            return producto;
        }
        public int Division()
        {
            int division = valor1 / valor2;
            return division;
        }
        public void Mostrar()
        {
            int suma, resta, producto, division;
            Console.WriteLine("Suma: " + (suma = Suma()));
            Console.WriteLine("Resta: " + (resta = Resta()));
            Console.WriteLine("Multiplicación: " + (producto = Multiplicacion()));
            Console.WriteLine("División: " + (division = Division()));
        }
        static void Main(string[] args)
        {
            Operaciones operaciones1 = new Operaciones();
            operaciones1.Mostrar();
            Operaciones operaciones2 = new Operaciones();
            operaciones2.Mostrar();
        }
    }
}
