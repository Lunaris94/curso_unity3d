﻿using System;

namespace Leccion08_Ejercicio06
{
    /*
     * De un operario se conoce su sueldo y los años de antigüedad. Se pide confeccionar un programa que lea los datos de entrada e informe:
     * a) Si el sueldo es inferior a 500 y su antigüedad es igual o superior a 10 años, otorgarle un aumento del 20 %, mostrar el sueldo a pagar.
     * b)Si el sueldo es inferior a 500 pero su antigüedad es menor a 10 años, otorgarle un aumento de 5 %.
     * c) Si el sueldo es mayor o igual a 500 mostrar el sueldo en pantalla sin cambios. 
     */
    class Program
    {
        static void Main(string[] args)
        {
            float sueldo;
            int años;
            string linea;

            Console.WriteLine("Ingrese el sueldo");
            linea = Console.ReadLine();
            sueldo = float.Parse(linea);
            Console.WriteLine("Ingrese los años de antigüedad");
            linea = Console.ReadLine();
            años = Int32.Parse(linea);

            if (sueldo < 500 && años >= 10)
            {
                float aum = (sueldo * 20) / 100;
                sueldo = sueldo + aum;
                Console.WriteLine("El sueldo ha pagar con aumento del 20% es: " + sueldo);
            }else if (sueldo < 500 && años < 10)
            {
                float aum = (sueldo * 5) / 100;
                sueldo = sueldo + aum;
                Console.WriteLine("El sueldo ha pagar con aumento del 5% es: " + sueldo);
            }else if (sueldo >= 500)
            {
                Console.WriteLine("El sueldo sin cambios es de: " + sueldo);
            }
            Console.ReadKey();
        }
    }
}
