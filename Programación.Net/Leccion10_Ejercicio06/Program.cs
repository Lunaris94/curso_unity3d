﻿using System;

namespace Leccion10_Ejercicio06
{
    /*
     * Escribir un programa que pida ingresar coordenadas (x,y) que representan puntos en el plano.
     * Informar cuántos puntos se han ingresado en el primer, segundo, tercer y cuarto cuadrante. Al comenzar el programa se pide que se ingrese la cantidad de puntos a procesar.
     */
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Cuántas coordenadas va a introducir?");
            string linea = Console.ReadLine();
            int n = Int32.Parse(linea);

            int cuadUno = 0;
            int cuadDos = 0;
            int cuadTres = 0;
            int cuadCuatro = 0;

            for (int i = 0; i < n; i++)
            {
                int x, y;

                Console.WriteLine("Nuevo vector");
                Console.WriteLine("Ingrese la coordenada x");
                linea = Console.ReadLine();
                x = Int32.Parse(linea);
                Console.WriteLine("Ingrese la coordenada y");
                linea = Console.ReadLine();
                y = Int32.Parse(linea);

                if (x > 0 && y > 0)
                {
                    cuadUno = cuadUno + 1;
                }
                else if (x < 0 && y > 0)
                {
                    cuadDos = cuadDos + 1;
                }
                else if (x < 0 && y < 0)
                {
                    cuadTres = cuadTres + 1;
                }
                else if (x > 0 && y < 0)
                {
                    cuadCuatro = cuadCuatro + 1;
                }

            }
            Console.WriteLine("En el 1º Cuadrante hay: " + cuadUno);
            Console.WriteLine("En el 2º Cuadrante hay: " + cuadDos);
            Console.WriteLine("En el 3º Cuadrante hay: " + cuadTres);
            Console.WriteLine("En el 4º Cuadrante hay: " + cuadCuatro);

            Console.ReadKey();
        }
    }
}
