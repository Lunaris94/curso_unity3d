﻿using System;

public class EjemploTryCatch
{
    static void Main()
    {
         try
         {
             Console.WriteLine("Empieza el programa");
             int x = Int32.Parse("NO ES NÚMERO"); //Lanzará una excepción
             Console.WriteLine("Esta linea no se ejecuta");

         }
         catch (FormatException error)
         {
             Console.WriteLine("Podemos diferenciar entre tipos de errores:");
             Console.WriteLine("ERROR DE FORMATO");
             Console.WriteLine(error.Message);
         }
         catch (Exception error)
         {
             Console.WriteLine("ERROR GENÉRICO");
             Console.WriteLine(error.Message);
         }
         Console.WriteLine("Pero al capturar el error, sí permitimos continuar al programa");
         Console.WriteLine("Incluso mostrar el error de manera controlada: ");
         Console.WriteLine("Termina el programa");

        ArrayTryCatch();
    }

        //Provocar un error de límites de array. Es decir, creamos un array de 5 
        //elementos e intentamos acceder al 7º (que no existe, provocando un error)
        //Luego, controlarlo con Try/Catch

    static void ArrayTryCatch()
    { 
        Console.WriteLine("Provocar error array");

        try
        {
            Console.WriteLine("Empieza el programa");
            int[] PruebaError = { 1, 2, 3, 4, 5 };
            Console.WriteLine(PruebaError[6]);
            Console.WriteLine("Esta linea no se ejecuta");
        }
        catch (FormatException error)
        {
            Console.WriteLine("Error formato");
            Console.WriteLine(error.Message);
        }
        catch (IndexOutOfRangeException error)
        {
            Console.WriteLine("Error array");
            Console.WriteLine(error.Message);
        }
        catch (Exception error)
        {
            Console.WriteLine("Error genérico");
            Console.WriteLine(error.Message);
        }
        //A la hora de poner varios catch, tenemos que ir de lo más específico
        //a lo más genérico, para que así no haya ningún error
        Console.WriteLine("Final de programa");
    }
    //A previous catch clause already catches all exceptions of this or a super type `System.Exception'

}
