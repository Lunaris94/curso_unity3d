﻿using System;
using System.ComponentModel;

namespace Leccion20_Ejercicio01
{
    /*
     * Cargar en un vector los nombres de 5 paises y en otro vector paralelo la cantidad de habitantes del mismo. Ordenar alfabéticamente e imprimir los resultados. Por último ordenar con respecto a la cantidad de habitantes (de mayor a menor) e imprimir nuevamente.
     */
    class Program
    {
        string[] pais;
        float[] habitantes;
        static void Main(string[] args)
        {
            Program listaHab = new Program();

            listaHab.CargarDatos();
            listaHab.MostrarListas();
            listaHab.OrdenAlfabetico();
            listaHab.MostrarListas();
            listaHab.OrdenMayor();
            listaHab.MostrarListas();
        }

        public void CargarDatos()
        {
            pais = new string[5];
            habitantes = new float[5];

            for(int i = 0; i < 5; i++)
            {
                Console.WriteLine("Ingrese nombre de país");
                pais[i] = Console.ReadLine();
                Console.WriteLine("Ingrese número habitantes");
                habitantes[i] = float.Parse(Console.ReadLine());
            }
        }
        public void MostrarListas()
        {
            Console.WriteLine();
            for(int i = 0; i < 5; i++)
            {
                Console.WriteLine(pais[i] + " = " + habitantes[i]);
            }
        }

        public void OrdenAlfabetico()
        {
            string aux;
            float aux2;
            
            for(int j = 0; j < pais.Length; j++)
            {
                for(int i = 0; i < pais.Length-1; i++)
                {
                    if (pais[i].CompareTo(pais[i+1])> 0)
                    {
                        aux = pais[i];
                        pais[i] = pais[i + 1];
                        pais[i + 1] = aux;

                        aux2 = habitantes[i];
                        habitantes[i] = habitantes[i + 1];
                        habitantes[i + 1] = aux2;
                    }
                }
            }
        }
        public void OrdenMayor()
        {
            float aux;
            string aux2;
            for(int j = 0; j < habitantes.Length; j++)
            {
                for(int i = 0; i < habitantes.Length - 1; i++)
                {
                    if (habitantes[i] < habitantes[i + 1])
                    {
                        aux = habitantes[i];
                        habitantes[i] = habitantes[i + 1];
                        habitantes[i + 1] = aux;

                        aux2 = pais[i];
                        pais[i] = pais[i + 1];
                        pais[i + 1] = aux2;
                    }
                }
            }

        }
    }
}
