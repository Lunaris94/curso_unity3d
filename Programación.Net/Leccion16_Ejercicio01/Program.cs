﻿using System;

namespace Leccion16_Ejercicio01
{
    /*
     * Desarrollar un programa que permita ingresar un vector de n elementos, ingresar n por teclado. Luego imprimir la suma de todos sus elementos 
     */
    class Program
    {
        private int[] numeros;
        static void Main(string[] args)
        {
            Program sumaElementos = new Program();
            sumaElementos.CargarDatos();
            sumaElementos.SumarDatos();
        }
        public void CargarDatos()
        {
            Console.WriteLine("Número de datos");
            int n = Int32.Parse(Console.ReadLine());
            numeros = new int[n];
            //int suma = 0;
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine("Introduzca dato");
                numeros[i] = Int32.Parse(Console.ReadLine());
                //suma = suma + numeros[i];
            }
            //Console.WriteLine("La suma de todos los valores es: " + suma);
        }
        public void SumarDatos()
        {
            int suma = 0;
            for (int i = 0; i < numeros.Length; i++)
            {
                suma = suma + numeros[i];
            }
            Console.WriteLine("La suma de todos los valores es: " + suma);
        }
    }
}
