﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Leccion13_EjemploPOO3enRaya
{
    class Tablero
    {
        public string[,] casillas;
        public int filas;
        public int columnas;
        public string simboloGanador = "_";

        public Tablero()
        {
            this.filas = 3;
            this.columnas = 3;
            this.casillas = new string[filas, columnas];
        }
        public bool SiTableroLleno()
        {
            for(int f=0; f < this.filas; f++)
            {
                for(int c = 0; c < this.columnas; c++)
                {
                    if (this.casillas[f, c] == "_")
                    {
                        return false;
                    }
                }
            }
            Console.WriteLine("\nEl tablero está completo");
            return true;
        }
    }
}
