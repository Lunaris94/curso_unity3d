﻿using System;

namespace Leccion13_EjemploPOO3enRaya
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("<----¡TRES EN RAYA!---->");
            Juego miJuego = new Juego();
            JuegoDif miJuego2 = new JuegoDif();
            //TODO: Que el jugador pueda elegir entre jugaro contra otra persona o contra el propio ordenador
            string dif = "";
            do
            {
                Console.WriteLine("\nElige nivel de dificultad: \n1-Normal \n2-DifÍcil");
                dif = Console.ReadLine();
                if (dif == "1")
                {
                    Console.WriteLine("\nHas escogido el nivel normal");
                    miJuego.Inicializar();
                    miJuego.RellenaConEspacios();
                    miJuego.MostrarTablero();
                    miJuego.Jugando();
                    //miJuego.ComprobarGameWin();
                }
                else if (dif == "2")
                {
                    Console.WriteLine("\nHas escogido el nivel difícil");
                    miJuego2.Inicializar();
                    miJuego2.RellenaConEspacios();
                    miJuego2.MostrarTablero();
                    miJuego2.Jugando();

                    //Console.WriteLine("\nEste nivel está en proceso de construcción. Lamentamos las molestias.");
                    Environment.Exit(0);
                }
                else
                {
                    Console.WriteLine("\nEl nivel seleccionado no existe, prueba otra vez");
                }
            } while (dif != "1" || dif != "2");
        }
    }
}
