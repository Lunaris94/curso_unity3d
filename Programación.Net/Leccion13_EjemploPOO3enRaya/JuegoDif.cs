﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Leccion13_EjemploPOO3enRaya
{
    class JuegoDif
    {
        public Jugadores jugador1;
        public Jugadores jugador2;
        public Tablero tresEnRaya;
        public string[] guiaNum;
        public void Inicializar()
        {
            this.jugador1 = new Jugadores();
            this.jugador2 = new Jugadores();
            this.tresEnRaya = new Tablero();

            guiaNum = new string[3];
            guiaNum[0] = "1";
            guiaNum[1] = "2";
            guiaNum[2] = "3";

            Console.WriteLine("\nIngresa nombre Jugador 1: ");
            this.jugador1.nombre = Console.ReadLine();
            Console.WriteLine("\nIngresa símbolo para Jugador 1: ");
            this.jugador1.simbolo = Console.ReadLine();
            Console.WriteLine("\nIngresa nombre Jugador 2: ");
            this.jugador2.nombre = Console.ReadLine();
            Console.WriteLine("\nIngresa símbolo para Jugador 2: ");
            this.jugador2.simbolo = Console.ReadLine();
        }
        public void MostrarLista()
        {
            Console.Write("  ");
            for (int i = 0; i < guiaNum.Length; i++)
            {
                Console.Write(guiaNum[i] + " ");
            }
            Console.WriteLine();

        }
        public void MostrarTablero()
        {
            Console.WriteLine();
            MostrarLista();
            for (int f = 0; f < tresEnRaya.filas; f++)
            {
                Console.Write(guiaNum[f] + " ");
                for (int c = 0; c < tresEnRaya.columnas; c++)
                {
                    Console.Write(tresEnRaya.casillas[f, c] + " ");
                }
                Console.WriteLine();
            }
        }
        public void RellenaConEspacios()
        {
            for (int f = 0; f < tresEnRaya.filas; f++)
            {
                for (int c = 0; c < tresEnRaya.columnas; c++)
                {
                    tresEnRaya.casillas[f, c] = "_";
                }
            }
        }
        public void Jugando()
        {
            for(int i = 0; i < 3; i++)
            {
                jugador1.RellenarCasilla(tresEnRaya);
                MostrarTablero();
                if (i == 2)
                {
                    //Este if me sirve para que solo compruebe si la tercera vez que el jugador 1 rellena ficha ha logrado ganar
                    ComprobarGameWin();
                }
                jugador2.RellenarCasilla(tresEnRaya);
                MostrarTablero();
            }
            //Para comprobar si el jugador 2 ha ganado despues de su tercer movimiento ya no hace falta poner if, asi que lo saco fuera y cuando acabe el bucle(3 veces) se comprueba de nuevo
            ComprobarGameWin();

            Console.WriteLine("\n¡HA MOVER LAS FICHAS!");
            do
            {
                string pos1 = jugador1.MoverFicha(tresEnRaya);
                Console.WriteLine(pos1);
                MostrarTablero();
                jugador1.ColocarFicha(tresEnRaya, pos1);
                MostrarTablero();
                ComprobarGameWin();
                string pos2 = jugador2.MoverFicha(tresEnRaya);
                //Console.WriteLine(pos2);
                MostrarTablero();
                jugador2.ColocarFicha(tresEnRaya, pos2);
                MostrarTablero();
                ComprobarGameWin();

            } while (SiGameWin()!=true);
        }
        public void ComprobarGameWin()
        {
            if (jugador1.ComprobarHorizontal(tresEnRaya) == true || jugador1.ComprobarVertical(tresEnRaya) == true || jugador1.ComprobarDiag1(tresEnRaya) == true || jugador1.ComprobarDiag2(tresEnRaya) == true)
            {
                Console.WriteLine("\n¡GAME WIN!\n¡¡ " + jugador1.nombre + " HA GANADO!!");
                Console.ReadKey();
                Reiniciar();
            }
            else if (jugador2.ComprobarHorizontal(tresEnRaya) == true || jugador2.ComprobarVertical(tresEnRaya) == true || jugador2.ComprobarDiag1(tresEnRaya) == true || jugador2.ComprobarDiag2(tresEnRaya) == true)
            {
                Console.WriteLine("\n¡GAME WIN!\n¡¡ " + jugador2.nombre + " HA GANADO!!");
                Console.ReadKey();
                Reiniciar();
            }
        }
        public void Reiniciar()
        {
            Console.WriteLine("\n--¿Quieres volver a jugar?--" + "\nS=Si/N=No");
            string aux = Console.ReadLine();
            if (aux == "S" || aux == "s")
            {
                RellenaConEspacios();
                MostrarTablero();
                Jugando();
            }
            else
            {
                Environment.Exit(0);
            }
        }
        public bool SiGameWin()
        {
            if(jugador1.ComprobarHorizontal(tresEnRaya) == true || jugador1.ComprobarVertical(tresEnRaya) == true || jugador1.ComprobarDiag1(tresEnRaya) == true || jugador1.ComprobarDiag2(tresEnRaya) == true)
            {
                return true;
            }else if(jugador2.ComprobarHorizontal(tresEnRaya) == true || jugador2.ComprobarVertical(tresEnRaya) == true || jugador2.ComprobarDiag1(tresEnRaya) == true || jugador2.ComprobarDiag2(tresEnRaya) == true)
            {
                return true;
            }
            return false;
        }
    }
}
