﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Leccion13_EjemploPOO3enRaya
{
    class Jugadores
    {
        public string nombre;
        public string simbolo;

        public Jugadores()
        {
            this.nombre = "Jugador sin nombre";
            this.simbolo = "Jugador sin simbolo";
        }

        public void RellenarCasilla(Tablero tablero)
        {
            int f, c;
            Console.WriteLine("\n---¡EMPIEZA EL JUEGO!---");
            do
            {
                Console.WriteLine("\n--Turno de: " + this.nombre + "\nIndica la casilla que vas a rellenar");
                f = PedirFicha("Fila");
                c = PedirFicha("Columna");
                if (tablero.casillas[f, c] != "_")
                {
                    Console.WriteLine("\nLa casilla ya está rellena, prueba con otra");
                }
            }
            while (tablero.casillas[f, c] != "_");

            tablero.casillas[f, c] = this.simbolo;
        }

        public int PedirFicha(string tipo)
        {
            int ficha;
            Console.WriteLine(tipo + ":");
            ficha = Int32.Parse(Console.ReadLine());
            ficha = ficha - 1;
            if (ficha > 2 || ficha < 0)
            {
                do
                {
                    Console.WriteLine("\nEl número introducido (para "+tipo+") está fuera de los límites del tablero, prueba con otro");
                    Console.WriteLine(tipo + ":");
                    ficha = Int32.Parse(Console.ReadLine());
                    ficha = ficha - 1;
                } while (ficha > 2 || ficha < 0);
            }
            return ficha;
        }
        public string MoverFicha(Tablero tablero)
        {
            int posF;
            int posC;
            do
            {
                Console.WriteLine("\n--Turno de: " + this.nombre+ "\nElige una ficha para mover ");
                posF = PedirFicha("Fila");
                posC = PedirFicha("Columna");
                if (tablero.casillas[posF, posC] == "_")
                {
                    Console.WriteLine("\nLa casilla está vacía, selecciona una que contenga una ficha tuya");
                }
                else if(tablero.casillas[posF, posC] != this.simbolo)
                {
                    Console.WriteLine("\nLa ficha seleccionada no es tuya, selecciona de nuevo otra posición");
                }
            } while (tablero.casillas[posF,posC]!=this.simbolo);
            string pos = posF + "" + posC ;
            tablero.casillas[posF, posC] = "_";
            return pos;
        }
        public void ColocarFicha(Tablero tablero, string pos)
        {
            int f, c;
            string posFNueva, posCNueva;
            string posNueva;
            do
            {
                Console.WriteLine("\n--Turno de: " + this.nombre + "\nIndica donde quieres colocar tu ficha");
                f = PedirFicha("Fila");
                c = PedirFicha("Columna");
                posFNueva = f.ToString();
                posCNueva = c.ToString();
                posNueva = posFNueva + "" + posCNueva;
                Console.WriteLine(posNueva);
                if (SiSonIguales(pos, posNueva)==true)
                {
                    Console.WriteLine("\nHas elegido la msima casilla en la que estaba la ficha, prueba con otra");
                }else if (tablero.casillas[f, c] != "_")
                {
                    Console.WriteLine("\nLa casilla ya está rellena, prueba con otra");
                }
            }
            while (SiSonIguales(pos, posNueva) == true || tablero.casillas[f, c] != "_");

            tablero.casillas[f, c] = this.simbolo;
        }
        public bool SiSonIguales(string pos, string posNueva)
        {
            if (pos.Equals(posNueva))
            {
                return true;
            }
            return false;
        }
        public bool ComprobarHorizontal(Tablero tablero)
        {
            for (int f = 0; f < tablero.filas; f++)
            {
                if (tablero.casillas[f, 0] == tablero.casillas[f, 1]  && tablero.casillas[f, 1] == tablero.casillas[f, 2])
                {
                    if(tablero.casillas[f, 0] == this.simbolo)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public bool ComprobarVertical(Tablero tablero)
        {
            for (int c = 0; c < tablero.columnas; c++)
            {
                if (tablero.casillas[0, c] == tablero.casillas[1, c] && tablero.casillas[1, c] == tablero.casillas[2, c])
                {
                    if (tablero.casillas[0, c] == this.simbolo)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public bool ComprobarDiag1(Tablero tablero)
        {
            if (tablero.casillas[0, 0] == tablero.casillas[1, 1] && tablero.casillas[1, 1] == tablero.casillas[2, 2])
            {
                if (tablero.casillas[0, 0] == this.simbolo)
                {
                    return true;
                }
            }
            return false;
        }
        public bool ComprobarDiag2(Tablero tablero)
        {
            if (tablero.casillas[0, 2] == tablero.casillas[1, 1] && tablero.casillas[1, 1] == tablero.casillas[2, 0])
            {
                if (tablero.casillas[0, 2] == this.simbolo)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
