﻿using System;

namespace Leccion15_Ejercicio03
{
    /*
     * Se tienen las notas del primer parcial de los alumnos de dos cursos, el curso A y el curso B, cada curso cuenta con 5 alumnos.
     * Realizar un programa que muestre el curso que obtuvo el mayor promedio general. 
     */
    class Notas
    {
        int[] notasA;
        int[] notasB;
        static void Main(string[] args)
        {
            Notas promedioNotas = new Notas();

            promedioNotas.CargarDatos();
            promedioNotas.PromedioMayor();
        }
        public void CargarDatos()
        {
            int nota;
            Console.WriteLine("Cargar notas Curso A");
            notasA = new int[5];
            for(int i = 0; i < notasA.Length; i++)
            {
                nota = Int32.Parse(Console.ReadLine());
                notasA[i] = nota;
            }
            Console.WriteLine("Cargar notas Curso B");
            notasB = new int[5];
            for (int i = 0; i < notasB.Length; i++)
            {
                nota = Int32.Parse(Console.ReadLine());
                notasB[i] = nota;
            }
        }
        public void PromedioMayor()
        {
            int sumaA = 0;
            int sumaB = 0;
            int promedioA, promedioB;
            for(int i = 0; i < 5; i++)
            {
                sumaA = sumaA + notasA[i];
                sumaB = sumaB + notasB[i];
            }
            promedioA = sumaA / notasA.Length;
            promedioB = sumaB / notasB.Length;
            if (promedioA > promedioB)
            {
                Console.WriteLine("La clase A tiene unas notas promedio mayor");
            }
            else
            {
                Console.WriteLine("La clase B tiene unas notas promedio mayor");
            }
        }
    }
}
