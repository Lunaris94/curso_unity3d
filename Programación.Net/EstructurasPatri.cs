﻿using System;

struct Jugador
{
   public string nombre;
   public int edad;
   public bool member; //Si es miembro (true) ha pagado, si es false es free to play
   public float vida;
   
    //Constructor: Función especial que sirve para inicializar los datos de una
    //estructura del tirón. En las estructuras es obligatorio inicializar todos
    //los variables miembros(campos, porpiedades, atributos...)
    public Jugador(string nombre, int edad, bool member, float vida) 
    {
        this.nombre = nombre;
        this.edad = edad;
        this.member = member;
        this.vida = vida;
    }

//ya no hace falta determinar los parámetros que recibe, porque los tiene declarados arriba
   public void Mostrar() //Debe ser público y no estático
   {
        Console.WriteLine("Jugador:" + this.nombre + " Edad:" + this.edad + ( member ? " Ha pagado" : " Free to play") + " Vida:" + this.vida);
        //Si f es verdadero, escribirá Fuma, si f es falso, escribirá No fuma.
        //Utililzamos el this. para hacer referencia a la edad del personaje
        //porque se confundiría si instanciamos un entero que se llame tambien edad
   }
}

struct Enemigo
{
    public string nombre;
    public float ataque;

    public Enemigo(string nombre, float ataque)
    {
        this.nombre = nombre;
        this.ataque = ataque;
    }

    public void Mostrar()
    {
        Console.WriteLine("Enemigo:" + this.nombre + " Ataque:" + this.ataque);
    }

    public void Ataque(ref Jugador jugador)
    {
        jugador.vida = jugador.vida - this.ataque;
        Console.WriteLine("Acaba de hacer el ataque. Resultado: ");
        jugador.Mostrar();
    }

    //
    public void AtaqueAVarios(Jugador[] jugadores)
    {
        for (int i = 0; i < jugadores.Length; i++)
        {
            jugadores[i].vida = jugadores[i].vida - this.ataque;
            Console.WriteLine("Acaba de hacer el segundo ataque. Resultado:");
            jugadores[i].Mostrar();
        }
    }
}

public class Estructuras
{
    public static void Main()
    {
        Jugador jug1 =new Jugador("Pedro", 30, false, 400f);
        jug1.Mostrar();
        Jugador jug2 = new Jugador("María", 32, true, 350f);
        jug2.Mostrar();
        Jugador jug3 = new Jugador("Lucía", 35, false, 450f);
        jug3.Mostrar();

        Enemigo enemigo1 = new Enemigo("Zombie", 150f);
        enemigo1.Mostrar();
        Enemigo enemigo2 = new Enemigo("Rata", 50f);
        enemigo2.Mostrar();

        enemigo1.Ataque(ref jug1);  //DOBLE ATAQUE
        enemigo1.Ataque(ref jug1);  //SE DEBERÍA QUEDAR EN 100
        //jug1.Mostrar();
        //Para que la segunda vez que le pedimos que nos muestre la vida del jugador que ha sido dañado
        //y que nos muestre el valor actual, no el inicial, ¿podríamos?:
        //-Hacer un cambio de o de referencia, de forma que, cuando ha sido dañado, utilice la vida actualizada
        //Crear otro método dentro de jugador, de forma que se realice esa sustitución de valor o la utilización de ese puntero
        
        Jugador[] todosLosJug = new Jugador[3];
        todosLosJug[0] = jug1;
        todosLosJug[1] = jug2;
        todosLosJug[2] = jug3;

        enemigo2.AtaqueAVarios(todosLosJug); //DOBLE ATAQUE
        enemigo2.AtaqueAVarios(todosLosJug);

        Console.WriteLine("RESULTADOS FINALES");
        jug1.Mostrar();
        jug2.Mostrar();
        jug3.Mostrar();

        /*Jugador jug1;
        jug.nombre = "Pedro";
        jug.edad = 30;
        jug.member = false;
        jug.Mostrar();
        //MostrarPersona(jug.nombre, jug.edad, jug.member); Ésto queda sustiuido
        por la línea anterior

        Jugador jug2;
        jug2.nombre = "Maria";
        jug2.edad = 32;
        jug2.member= true;
        jug2.Mostrar();
        //MostrarPersona(jug2.nombre, jug2.edad, jug2.member);

        Jugador jug3;
        jug3.nombre = "Lucía";
        jug3.edad = 35;
        jug3.member = false;
        jug.Mostrar();
        //MostrarPersona(jug3.nombre, jug3.edad, jug3.member);

        Enemigo enem;
        enem.nombre = "Zombie";
        enem.ataque = 150;
        enem.Mostrar();
        //MostrarEnemigo(enem.nombre, enem.ataque);*/


        //EJERCICIO
        //1-Crear otro enemigo *
        //2-Añadir otro campo "Vida" de tipo float al jugador *
        //3-Hacer un método ataque en enemigo, que reciba un jugador como 
        //parámetro y que le quite vida al jugador
        //4-Usarlos: enemigo 1 ataqua al jugador 1 y enemigo 2 ataca a todos los jugadores
    }
}
