﻿using System;

namespace Leccion12_Ejemplo02
{
    class Program
    {
        static void Main(string[] args)
        {
            string apenom1, apenom2;
            int edad1, edad2;
            string linea;
            Console.Write("Ingrese el apellido y el nombre:");
            apenom1 = Console.ReadLine();
            Console.Write("Ingrese edad:");
            linea = Console.ReadLine();
            edad1 = int.Parse(linea);
            Console.Write("Ingrese el apellido y el nombre:");
            apenom2 = Console.ReadLine();
            Console.Write("Ingrese edad:");
            linea = Console.ReadLine();
            edad2 = int.Parse(linea);
            Console.Write("La persona de mayor edad es:");
            if (edad1 > edad2)
            {
                Console.Write(apenom1);
            }
            else
            {
                Console.Write(apenom2);
            }
            Console.ReadKey();
        }
    }
}
