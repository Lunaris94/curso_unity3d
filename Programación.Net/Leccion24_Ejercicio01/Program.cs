﻿using System;
using System.Globalization;

namespace Leccion24_Ejercicio01
{
    /*
     * Confeccionar una clase para administrar una matriz irregular de 5 filas y 1 columna la primer fila, 2 columnas la segunda fila y así sucesivamente hasta 5 columnas la última fila (crearla sin la intervención del operador)
     * Realizar la carga por teclado e imprimir posteriormente. 
     */
    class Program
    {
        int[][] numeros;
        static void Main(string[] args)
        {
            Program matrizIrr = new Program();

            matrizIrr.CargarDatos();
            matrizIrr.MostrarMatriz();
        }
        public void CargarDatos()
        {
            int valor;
            Console.WriteLine("Creando matriz irregular");
            numeros = new int[5][];
            for(int f = 0; f < numeros.Length; f++)
            {
                Console.WriteLine("Fila " + f);
                numeros[f] = new int[f + 1];
                for(int c = 0; c < numeros[f].Length; c++)
                {
                    Console.WriteLine("Introduzca valor");
                    valor = Int32.Parse(Console.ReadLine());
                    numeros[f][c] = valor;
                }
            }
        }
        public void MostrarMatriz()
        {
            for(int f=0; f < numeros.Length; f++)
            {
                for(int c = 0; c < numeros[f].Length; c++)
                {
                    Console.Write(numeros[f][c]);
                }
                Console.WriteLine();
            }
        }
    }
}
