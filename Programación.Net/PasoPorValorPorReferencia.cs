﻿using System;


public class PasoPorValorPorReferencia
{
    static void Main()
    {
        Console.WriteLine("Paso por valor");
        int y = 10;
        CambiarVariablePorValor(y);
        Console.WriteLine("Y = " + y);

        CambiarVariablePorReferencia(out y);
        Console.WriteLine("Y = " + y);

        //Ejemplo de paso por valor

        //Como la cadena de texto LOO no es en sí un numero, 
        //hay que controlar la excepción con try/catch
        
        string supuestoNumero_1 = "200", supuestoNumero_2 = "LOO";
        int numero_1 = 0, numero_2 = 0;
        try
        {
            numero_1 = Int32.Parse(supuestoNumero_1);   //BIEN
            numero_2 = Int32.Parse(supuestoNumero_2);   //FALLA
            Console.WriteLine("Número 1 = " + numero_1 + ", Número 2 = " + numero_2);
        }
        catch (FormatException error) //En vez de error también se puede poner "er" o "ex"
        {
            Console.WriteLine("Error de formato:  " + supuestoNumero_2);
            Console.WriteLine(error.Message);
        }
        catch (Exception ex)
        {
            Console.WriteLine("Error genérico");
            Console.WriteLine(ex.Message);
        }

        //Ejemplo de paso por referencia
        if (Int32.TryParse(supuestoNumero_1, out numero_1))
        {
            Console.WriteLine("OK, Num 1 = " + numero_1);
        }
        else
        {
            Console.WriteLine("Casca en el formateo");
        }

        if (Int32.TryParse(supuestoNumero_2, out numero_2))
        {
            Console.WriteLine("OK, Num 2 = " + numero_2);
        }
        else
        {
            Console.WriteLine("Casca en el formateo");
        }
    }
    // Pasamos el dato POR VALOR: Se hace una COPIA del valor en la memoria
    //Las variables (la anterior, y ste parámetro) representan variables 
    //diferentes, solo se copia el valor
    static void CambiarVariablePorValor(int x)
    {
        x = 20;
    }
    //Pasamos el dato POR REFERENCIA: En este caso (la anterior y este parámetro)
    //representan la MISMA VARIABLE (se copia la referencia, la dirección en la memoria)
    static void CambiarVariablePorReferencia(ref int z) //en vez de ref se puede utilizar out
    {
        z = 20;
    }

}
