﻿using System;

namespace Leccion08_Ejercicio04
{
    /*
     * Se ingresan por teclado tres números, si al menos uno de los valores ingresados es menor a 10, imprimir en pantalla la leyenda "Alguno de los números es menor a diez".
     */
    class Program
    {
        static void Main(string[] args)
        {
            int num1, num2, num3;
            string linea;

            Console.WriteLine("Ingrese el primer valor");
            linea = Console.ReadLine();
            num1 = Int32.Parse(linea);
            Console.WriteLine("Ingrese el segundo valor");
            linea = Console.ReadLine();
            num2 = Int32.Parse(linea);
            Console.WriteLine("Ingrese el tercer valor");
            linea = Console.ReadLine();
            num3 = Int32.Parse(linea);

            if (num1 < 10 || num2 < 10 || num3 < 10)
            {
                Console.WriteLine("Alguno de los numeros es menor a diez");
            }
            Console.ReadKey();
        }
    }
}
