﻿using System;

namespace Leccion08_Ejercicio05
{
    /*
     * Escribir un programa que pida ingresar la coordenada de un punto en el plano, es decir dos valores enteros x e y (distintos a cero).
     * Posteriormente imprimir en pantalla en que cuadrante se ubica dicho punto. (1º Cuadrante si x > 0 Y y > 0 , 2º Cuadrante: x < 0 Y y > 0, etc.)
     */
    class Program
    {
        static void Main(string[] args)
        {
            int x, y;
            string linea;

            Console.WriteLine("Ingrese la coordenada x");
            linea = Console.ReadLine();
            x = Int32.Parse(linea);
            Console.WriteLine("Ingrese la coordenada y");
            linea = Console.ReadLine();
            y = Int32.Parse(linea);

            if (x > 0 && y > 0)
            {
                Console.WriteLine("El vector se localiza en el 1º Cuadrante");
            }else if (x < 0 && y > 0)
            {
                Console.WriteLine("El vector se localiza en el 2º Cuadrante");
            }else if(x < 0 && y < 0)
            {
                Console.WriteLine("El vector se localiza en el 3º Cuadrante");
            }
            else if (x < 0 && y < 0)
            {
                Console.WriteLine("El vector se localiza en el 4º Cuadrante");
            }
            Console.ReadKey();
        }
    }
}
