using System;

public class EjerciciosDeCasa5
{
	static void Main()
	{
		/*
		//.Imprimir numeros del 1 al 100
		Console.WriteLine("Inicio del bucle");
		for(int n=1; n<=100; n=n+1)
		{
			Console.WriteLine(n);
		}
		Console.WriteLine("Fin del bucle");
		
		
		
		//.Cargar 10 valores y que nos muestre la suma de todos los valores ingresados anterioremente
		int suma=0;
		for (int n=1; n<=10; n=n+1)
		{
			Console.WriteLine("Introduzca valor");
			string linea=Console.ReadLine();
			int valor=Int32.Parse(linea);
			suma=suma+valor;
		}
		Console.WriteLine("La suma total es: "+suma);
		
		
		//.Leer 10 notas de alumnos e informar cuántos tienen notas mayores o iguales a 7 y
		//cuantos menores
		int nota, sumaMay, sumaMen;
		sumaMay=0;
		sumaMen=0;
		for(int i=1; i<=10; i=i+1)
		{
			Console.WriteLine("Ingrese nota");
			string linea=Console.ReadLine();
			nota=Int32.Parse(linea);
			if (nota>=7)
			{
				sumaMay=sumaMay+1;
			}else
			{
				sumaMen=sumaMen+1;
			}
		}
		Console.WriteLine("Hay "+sumaMay+" notas altas");
		Console.WriteLine("Hay "+sumaMen+" notas bajas");
		
		
		
		//.Leer 10 numeros enteros y mostrar valores ingresados que sean múltiplos de 3 y cuantos de 5.
		//Contando con que puede haber algunos que lo sean tanto de 3 como de 5 también.
		int multiploAmb=0;
		int multiplo3=0;
		int multiplo5=0;
		for(int i=1; i<=10; i=i+1)
		{
			Console.WriteLine("Ingrese numero entero");
			string linea=Console.ReadLine();
			int numEntero=Int32.Parse(linea);
			if (multiplo3%3==0 && multiplo5%5==0)
			{
				multiploAmb=multiploAmb+1;
			}
			if(numEntero%3==0)
			{
				multiplo3=multiplo3+1;
			}else if (numEntero%5==0)
			{
				multiplo5=multiplo5+1;
			}
		}
		Console.WriteLine("Hay " +multiplo3+ " múltiplos de 3");
		Console.WriteLine("Hay " +multiplo5+ " múltiplos de 5");
		Console.WriteLine("Hay " +multiploAmb+ " múltiplos de 3 y de 5");
		
		
		
		//.Leer n números enteros y calcular la cantidad de valores mayores o iguales a 1000
		int n, numEntero, suma;
		string linea;
		suma=0;
		Console.WriteLine("¿Cuántos números enteros va a ingresar?");
		linea=Console.ReadLine();
		n=Int32.Parse(linea);
		for (int i=1; i<=n; i=i+1)
		{
			Console.WriteLine("Ingrese numero entero");
			linea=Console.ReadLine();
			numEntero=Int32.Parse(linea);
			if (numEntero>=1000)
			{
				suma=suma+1;
			}
		}
		Console.WriteLine("Cantidad de valores > ó = a 1000 " + suma);
		
		
		
		//.Ingresar n pares de datos, cada par de datos corresponde a la medida de la base
		//y la altura de un triangulo. Se infromará de:
		//	-de cada triángulo, la medida de su base, su altura y su superficie
		//	-la cantidad de triángulos cuya superficie es mayor a 12
		int n, baseT, alturaT, totalAreas;
		totalAreas=0;
		string linea;
		Console.WriteLine("¿Cuántos triángulos va a ingresar?");
		linea=Console.ReadLine();
		n=Int32.Parse(linea);
		for(int i=1; i<=n; i=i+1)
		{
			Console.WriteLine("Ingrese base del triángulo");
			linea=Console.ReadLine();
			baseT=Int32.Parse(linea);
			
			Console.WriteLine("Ingrese altura del triángulo");
			linea=Console.ReadLine();
			alturaT=Int32.Parse(linea);
			
			int area=baseT*alturaT;
			Console.WriteLine("Base: " +baseT+ " Altura: " +alturaT+" Superficie: "+area);
			
			if (area>12)
			{
				totalAreas=totalAreas+1;
			}
		}
		Console.WriteLine("Total triángulos con superficies mayores de 12: "+totalAreas);
		
		
		//.Carga de 10 numeros e imprimir la suma de los últimos 5 valores ingresados
		int suma=0;
		for(int i=1; i<=10; i++)
		{
			Console.WriteLine("Ingrese número");
			string linea=Console.ReadLine();
			int numero=Int32.Parse(linea);
			if (i>=5)
			{
				suma=suma+numero;
			}
		}
		Console.WriteLine("La suma de los últimos 5 valores es: "+suma);
		
		
		//.Mostrar la tabla de multiplicar del 5 (del 5 al 50)
		for(int i=1; i<=10; i++)
		{
			int x=5*i;
			Console.WriteLine(x);
		}
		
		
		//.Ingresar un valor del 1 al 10 y nos muestre la tabla de multiplicar del mismo
		//(los primeros 12 términos, es decir, llegar a multiplicar hasta por 12)
		Console.WriteLine("Ingrese número del 1 al 10");
		string linea=Console.ReadLine();
		int numero=Int32.Parse(linea);
		for(int i=1; i<=12; i++)
		{
			int x=numero*i;
			Console.WriteLine(x);
		}
		
		
		//.Leer los lados de n triángulos e informar:
		//	-que tipo de triángulo es: equilátero (todos sus lados iguales), isósceles
		//	(dos lados iguales) o escaleno(ningun lado igual)
		//	-cantidad de triángulos de cada tipo
		//	-tipo de triángulo que posee menor cantidad
		string linea;
		int n, ladoA, ladoB, ladoC;
		int suma1=0;
		int suma2=0;
		int suma3 =0;
		Console.WriteLine("¿Cuántos triángulos va a ingresar?");
		linea=Console.ReadLine();
		n=Int32.Parse(linea);
		for(int i=1; i<=n; i++)
		{
			Console.WriteLine("Ingrese primer lado");
			linea=Console.ReadLine();
			ladoA=Int32.Parse(linea);
			Console.WriteLine("Ingrese segundo lado");
			linea=Console.ReadLine();
			ladoB=Int32.Parse(linea);
			Console.WriteLine("Ingrese tercer lado");
			linea=Console.ReadLine();
			ladoC=Int32.Parse(linea);
			
			if (ladoA==ladoB && ladoA==ladoC)
			{
				Console.WriteLine("El triángulo es equilátero");
				suma1++;
			}else if (ladoA==ladoB || ladoA==ladoC || ladoB==ladoC)
			{
				Console.WriteLine("El triángulo es isósceles");
				suma2++;
			}else
			{
				Console.WriteLine("El triángulo es escaleno");
				suma3++;
			}
			Console.WriteLine("Siguiente triángulo");
		}	
		Console.WriteLine("Triángulos equiláteros: "+suma1);
		Console.WriteLine("Triángulos isósceles: "+suma2);
		Console.WriteLine("Triángulos escalenos: "+suma3);
			
		if (suma1==suma2 && suma2==suma3)
		{
			Console.WriteLine("Hay el mismo numero de triángulos");
		}
		else if (suma1>suma2 && suma1>suma3)
		{
			Console.WriteLine("Hay más Triángulos equiláteros");
		}else if(suma2>suma3)
		{
			Console.WriteLine("Hay más Triángulos isósceles");
		}else 
		{
			Console.WriteLine("Hay más Triángulos escalenos");
		}
		
		
		
		//.Ingresar coordenadas (x,y) que representan puntos en el plano. Informar cuántos 
		//puntos se han ingreasdo en el primer, segundo, tercer y cuarto cuadrante
		//Al comenzar el programa se pide que se ingrese la cantidad de puntos a ingresar
		for(int i=1; i<=4; i++)
		{
			Console.WriteLine("Ingrese coordenada x");
			string linea=Console.ReadLine();
			int x=Int32.Parse(linea);
			Console.WriteLine("Ingrese coordenada y");
			string linea=Console.ReadLine();
			int y=Int32.Parse(linea);
		}
		*/
		
		//.Carga de 10 valores enteros
		//	-la cantidad de valores ingre4sados negativos
		//	-la  cantida de calores ingresados negativos
		//	-la cantidad de multiplos de 15
		//	-el valor acumulado de los numeros ingresados que son pares
		int pos=0;
		int neg=0;
		int multiplo15=0;
		int suma=0;
		for(int i=1; i<=10; i++)
		{
			Console.WriteLine("Ingrese valor entero");
			string linea=Console.ReadLine();
			int valor=Int32.Parse(linea);
			if (valor<1)
			{
				neg=neg+1;
			}else
			{
				pos=pos+1;
				suma=suma+valor;
			}
			if (valor%15==0)
			{
				multiplo15=multiplo15+1;
			}
		}
		Console.WriteLine("Cantidad de valores negativos: "+neg);
		Console.WriteLine("Cantidad de valores positivos: "+pos);
		Console.WriteLine("Cantidad de múltiplos de 15: "+multiplo15);
		Console.WriteLine("Valor acumulado de numeros pares: "+suma);
		
		
		//.Se cuenta con la siguiente información:
		//	-las edades de 50 estudiantes del turno de mañana
		//	-las edades de 60 estudiantes del turno de tarde
		//	-las edades de 110 estudiantes del turno de noche
		//Las edades de cada estudiante deben ingresarse por teclado
		//	-obtener el promedio de las edades de cada turno (tres promedios)
		//	-imprimir dichos promedios de cada turno
		//	-mostrar por pantalla un mensaje que indique cu7al de los tres turnos 
		//	tiene un promeedio de edades mayor
		
		
		
		
		
		
		
		
		
		
		
	}
}