﻿using System;

namespace Leccion06_Ejercicio01
{
    /**
     * Realizar un programa que lea por teclado dos números, si el primero es mayor al segundo informar su suma y diferencia, en caso contrario informar el producto y la división del primero respecto al segundo.
     */
    class Program
    {
        static void Main(string[] args)
        {
            float num1, num2;
            string linea;

            Console.WriteLine("Ingrese primer número");
            linea = Console.ReadLine();
            num1 = float.Parse(linea);

            Console.WriteLine("Ingrese segundo número");
            linea = Console.ReadLine();
            num2 = float.Parse(linea);

            if (num1 > num2)
            {
                float suma = num1 + num2;
                Console.WriteLine("La suma de ambos es: " + suma);
                Console.ReadKey();
            }
            else
            {
                float producto = num1 * num2;
                float division = num1 / num2;
                Console.WriteLine("El producto de ambos es: " + producto + " y la división es: " + division);
                Console.ReadKey();
            }
        }
    }
}
