﻿using System;

namespace Leccion07_Ejercicio01
{
    /**
     * Se cargan por teclado tres números distintos. Mostrar por pantalla el mayor de ellos. 
     */
    class Program
    {
        static void Main(string[] args)
        {
            int num1, num2, num3;
            string linea;

            Console.WriteLine("Ingrese primer número");
            linea = Console.ReadLine();
            num1 = Int32.Parse(linea);
            Console.WriteLine("Ingrese segundo número");
            linea = Console.ReadLine();
            num2 = Int32.Parse(linea);
            Console.WriteLine("Ingrese tercer número");
            linea = Console.ReadLine();
            num3 = Int32.Parse(linea);

            if (num1>num2)
            {
                if (num1>num3)
                {
                    Console.WriteLine("El númer mayor es: " + num1);
                }
            }
            else if(num2>num3)
            {
                 Console.WriteLine("El númer mayor es: " + num2);
            }
            else
            {
                Console.WriteLine("El númer mayor es: " + num3);
            }
        }
    }
}
