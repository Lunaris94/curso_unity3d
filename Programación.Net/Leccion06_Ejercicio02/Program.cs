﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Leccion06_Ejercicio02
{
    /**
     * Se ingresan tres notas de un alumno, si el promedio es mayor o igual a siete mostrar un mensaje "Promocionado".
     */
    class Program
    {
        static void Main(string[] args)
        {
            float nota1, nota2, nota3;
            string linea;

            Console.WriteLine("Ingrese primera nota");
            linea = Console.ReadLine();
            nota1 = float.Parse(linea);
            Console.WriteLine("Ingrese segunda nota");
            linea = Console.ReadLine();
            nota2 = float.Parse(linea);
            Console.WriteLine("Ingrese tercera nota");
            linea = Console.ReadLine();
            nota3 = float.Parse(linea);

            float suma = nota1 + nota2 + nota3;
            float media = suma / 3;

            if (media >= 7)
            {
                Console.WriteLine("Promocionado");
            }
            Console.ReadKey();
        }
    }
}
