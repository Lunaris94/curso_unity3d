﻿using System;

namespace Leccion07_Ejercicio04
{
    /**
     * Un postulante a un empleo, realiza un test de capacitación, se obtuvo la siguiente información: cantidad total de preguntas que se le realizaron y la cantidad de preguntas que contestó correctamente. Se pide confeccionar un programa que ingrese los dos datos por teclado e informe el nivel del mismo según el porcentaje de respuestas correctas que ha obtenido, y sabiendo que:
     *      Nivel máximo:	Porcentaje>=90%.
     *      Nivel medio:	Porcentaje>=75% y <90%.
     *      Nivel regular:	Porcentaje>=50% y <75%.
     *      Fuera de nivel:	Porcentaje<50%.
     */

    class Program
    {
        static void Main(string[] args)
        {
            int pregReal, pregAcert;
            string linea;

            Console.WriteLine("Ingrese el total de preguntas realizadas");
            linea = Console.ReadLine();
            pregReal = Int32.Parse(linea);
            Console.WriteLine("Ingrese el total de preguntas acertadas");
            linea = Console.ReadLine();
            pregAcert = Int32.Parse(linea);

            int porc = (pregAcert * 100) / pregReal;

            if (porc >= 90)
            {
                Console.WriteLine("Ha obtenido el nivel máximo");
            }else if(porc>=75 && porc < 90)
            {
                Console.WriteLine("Ha obtenido el nivel medio");
            }else if (porc >= 50 && porc < 75)
            {
                Console.WriteLine("Ha obtenido el nivel regular");
            }else if (porc < 50)
            {
                Console.WriteLine("Está usted fuera de nivel");
            }
        }
    }
}
