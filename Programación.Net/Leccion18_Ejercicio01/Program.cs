﻿using System;

namespace Leccion18_Ejercicio01
{
    /*
     * Cargar un vector de n elementos. imprimir el menor y un mensaje si se repite dentro del vector.
     */
    class Vector
    {
        public int[] vector;
        int menor;
        static void Main(string[] args)
        {
            Vector vectorUno = new Vector();
            vectorUno.CargarDatos();
            vectorUno.BuscarMenor();
            vectorUno.BuscarRepetido();
        }
        public void CargarDatos()
        {
            Console.WriteLine("Número de elementos");
            int n = int.Parse(Console.ReadLine());
            vector = new int[n];
            for(int i = 0; i < n; i++)
            {
                Console.WriteLine("Introducir valor");
                int valor = int.Parse(Console.ReadLine());
                vector[i] = valor;
            }
        }
        public void BuscarMenor()
        {
            menor=vector[0];
            for(int i = 0; i < vector.Length; i++)
            {
                if (vector[i] < menor)
                {
                    menor = vector[i];
                }
            }
            Console.WriteLine("Menor: " + menor);
        }
        public void BuscarRepetido()
        {
            int repe = 0;
            for(int i=0; i < vector.Length; i++)
            {
                if (vector[i] == menor)
                {
                    repe++;
                }
            }
            Console.WriteLine("El menor " + menor + " se repite " + repe + " veces");
        }
    }
}
