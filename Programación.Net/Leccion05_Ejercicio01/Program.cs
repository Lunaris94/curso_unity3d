﻿using System;

namespace Leccion05_Ejercicio01
{
    /**
     * Realizar la carga del lado de un cuadrado, mostrar por pantalla el perímetro del mismo (El perímetro de un cuadrado se calcula multiplicando el valor del lado por cuatro) 
     */

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int lado;
            string linea;

            Console.WriteLine("Ingrese el lado del cuadrado");
            linea = Console.ReadLine();
            lado = Int32.Parse(linea);

            int per = lado * 4;
            Console.WriteLine("El perímetro del cuadrado es: " + per);

            Console.ReadKey();

        }
    }
}
