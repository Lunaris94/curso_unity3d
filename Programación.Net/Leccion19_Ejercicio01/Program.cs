﻿using System;

namespace Leccion19_Ejercicio01
{
    /*
     * Cargar un vector de n elementos de tipo entero. Ordenar posteriormente el vector.
     */
    class Numeros
    {
        int [] numerosOrd;
        int n, valor;
        static void Main(string[] args)
        {
            Numeros listaUno = new Numeros();

            listaUno.CargarValores();
            listaUno.OrdenarValores();
            listaUno.MostrarLista();
        }
        public void CargarValores()
        {
            Console.WriteLine("Número de valores");
            n = int.Parse(Console.ReadLine());
            numerosOrd = new int[n];
            for(int i = 0; i < n; i++)
            {
                Console.WriteLine("Introducir valor");
                valor = int.Parse(Console.ReadLine());
                numerosOrd[i] = valor;
            }
        }
        public void OrdenarValores()
        {
            int aux;
            for(int j = 0; j < numerosOrd.Length; j++)
            {
                for(int i = 0; i < numerosOrd.Length - 1; i++)
                {
                    if (numerosOrd[i] > numerosOrd[i+1])
                    {
                        aux = numerosOrd[i];
                        numerosOrd[i] = numerosOrd[i+1];
                        numerosOrd[i+1] = aux;
                    }
                }
            }
            //MostrarLista(numerosOrd);
        }
        public void MostrarLista()
        {
            Console.WriteLine();
            for(int i = 0; i < numerosOrd.Length; i++)
            {
                Console.WriteLine(numerosOrd[i]);
            }
        }
    }
}
