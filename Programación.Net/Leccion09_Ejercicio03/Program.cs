﻿using System;

namespace Leccion09_Ejercicio03
{
    /*
     * En una empresa trabajan n empleados cuyos sueldos oscilan entre $100 y $500, realizar un programa que lea los sueldos que cobra cada empleado e informe cuántos empleados cobran entre $100 y $300 y cuántos cobran más de $300. Además el programa deberá informar el importe que gasta la empresa en sueldos al personal. 
     */
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ingrese el número de empleados");
            string linea = Console.ReadLine();
            int n = Int32.Parse(linea);

            float sueldo = 0;
            int mas = 0;
            int menos = 0;
            float suma=0; 

            int i = 0;
            while (i < n)
            {
                Console.WriteLine("Ingrese sueldo");
                linea = Console.ReadLine();
                sueldo = float.Parse(linea);

                suma = suma + sueldo;

                if (sueldo >= 100 && sueldo < 300)
                {
                    menos = menos + 1;
                } 
                else if (sueldo >= 300)
                {
                    mas = mas + 1;
                }
                i++;
            }
            Console.WriteLine("Hay "+ menos + " empleados que cobran menos de 300 euros");
            Console.WriteLine("Hay " + mas + " empleados que cobran mas de 300 euros");
            Console.WriteLine("Total gastos empresa: " + suma);
        }
    }
}
