﻿using System;
using System.Collections.Generic;

public class OrdenarComoHumano
{
    // Controlar qué entra y qué sale en las funciones

	public static void Main()
	{
		List<int> numeros = new List<int>();
		numeros.Add(4);		// numeros[0] = 4
		numeros.Add(5);
		numeros.Add(70);
		numeros.Add(-2);
        numeros.Add(60);
        numeros.Add(-3);
        numeros.Add(2);
		numeros.Add(10);
        numeros.Add(20);

        Console.WriteLine("La lista es: " + numeros);
		MostrarLista(numeros);
        //BuscarMenor(numeros);
        //Ordenar(numeros);
        //MostrarLista(numOrdenados);
        List <int> numOrd = Ordenar(numeros);
        MostrarLista(numOrd);

    }
    public static List<int> Ordenar(List<int> lista)
    {
        //TODO: Ordenar, buscando el más pequeño, sacándolo y poniendo la lista ordenada

        int menor = 0;
        List<int> numOrdenados = new List<int>();

       /* menor = BuscarMenor(lista);
        numOrdenados.Add(menor);
        MostrarLista(numOrdenados);
        lista.Remove(menor);
        MostrarLista(lista);*/

        while (0 < lista.Count)
        {
            Console.WriteLine("El tamaño de la lista es de: " + lista.Count);            
            menor = BuscarMenor(lista);
            numOrdenados.Add(menor);
            MostrarLista(numOrdenados);
            lista.Remove(menor);
            MostrarLista(lista);

            //TODO:guardar lista.Count actualizado

            Console.WriteLine("FIN DE LA ORDENACIÓN");

        }
        //MostrarLista(numOrdenados);
        return numOrdenados;
    }

    public static void MostrarLista(List<int> lista)
    {
		Console.WriteLine("Lista: ");
		for (int i = 0; i < lista.Count; i++)
        {
			Console.WriteLine(lista[i]);	// numeros[3]
        }
        Console.WriteLine("  FIN");
    }

    public static int BuscarMenor(List<int> lista)
    {
        int menor = lista[0];
        for (int i = 0; i < lista.Count; i++)
        {
            if (lista[i] < menor)
            {
                menor = lista[i];
            }
        }
        Console.WriteLine("Número menor: " + menor);
        return menor;
    }

}
