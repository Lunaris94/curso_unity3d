﻿using System;

namespace Leccion05_Ejercicio04
{
    /**
     * Se debe desarrollar un programa que pida el ingreso del precio de un artículo y la cantidad que lleva el cliente. Mostrar lo que debe abonar el comprador.
     */
    class Program
    {
        static void Main(string[] args)
        {
            float precio, cantidad;
            string linea;

            Console.WriteLine("Ingrese el precio del artículo");
            linea = Console.ReadLine();
            precio = float.Parse(linea);
            Console.WriteLine("Ingrese la cantidad de artículos");
            linea = Console.ReadLine();
            cantidad = float.Parse(linea);

            float total = cantidad * precio;
            Console.WriteLine("Debe abonar: " + total);
            Console.ReadKey();
        }
    }
}
