﻿using System;
using System.Collections.Generic;

public class OrdenarComoHumano
{
	public static void Main()
	{
		List<int> numeros = new List<int>();
		numeros.Add(4);			//numeros[0] = 4;
		numeros.Add(5);
		numeros.Add(3);
		numeros.Add(1);
		numeros.Add(2);

		MostrarLista(numeros);
		Menor(numeros);
	}

	public static void MostrarLista(List<int> lista)
	{
		Console.WriteLine("Lista: ");
		for (int i = 0; i < lista.Count; i++)
		{
			Console.Write(lista[i] + ", ");    //numeros[3]
		}
		Console.WriteLine("FIN");
	}

	public static void Menor(List<int> lista)
	{
		int menor = lista[0];
		for (int i = 0; i < lista.Count; i++)
		{
			if (lista[i] < menor)
			{
				menor = lista[i];
			}
			Console.WriteLine("Número menor: " + menor);
		}
	}
}
