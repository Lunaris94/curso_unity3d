﻿using System;

namespace Leccion22_Ejercicio02
{
    /*
     * Crear una matriz de n * m filas (cargar n y m por teclado) Imprimir los cuatro valores que se encuentran en los vértices de la misma (mat[0][0] etc.) 
     */
    class Program
    {
        int[,] mat;
        int filas, colum;
        static void Main(string[] args)
        {
            Program matriz = new Program();
            matriz.CargarDatos();
            matriz.MostrarMatriz();
            matriz.MostrarEsquinas();
        }
        public void CargarDatos()
        {
            Console.WriteLine("Número de datos por fila: ");
            filas = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Número de datos por columna: ");
            colum = Int32.Parse(Console.ReadLine());
            mat = new int[filas, colum];

            int aux;
            for (int f = 0; f < filas; f++)
            {
                Console.WriteLine("Siguiente columna");
                for (int c = 0; c < colum; c++)
                {
                    Console.WriteLine("Introduzca dato");
                    aux = Int32.Parse(Console.ReadLine());
                    mat[f, c] = aux;
                }
            }
        }
        public void MostrarMatriz()
        {
            Console.WriteLine("Mostrando matriz");
            for (int f = 0; f < filas; f++)
            {
                for (int c = 0; c < colum; c++)
                {
                    Console.Write(mat[f, c] + " ");
                }
                Console.WriteLine();
            }
        }
        public void MostrarEsquinas()
        {
            Console.WriteLine("Mostrando valores de esquinas");
            Console.WriteLine("Esquina superior izquierda: " + mat[0, 0]);
            Console.WriteLine("Esquina superior derecha: " + mat[0, colum-1]);
            Console.WriteLine("Esquina inferior izquierda: " + mat[filas-1, 0]);
            Console.WriteLine("Esquina inferior derecha: " + mat[filas-1, colum-1]);
            //En vez de utilizar el tamaño de las filas y las columnas que se ha puesto al principio por teclado, también podríamos utilizar el método GetLength (0 ó 1)-1.
        }
    }
}
