﻿using System;

namespace Leccion10_Ejercicio08
{
    /*
     * Se cuenta con la siguiente información:
     * Las edades de 50 estudiantes del turno mañana.
     * Las edades de 60 estudiantes del turno tarde.
     * Las edades de 110 estudiantes del turno noche.
     * Las edades de cada estudiante deben ingresarse por teclado.
     * a) Obtener el promedio de las edades de cada turno (tres promedios)
     * b) Imprimir dichos promedios (promedio de cada turno)
     * c) Mostrar por pantalla un mensaje que indique cual de los tres turnos tiene un promedio de edades mayor. 
     */
    class Program
    {
        static void Main(string[] args)
        {
            int edadMañ = 0;
            int edadTar = 0;
            int edadNoc = 0;
            int edadMañTotal = 0;
            int edadTarTotal = 0;
            int edadNocTotal = 0;
            string linea;
            int promMañ = 0;
            int promTar = 0;
            int promNoc = 0;

            Console.WriteLine("Turno de mañana");
            int n = 3;
            for(int i = 0; i < n; i++)
            {
                Console.WriteLine("Introduzca edad");
                linea = Console.ReadLine();
                edadMañ = Int32.Parse(linea);
                edadMañTotal = edadMañTotal + edadMañ;
            }
            promMañ = edadMañTotal / n;
            Console.WriteLine("Promedio de edades turno mañana" + promMañ);

            Console.WriteLine("Turno de tarde");
            int m = 3;
            for(int i = 0; i < m; i++)
            {
                Console.WriteLine("Introduzca edad");
                linea = Console.ReadLine();
                edadTar = Int32.Parse(linea);
                edadTarTotal = edadTarTotal + edadTar;
            }
            promTar = edadTarTotal / m;
            Console.WriteLine("Promedio de edades turno tarde" + promTar);

            Console.WriteLine("Turno de noche");
            int x = 3;
            for(int i = 0; i < x; i++)
            {
                Console.WriteLine("Introduzca edad");
                linea = Console.ReadLine();
                edadNoc = Int32.Parse(linea);
                edadNocTotal = edadNocTotal + edadNoc;
            }
            promNoc = edadTarTotal / x;
            Console.WriteLine("Promedio de edades turno noche" + promNoc);

            if (promMañ > promTar && promMañ > promNoc)
            {
                Console.WriteLine("El turno de mañana tiene el promedio mayor"); 
            }
            else if (promTar > promNoc)
            {
                Console.WriteLine("El turno de tarde tiene el promedio mayor");
            }
            else
            {
                Console.WriteLine("El turno de noche tiene el promedio mayor");
            }
        }
    }
}
