﻿using System;
using System.Numerics;

namespace Leccion15_Ejercicio01
{
    /*
     * Desarrollar un programa que permita ingresar un vector de 8 elementos, e informe:
     * El valor acumulado de todos los elementos del vector.
     * El valor acumulado de los elementos del vector que sean mayores a 36.
     * Cantidad de valores mayores a 50.
     */
    class PruebaVector
    {
        public int[] vector;
        static void Main(string[] args)
        {
            PruebaVector pV = new PruebaVector();
            pV.CargarDatos();
            pV.ValorAcumulado();
            pV.ValoresMayores();

        }
        public void CargarDatos()
        {
            vector = new int[8];
            int valor;
            for(int i = 0; i < vector.Length; i++)
            {
                Console.WriteLine("Ingrese valor");
                valor = Int32.Parse(Console.ReadLine());
                vector[i]=valor;
            }
        }
        public void ValorAcumulado()
        {
            int valorAcum = 0;
            for (int i=0;i<vector.Length;i++)
            {
                valorAcum = valorAcum + vector[i];
            }
            Console.WriteLine("El valor acumulado es" + valorAcum);
        }
        public void ValorAcumuladoMayor()
        {
            int valorAcum = 0;
            for(int i=0; i < vector.Length; i++)
            {
                if (vector[i] >= 36)
                {
                    valorAcum = valorAcum + vector[i];
                }
            }
            Console.WriteLine("El valor acumulado mayor de 36 es " + valorAcum);
        }
        public void ValoresMayores()
        {
            int mayores = 0;
            for(int i = 0; i < vector.Length; i++)
            {
                if (vector[i] >= 50)
                {
                    mayores++;
                }
            }
            Console.WriteLine("Hay " + mayores+ " valores mayores de 50");
        }
    }
}
