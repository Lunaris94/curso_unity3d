using System;

public class EjerciciosDeCasa3
{
	static void Main()
	{
		/*.Pedir 3 notas de alumno y calcular promedio. Imprimir uno de estos mensajes:
			Si Promedio es >=7 "Promocionado"
			Si Promedio es >=4 y <7 "Regular"
			Si Promedio es <4 "Reprobado"
		float notaA, notaB, notaC, promedio;
		string linea;
		Console.WriteLine("Ingrese la primera nota");
		linea=Console.ReadLine();
		notaA = float.Parse(linea);
		Console.WriteLine("Ingrese la segunda nota");
		linea=Console.ReadLine();
		notaB = float.Parse(linea);
		Console.WriteLine("Ingrese la tercera nota");
		linea=Console.ReadLine();
		notaC = float.Parse(linea);
		
		promedio = (notaA + notaB + notaC)/3;
		
		if (promedio >=7)
		{
			Console.WriteLine("Promocionado");
		}else if(promedio >=4)
		{
			Console.WriteLine("Regular");
		}else 
		{
			Console.WriteLine("Reprobado");
		}
		
		
		
		//.Cargar tres números distintos y mostrar cual de ellos el mayor
		int numA, numB, numC;
		string linea;
		Console.WriteLine("Ingrese el primer número");
		linea = Console.ReadLine();
		numA = Int32.Parse(linea);
		Console.WriteLine("Ingrese el segundo número");
		linea = Console.ReadLine();
		numB = Int32.Parse(linea);
		Console.WriteLine("Ingrese el tercer número");
		linea = Console.ReadLine();
		numC = Int32.Parse(linea);
		
		if (numA > numB)
		{
			if (numA > numC)
			{
			Console.WriteLine("El primer número es el mayor: " + numA);
			}else 
			{
				Console.WriteLine("El tercer numero es el mayor: " + numC);
			}
		}else if (numB > numC)
		{
			Console.WriteLine("El segundo numero es el mayor: " + numB);
		}else 
		{
			Console.WriteLine("El tercer numero es el mayor: " + numC);
		}
		
		
		
		//.Ingresar valor entero, mostrar leyenda que indique si es positivo, nulo o negativo.
		int valor;
		string linea;
		Console.WriteLine("Ingrese un valor entero");
		linea = Console.ReadLine();
		valor = Int32.Parse(linea);
		
		if (valor==0)
		{
			Console.WriteLine("El valor " + valor + " es nulo");
		}else if (valor >= 1)
		{
			Console.WriteLine("El valor " + valor + " es positivo");
		}else
		{
			Console.WriteLine("El valor " + valor + " es negativo");
		}
		
		
		
		//.Cargar numero de hasta 3 cifras y mostrar un mensaje indicando si tiene una, dos o tres 
		//cifras. Mostrar error si tiene más cifras
		int numero;
		string linea;
		Console.WriteLine("Ingrese un número");
		linea = Console.ReadLine();
		numero = Int32.Parse(linea);

		if (numero <10)
		{
			Console.WriteLine("El número tiene una cifra");
		}else if (numero <100)
		{
			Console.WriteLine("El número tiene dos cifras");
		}else if (numero <1000)
		{
			Console.WriteLine("El número tiene tres cifras");
		}else
		{
			Console.WriteLine("Error al introducir valor");
		}
		
		
		
		//.Un postulante realiza un test de capacitación. Se obtiene la siguiente información: total
		//preguntas realizadas y cantidad de preguntas acertadas. Crear un programa que ingrese los 
		//datos e informe del nivel según el porcentaje de respuestas correctas, sabiendo que:
		//	Nivel máximo: Porcentaje >=90%
		//	Nivel medio: Porcentaje >=75% y <90%
		//	Nivel regular: Porcentaje >=50% y <75%
		//	Fuera de nivel: Porcentaje <50%
		int preg, pregAcer, porcent;
		string linea;
		Console.WriteLine("Ingrese el total de preguntas realizadas");
		linea = Console.ReadLine();
		preg = Int32.Parse(linea);
		Console.WriteLine("Ingrese el total de preguntas acertadas");
		linea = Console.ReadLine();
		pregAcer = Int32.Parse(linea);
		
		porcent = (pregAcer*100)/preg;
		if (porcent>=90)
		{
			Console.WriteLine("Nivel máximo: " + porcent+"%");
		}else if (porcent>=75)
		{
			Console.WriteLine("Nivel medio: " + porcent+"%");
		}else if (porcent>=50)
		{
			Console.WriteLine("Nivel regular: " + porcent+"%");
		}else
		{
			Console.WriteLine("Fuera de nivel: " + porcent+"%");
		}
		
		
		
		//.confeccionar un programa que lea 3 dígitos y que nos diga cual es el mayor
		int valor1, valor2, valor3;
		string linea;
		Console.WriteLine("Ingrese el primer dígito");
		linea = Console.ReadLine();
		valor1 = Int32.Parse(linea);
		Console.WriteLine("Ingrese el segundo dígito");
		linea = Console.ReadLine();
		valor2 = Int32.Parse(linea);
		Console.WriteLine("Ingrese el tercer dígito");
		linea = Console.ReadLine();
		valor3 = Int32.Parse(linea);
		
		if (valor1>valor2 && valor1>valor3)
		{
			Console.WriteLine("El primer valor introducido es mayor: " + valor1);
		}else if (valor2>valor3)
		{
			Console.WriteLine("El segundo valor introducido es mayor: " + valor2);
		}else
		{
			Console.WriteLine("El tercer valor introducido es mayor: " + valor3);
		}*/
		
		
		//.Cargar fecha (día, mes y año) y mostrar mensaje si corresponde con el 
		//primer trimestre del año (enero, febrero o mmarzo)
		int dia, mes, año;
		string linea;
		Console.WriteLine("Ingrese día");
		linea = Console.ReadLine();
		dia = Int32.Parse(linea);
		Console.WriteLine("Ingrese mes");
		linea = Console.ReadLine();
		mes = Int32.Parse(linea);
		Console.WriteLine("Ingrese año");
		linea = Console.ReadLine();
		año = Int32.Parse(linea);
		Console.WriteLine("Ha introducida la fecha: " + dia + "/" + mes + "/" + año);
		
		if (mes==1 || mes==2 || mes==3)
		{
			Console.WriteLine("La fecha introducida corresponde al primer trimestre");
		}else
		{
			Console.WriteLine("La fecha introducida no corresponde al primer trimestre");
		}
	}
	
	
	
	
	
	
	
	
	
	
	
}