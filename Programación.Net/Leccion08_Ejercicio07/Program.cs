﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Leccion08_Ejercicio07
{
    /*
     * Escribir un programa en el cual: dada una lista de tres valores numéricos distintos se calcule e informe su rango de variación (debe mostrar el mayor y el menor de ellos) 
     */
    class Program
    {
        static void Main(string[] args)
        {
            List<int> valores = new List<int>();
            string linea;

            Console.WriteLine("Ingrese el primer valor");
            linea = Console.ReadLine();
            int valor1 = Int32.Parse(linea);
            valores.Add(valor1);
            Console.WriteLine("Ingrese el segundo valor distinto");
            linea = Console.ReadLine();
            int valor2 = Int32.Parse(linea);
            valores.Add(valor2);
            Console.WriteLine("Ingrese el tercer valor distinto");
            linea = Console.ReadLine();
            int valor3 = Int32.Parse(linea);
            valores.Add(valor3);

            int max=ValorMaximo(valores);
            int min=ValorMinimo(valores);
            Mostrar("máximo", max);
            Mostrar("mínimo", min);
            RangoVariacion(max, min);

            Console.ReadKey();
        }
        public static int ValorMaximo(List<int> lista)
        {
            int max = 0;
            for(int i = 0; i < lista.Count; i++)
            {
                if(lista[i] > max)
                {
                    max = lista[i];
                }
            }
            return max;
        }
        public static int ValorMinimo(List<int> lista)
        {
            int min = lista[0];
            for (int i = 0; i < lista.Count; i++)
            {
                if (lista[i]<min)
                {
                    min = lista[i];
                }
            }
            return min;
        }
        public static void RangoVariacion(int max, int min)
        {
            int valor = max - min;
            Console.WriteLine("El rango de variación es de " + valor);
        }
        public static void Mostrar(string aux, int valor)
        {
            Console.WriteLine("El valor " + aux + " es de " + valor);
        }
    }
}
