﻿using System;
using System.Collections.Generic;

public class OrdenarComoHumano
{
	public static void Main()
	{
		List<int> numeros = new List<int>();
		numeros.Add(2);
		numeros.Add(1);
		numeros.Add(5);
		numeros.Add(4);
		numeros.Add(3);

		MostrarLista(numeros);
		Ordenar(numeros);
		//BuscarMenor(numeros);
		
		//List<int> numOrdenados = Ordenar(numeros);
		//MostrarLista(numOrdenados);

	}

	public static void MostrarLista(List<int> lista) 
	{
		Console.WriteLine("Inicio");
		for(int i = 0; i < lista.Count; i++)
        {
			Console.WriteLine(lista[i]);
        }
		Console.WriteLine("Fin");
	}

	public static List<int> Ordenar(List<int> lista)
	{
		for (int i = 0; i < lista.Count; i++)
		{
			int menor = BuscarMenor(lista, i);
			lista.Insert(0, menor);
			lista.Remove(lista[i]);
			MostrarLista(lista);
		}
		return lista;
	}

	public static int BuscarMenor(List<int> lista)
	{
		int menor = lista[0];
		for (int j=0; j < lista.Count; j++)
		{
			if (lista[j] < menor)
			{
				menor = lista[j];
			}
		}
		Console.WriteLine("El número menor es: " + menor);
		return menor;
	}
}
