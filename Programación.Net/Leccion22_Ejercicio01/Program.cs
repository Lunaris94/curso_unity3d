﻿using System;

namespace Leccion22_Ejercicio01
{
    /*
     * Crear una matriz de n * m filas (cargar n y m por teclado) Intercambiar la primer fila con la segundo. Imprimir luego la matriz. 
     */
    class Program
    {
        int[,] mat;
        int filas, colum;
        static void Main(string[] args)
        {
            Program matriz = new Program();
            matriz.CargarDatos();
            matriz.MostrarMatriz();
            matriz.IntercambioFilas();
            matriz.MostrarMatriz();
        }
        public void CargarDatos()
        {
            Console.WriteLine("Número de datos por fila: ");
            filas = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Número de datos por columna: ");
            colum = Int32.Parse(Console.ReadLine());
            mat = new int[filas, colum];

            int aux;
            for(int f=0; f < filas; f++)
            {
                Console.WriteLine("Siguiente columna");
                for(int c=0; c < colum; c++)
                {
                    Console.WriteLine("Introduzca dato");
                    aux = Int32.Parse(Console.ReadLine());
                    mat[f, c] = aux;
                }
            }
        }
        public void MostrarMatriz()
        {
            Console.WriteLine("Mostrando matriz");
            for (int f = 0; f < filas;f++)
            {
                for(int c = 0; c < colum; c++)
                {
                    Console.Write(mat[f, c]+" ");
                }
                Console.WriteLine();
            }
        }
        public void IntercambioFilas()
        {
            int aux;
            for (int f = 0; f < filas; f++)
            {
                for (int c = 0; c < colum; c++)
                {
                    if (f == 0)
                    {
                        aux = mat[f, c];
                        mat[f, c] = mat[f + 1, c];
                        mat[f + 1, c] = aux;
                    }
                }
            }

        }
    }
}
