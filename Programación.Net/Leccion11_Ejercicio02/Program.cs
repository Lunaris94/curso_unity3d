﻿using System;

namespace Leccion11_Ejercicio02
{
    /*
     * En un banco se procesan datos de las cuentas corrientes de sus clientes. De cada cuenta corriente se conoce: número de cuenta y saldo actual. El ingreso de datos debe finalizar al ingresar un valor negativo en el número de cuenta.
     * Se pide confeccionar un programa que lea los datos de las cuentas corrientes e informe:
     * a)De cada cuenta: número de cuenta y estado de la cuenta según su saldo, sabiendo que:
     *      Estado de la cuenta	'Acreedor' si el saldo es >0.
     *                          'Deudor' si el saldo es <0.
     *                          'Nulo' si el saldo es =0.
     *b) La suma total de los saldos acreedores.
     */
    class Program
    {
        static void Main(string[] args)
        {
            //Siempre hay que pensar en negativo(en este caso, que el bucle continue MIENTRAS QUE EL NÚMERO SEA MAYOR O IGUAL A 0)
            string linea;
            int numCuenta, saldoAct;
            int sumaTotal = 0;

            do
            {
                Console.WriteLine("Introduzca número de cuenta");
                linea = Console.ReadLine();
                numCuenta = Int32.Parse(linea);
                if (numCuenta >= 0)
                {

                    Console.WriteLine("Introduzca saldo actual");
                    linea = Console.ReadLine();
                    saldoAct = Int32.Parse(linea);

                    Console.WriteLine("Cuenta nº: " + numCuenta);
                    if (saldoAct == 0)
                    {
                        Console.WriteLine("Nulo");

                    }
                    else if (saldoAct > 0)
                    {
                        Console.WriteLine("Acreedor");
                        sumaTotal = sumaTotal + saldoAct;
                    }
                    else
                    {
                        Console.WriteLine("Deudor");
                    }
                }
                
            } while (numCuenta >= 0);

            Console.WriteLine("La suma total de los acreedores es: " + sumaTotal);
        }
    }
}
