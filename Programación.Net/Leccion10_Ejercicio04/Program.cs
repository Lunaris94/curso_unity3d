﻿using System;

namespace Leccion10_Ejercicio04
{
    /*
     * Confeccionar un programa que permita ingresar un valor del 1 al 10 y nos muestre la tabla de multiplicar del mismo (los primeros 12 términos)
     * Ejemplo: Si ingreso 3 deberá aparecer en pantalla los valores 3, 6, 9, hasta el 36.
     */
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introduzca un valor");
            string linea = Console.ReadLine();
            int n = Int32.Parse(linea);
            Console.WriteLine("Tabla de multiplicar del " + n);
            
            int x = 0;
            for(int i=0; i<=10; i++)
            {
                Console.WriteLine(x);
                x = x + n;
            }
        }
    }
}
