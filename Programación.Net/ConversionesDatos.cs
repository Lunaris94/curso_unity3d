using System;

public class ConversionesDatos {
	public static void Main(){
		
		//Convertir número en texto y float en double
		//Conversiones implicitas:
		int edad = 40;
		string resultado;
		resultado = "" + edad;
		resultado = resultado + " era un número y ahora es texto";
		Console.WriteLine( resultado );
		
		double numDecimal=6.4545f;
		resultado = resultado+ ", el numDecimal = " +numDecimal;
		Console.WriteLine(resultado);
		
		//Conversiones explíticas:
		//Se pone entre paréntesis el tipo al que quieres convertir el dato que introducimos
		float otroDecimal= (float) 1.123456789123;
		resultado = resultado + " , otroDecimal " + otroDecimal;
		Console.WriteLine( resultado );
		
		//Esta función  no se puede realizar
		/*bool unBool =(bool) 1;
		resultado = resultado + " , unBool " + unBool;
		Console.WriteLine( resultado );*/ 
		
		//short  se refiere a un número pequeño, por ejemplo las vidas en un juego
		
		//Conversiones complejas: de texto a número
		int unEntero = Int32.Parse ("3434");
		resultado = resultado + " , unEntero " + unEntero;
		Console.WriteLine( resultado );
		
		//Ejercicio: 
		string numA="15", numB= "7";
		//Haz que el programa realize la suma y muestre el resultado
		//aquí arriba "15" y "7" están convertidos en texto, por eso 
		//cuando le hacemos la suma, lo único que hace es concatenar 
		//los dos textos de 15 y 7
		//resultado = numA + numB;
		//Console.WriteLine(resultado);
		
		
		int numeroA= Int32.Parse(numA);
		int numeroB= Int32.Parse(numB);
		int solucion= numeroA + numeroB;
		resultado= "El resultado de " + numA + " más " + numB + " es " + solucion;
		Console.WriteLine (resultado);
		
		//aqui transformamos "15" y "7" en números
		//después añadimos otro número llamado solución
		//que sería la suma entre los dos números anteriores
		//después, al tener "resultado" como texto, lo utilizamos
		//para que nos deje ver una cadena de texto con "15 y 7" 
		//en texto y el numero solucion que hemos obtenido anteriormente
		//finalmente, le decimos que nos escriba el texto resultado en la consola
		
		/*otra forma de hacerlo es la siguiente:
		int inNumA=Int32.Parse(numA);
		int inNumB=Int32.Parse(numB);
		int resultadoSuma = inNumA+inNumB;
		Console.WriteLine("El resultado es " + resultadoSuma);*/
		
	}
}