using System;

public class Funciones 
{
	public static void Main() 
	{
		//Para usar una función, ponemos el nombre y entre parentesis los argumentos (param)
		double y;
		string strvalorUsuario;
		double valUsu;
		
		Console.WriteLine ("Introuzca valor de X: ");
		strvalorUsuario = Console.ReadLine ();
		valUsu= Double.Parse (strvalorUsuario);
		y = FuncionLineal2x3 (valUsu);
		Console.WriteLine ("Resultado 2*X + 3: " + y);
		
		Console.WriteLine ("Introduzca valor de X: ");
		strvalorUsuario = Console.ReadLine ();
		valUsu=Double.Parse (strvalorUsuario);
		y = FuncionLineal2 (valUsu);
		Console.WriteLine ("Resultado de X ^ 2 + 1: " + y);
		
		
	}
	//Forma de una función estática, es decir, que no depende de
	//un factor externo
	// <Modificador acceso> static <tipo de dato result> <nombre de la fucnción> (<tipo> parám.1, <tipo> param.2,...)
	//y luego entre llaves el cuerpo de la función
	//Con return podemos devolver un valor
	
	private static double FuncionLineal2x3 (double x)
	{
		return x * 2 + 3;
	}
	
	private static double FuncionLineal2(double x)
	{
		return x * x + 1;
	}
	
	//private no es obligatorio, static es obligatorio, en vez de double pone float
	//la funcion se llama como quieras y le añades el tipo y el parametro
	//e el cuerpo de la funcion, en lugar de devolver el resultado la guarda
	//para que despues devuelva la variable
	//en este caso, el tipo de numero float ocupa mucho mas espacio
	//que en el caso de double, por eso, para un juego es mejor intentar reducir 
	//el espacio que se ocupa al maximo para que no vaya despacio
	/*static float FuncionXe12_1(float param) 
	{
		float resultado = param * param + 1;
		return resultado;
	}*/
	
	
}