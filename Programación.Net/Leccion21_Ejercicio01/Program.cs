﻿using System;

namespace Leccion21_Ejercicio01
{
    /*
     * Crear una matriz de 2 filas y 5 columnas. Realizar la carga de componentes por columna (es decir primero ingresar toda la primer columna, luego la segunda columna y así sucesivamente).
     * Imprimir luego la matriz.
     */
    class Program
    {
        private int[,] componentes;

        static void Main(string[] args)
        {
            Program matriz = new Program();
            matriz.CargarDatos();
            matriz.MostrarMatriz();
            matriz.MostrarDiag();
        }
        public void CargarDatos()
        {
            componentes = new int[4, 4];
            for(int f = 0; f < 4; f++)
            {
                for(int c = 0; c < 4; c++)
                {
                    Console.WriteLine("Introduzca número");
                    componentes[f, c] = Int32.Parse(Console.ReadLine());
                }
            }
        }
        public void MostrarMatriz()
        {
            for(int f = 0; f < 4; f++)
            {
                for(int c = 0; c < 4; c++)
                {
                    Console.Write(componentes[f, c] + " ");
                }
                Console.WriteLine();
            }
        }
        public void MostrarDiag()
        {
            for(int i = 0; i < 4; i++)
            {
                Console.WriteLine(componentes[i, i]+" ");
            }
        }
    }
}
