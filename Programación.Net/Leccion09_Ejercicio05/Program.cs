﻿using System;

namespace Leccion09_Ejercicio05
{
    /*
     * Mostrar los múltiplos de 8 hasta el valor 500. Debe aparecer en pantalla 8 - 16 - 24, etc. 
     */
    class Program
    {
        static void Main(string[] args)
        {
            int n = 0;
            while (n<=500)
            {
                Console.WriteLine(n);
                n = n + 8;
            }
            Console.WriteLine("500 no es múltiplo de 8");
        }
    }
}
