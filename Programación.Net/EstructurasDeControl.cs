using System;

public class EstructurasDeControl 
{
	public static void Main() 
	{
		/*EjemploIfSimple();
		EjemploIfComplicado();
		EjercicioIfSumas();
		EjemploIfConsecutivos();
		EjemploBucleWhile();
		EjemploBucleFor();*/
		EjemploBucleDoWhile();
	}
	static void EjemploIfSimple()
	{
		//Condicional simple: 
		//if (condicion/booleano) intruccion;
		if (true) Console.WriteLine("Pues sí");
		if (false) Console.WriteLine("Pues va a ser que no");
		//nos da un warning porque esta segunda línea nunca se va a compilar
		
		//Recibe variable
		bool oSioNo = true;
		if (oSioNo) Console.WriteLine("Pues también sí");
		
		//O recibimos condicionales
		if (5 == 5) Console.WriteLine("Pues 5 = 5");
		if (4 > 7) Console.WriteLine("Pues esto tampoco se muestra");
	}
	
	static void EjemploIfComplicado()
	{
		//If complicado: 
		//if (bool) instruccionVerdad; else instrucciónFalso;
		if (4 >= 7) Console.WriteLine("4 >= 7");
		else Console.WriteLine("4 < 7");
		//Podemos separar en varias líneas
		if ("Hola" == "Hola")Console.WriteLine("Son =");
		else Console.WriteLine("Son dist.");
		/*if ("Hola" != "Hola")Console.WriteLine("Son dist.");
		else Console.WriteLine("Son =");*/
	}
	
	static void EjercicioIfSumas()
	{
		//Ejercicio:
		string numA= "20", numB = "30", numC = "40";
		string resultadotxt = "50";
		int solucion1;
		int solucion2;
		int solucion3;
		int resultado = 50;
		//Suma las 3 combinaciones (A + B, B +C y A + C) y que el programa
		//diga cual es igual al resultado dado
		//Consola debe mostrar:
		//1 mostrar valores: A=20, B=30, C=40, resultado=50
		//A + B es igual a resultado
		//A + C es distinto a resultado
		//B + C es distinto a resultado
		
		int numeroA=Int32.Parse (numA);
		int numeroB=Int32.Parse (numB);
		int numeroC=Int32.Parse (numC);
		string valores = "A = " + numA + " B = " + numB + " C = " + numC + " Resultado = " + resultadotxt;
		Console.WriteLine (valores);
		
		//escribir estas solucones en realidad no hace falta, se podría 
		//poner directamente dentro del if como se ve más abajo, en la correción
		//del ejercicio
		solucion1 = numeroA + numeroB;
		solucion2 = numeroB + numeroC;
		solucion3 = numeroA + numeroC;
		
		if (solucion1 == resultado)Console.WriteLine("A + B es igual al resultado");
		else Console.WriteLine("A + B es diferente al resultado");
		if (solucion2 == resultado)Console.WriteLine("A + C es igual al resultado");
		else Console.WriteLine("A + C es diferente al resultado");
		if (solucion3 == resultado)Console.WriteLine("B + C es igual al resultado");
		else Console.WriteLine("B + C es diferente al resultado");
		
		/* Correción; La forma reducida sería:
		int intA, intB, intC;
		int intA=Int32.Parse (numA);
		int intB=Int32.Parse (numB);
		int intC=Int32.Parse (numC);
		
		if (intA + intB == resultado)
			Console.WriteLine("A + B es igual al resultado");
		else 
			Console.WriteLine("A + B es diferente al resultado");
		
		if (intA + intC == resultado)
			Console.WriteLine("A + C es igual al resultado");
		else 
			Console.WriteLine("A + C es diferente al resultado");
		
		if (intB + intC == resultado)
			Console.WriteLine("B + C es igual al resultado");
		else 
			Console.WriteLine("B + C es diferente al resultado");
		*/
	}
	
	static void EjemploIfConsecutivos() 
	{
		Console.WriteLine ("Introduzca una opción: ");
		Console.WriteLine ("1 - Opción primera ");
		Console.WriteLine ("2 - Opción segunda ");
		Console.WriteLine ("3 - Opción tercera ");
		Console.WriteLine (" (*) - Cualquier otra opción ");
		
		ConsoleKeyInfo opcion = Console.ReadKey();
		ConsoleKey conKey = opcion.Key;
		string caracter = conKey.ToString();
		
		Console.WriteLine (">>" + caracter);
		
		//Si el caracter es un 1 del teclado normal o bien ||
		//si el caracter es un 1 del teclado numérico
		if (caracter == "NumPad1" || caracter == "D1")
			Console.WriteLine(" Has elegido la primera ");
		else if (caracter == "NumPad2" || caracter == "D2")
			Console.WriteLine (" Has elegido la segunda");
		else if (caracter == "NumPad3" || caracter == "D3")
			Console.WriteLine (" Has elegido la tercera");
		else if (caracter == "NumPad4" || caracter == "D4")
			Console.WriteLine (" Has elegido la cuarta");
		else 
			Console.WriteLine(" OPCIÓN NO CONTEMPLADA ");
		
	}

	static void EjemploBucleWhile() 
	{	
	//El bucle while pude crear cualquier tipo de bucle
	//Puede que nunca se ejecute, si el booleano (condición) es false de entrada
	//o puede que sea un bucle infinito si la condició siempre es true
	//o un bucle normal, con una duración determinada
	
		Console.WriteLine("Antes de bucle");
		
		while (false)
		{
			Console.WriteLine("Instruccion que NO se repite");
		}
		
		int contador = 0;
		while (contador < 10) 
		{
			Console.WriteLine ("Contador es igual a " + contador);
			contador = contador +1;
		}
		bool siContinuar = true;
		while (siContinuar)
		{
			Console.WriteLine("Estamos en el bucle. ¿Desea salir?");
			string tecla = Console.ReadKey().Key.ToString();
			if (tecla == "S" || tecla == "s")
			{
				siContinuar = false;
			}
		}
		
		Console.WriteLine("Fin de bucle");
	}
	
	static void EjemploBucleFor() 
	{
	//El bucle for es como un bucle while
	//es una forma de "reducir" un bucle while
	// for (<inicializacion> ; <condicion> ; <incremento/decremento>)
	//	instrucción; o bien un {bloque que se repite}

		for (int contador = 0; contador < 10; contador = contador +1)
		{
			Console.WriteLine ("Contador es igual a " + contador);
		}
	}
	
	static void EjemploBucleDoWhile()
	{
		//Es como un bucle while, pero se ejecuta sí o sí, al menos una vez
		
		do {
			Console.WriteLine("Al menos una vez");
		} while (false);
		
		bool siSalir = true;
		do{
			Console.WriteLine ("Hasta que la condicion sea falsa");
			siSalir = ! siSalir;
		} while (! siSalir);
	}
}