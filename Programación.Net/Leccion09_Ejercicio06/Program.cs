﻿using System;
using System.Collections.Generic;

namespace Leccion09_Ejercicio06
{
    /*
     * Realizar un programa que permita cargar dos listas de 15 valores cada una. Informar con un mensaje cual de las dos listas tiene un valor acumulado mayor (mensajes "Lista 1 mayor", "Lista 2 mayor", "Listas iguales")
     * Tener en cuenta que puede haber dos o más estructuras repetitivas en un algoritmo.
     */
    class Program
    {
        static void Main(string[] args)
        {
            List<int> lista1 = new List<int>();
            List<int> lista2 = new List<int>();

            Console.WriteLine("Lista 1");
            AñadirValores(lista1);

            Console.WriteLine("Lista 2");
            AñadirValores(lista2);
            //MostrarLista(lista1);
            //MostrarLista(lista2);
            int valAcum1=Suma(lista1);
            int valAcum2=Suma(lista2);

            if (valAcum1 == valAcum2)
            {
                Console.WriteLine("Listas iguales");
            }
            else if (valAcum1 > valAcum2)
            {
                Console.WriteLine("Lista 1 mayor");
            }
            else if (valAcum2 > valAcum1)
            {
                Console.WriteLine("Lista 2 mayor");
            }
        }

        public static List<int> AñadirValores(List<int> lista)
        {
            string linea;
            int valor;
            int i = 0;
            while (i<15)
            {
                Console.WriteLine("Ingrese valor");
                linea = Console.ReadLine();
                valor = Int32.Parse(linea);
                lista.Add(valor);
                i++;
            }
            return lista;
        }
        public static int Suma(List<int> lista)
        {
            int suma = 0;
            for(int i = 0; i < lista.Count; i++)
            {
                suma = suma + lista[i];
            }
            return suma;
        }

        public static void MostrarLista(List<int> lista)
        {
            for(int i = 0; i < lista.Count; i++)
            {
                Console.WriteLine(lista[i]);
            }
        }
    }
}
