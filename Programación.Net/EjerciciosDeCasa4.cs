using System;

public class EjerciciosDeCasa4
{
	static void Main()
	{
		/**/
		//.Imprimir en pantalla numeros consecutivos, en este caso, del 2 al 100
		//pero mostrando solo los numeros pares
		int n=2;
		Console.WriteLine("Inicio del bucle");
		while (n<=100)
		{
		Console.WriteLine(n);
		n = n +2;
		}
		Console.WriteLine("Fin del bucle");
		
		
		
		//.Solicitar cargo de un valor positivo y mostrar desde 1 hasta el valor introducido
		//de uno en uno
		string linea;
		int n = 1;
		Console.WriteLine("Introduzca valor positivo");
		linea = Console.ReadLine();
		int valor = Int32.Parse(linea);
		
		while (n <=valor)
		{
			Console.WriteLine(n);
			n = n + 1;
		}
		
		
		
		//.Programa que carga 10 valores dentro de un bucle y nos muestra la suma 
		//de los valores ingresados y su promedio
		int n=1;
		int suma=0;
		while (n<=10)
		{
			Console.WriteLine("Ingrese un valor");
			string linea = Console.ReadLine();
			int valor=Int32.Parse(linea);
			suma=suma + valor;
			n=n+1;
		}
		int promedio = suma/10;
		Console.WriteLine("La suma total de los valores es: " + suma);
		Console.WriteLine("El promedio de los valores es: " + promedio);
		
		
		
		//.Una planta que fabrica perfiles de hierro, posee un lote de n piezas
		//ingresar la cantidad de piezas a procesar y luego ingrese la longuitud
		//de cada perfil, sabiendo que aquellos con una longuitud comprendida entre
		//1'20 y 1'30 son aptos. Imprimir la cantidad de piezas aptas que hay por lote
		int nPiezas, n, cantidad;
		float largo;
		string linea;
		n = 1;
		cantidad = 0;
		Console.WriteLine("Ingrese la cantidad de piezas por lote");
		linea = Console.ReadLine();
		nPiezas = Int32.Parse(linea);
		
		while (n<=nPiezas)
		{
			Console.WriteLine("Ingrese longuitud de pieza");
			linea = Console.ReadLine();
			largo = float.Parse(linea);
			if (largo >=1.20f && largo <=1.30f)
			{
				cantidad = cantidad + 1;
			}
			n = n + 1;
		}
		Console.WriteLine("Piezas aptas en este lote: " + cantidad);
		
		
		
		//.Ingresar 10 notas de alumnos e informar cuantos tienen notas mayores
		//o iguales a 7 y cuantos menores
		int n, nota, notaMay, notaMen;
		string linea;
		notaMay=0;
		notaMen=0;
		n=1;
		while (n<=10)
		{
			Console.WriteLine("Ingrese nota del alumno");
			linea=Console.ReadLine();
			nota=Int32.Parse(linea);
			if (nota>=7)
			{
				notaMay=notaMay+1;
			}else
			{
				notaMen=notaMen+1;
			}
			n=n+1;
		}
		Console.WriteLine("Notas mayores ó = a 7: " + notaMay);
		Console.WriteLine("Notas menores a 7: " + notaMen);
		
		
		//.Ingresar conjunto de n alturas de personas por teclado. Mostrar promedio
		int nPer, n;
		float altura, sumaAltura, promedio;
		string linea;
		n=1;
		sumaAltura=0;
		Console.WriteLine("¿A cuántas personas ha medido?");
		linea=Console.ReadLine();
		nPer=Int32.Parse(linea);
		
		while (n<=nPer)
		{
			Console.WriteLine("Ingrese altura");
			linea=Console.ReadLine();
			altura=float.Parse(linea);
			sumaAltura=sumaAltura + altura;
			n=n+1;
		}
		promedio=sumaAltura/nPer;
		Console.WriteLine("La altura promedio es de: " + promedio);
		
		
		
		//.En empresa trabajan n empleados cuyos sueldos oscilan entre 100€ y 500€
		//Leer los sueldos que cobra cada empleado e informar cuantos cobran entre
		//100 y 300 y cuantos más de 300. Además, informar de lo que gasta la empresa en sueldos
		int nEmpleados, n, sueldo, sueldoAlt, sueldoBaj, totalSueldos;
		string linea;
		sueldoAlt=0;
		sueldoBaj=0;
		totalSueldos=0;
		n=1;
		Console.WriteLine("Número de empleados");
		linea=Console.ReadLine();
		nEmpleados=Int32.Parse(linea);
		
		while (n<=nEmpleados)
		{
			Console.WriteLine("Ingrese sueldo");
			linea=Console.ReadLine();
			sueldo=Int32.Parse(linea);
			if (sueldo>=100 && sueldo<300)
			{
				sueldoBaj=sueldoBaj+1;
			}else
			{
				sueldoAlt=sueldoAlt+1;
			}
			totalSueldos=totalSueldos + sueldo;
			n=n+1;
		}
		Console.WriteLine("Hay " + sueldoBaj + " sueldos entre 100 y 300 euros");
		Console.WriteLine("Hay " + sueldoAlt + " sueldos mayores de 300 euros");
		Console.WriteLine("La empresa gasta un total de " + totalSueldos);
		
		
		
		//.Imprimir 25 términos de la serie 11-22-33-44, etc
		int n, serie;
		n=1;
		serie=11;
		Console.WriteLine("Inicio del bucle");
		while (n<=25)
		{
			Console.WriteLine(serie);
			serie=serie+11;
			n=n+1;
		}
		Console.WriteLine("Fin del bucle");
		
		
		
		//.Mostrar múltiplos de 8 hasta el valor 500, debiendo aparecer 8-16-24, etc.
		int multiplo;
		multiplo=8;
		while(multiplo<=500)
		{
			Console.WriteLine(multiplo);
			multiplo=multiplo*suma;
			multiplo=multiplo+8;
		}
		
		
		
		//.Cargar dos listas de 15 valores cada una. Informar cual de las dos tiene 
		//un valor acumulado mayor ("Lista 1 mayor", "Lista 2 mayor", "Listas iguales")
		//Tener en cuenta que puede haber dos o mas estructuras repetitivas 
		int n, valor1, valor2, suma1, suma2;
		string linea;
		n=1;
		suma1=0;
		suma2=0;
		Console.WriteLine("Primera lista");
		while(n<=15)
		{
			Console.WriteLine("Cargar valor para lista 1");
			linea=Console.ReadLine();
			valor1=Int32.Parse(linea);
			suma1=suma1+valor1;
			n=n+1;
		}
		Console.WriteLine("Segunda lista");
		n=1;
		while(n<=15)
		{
			Console.WriteLine("Cargar valor para lista 2");
			linea=Console.ReadLine();
			valor2=Int32.Parse(linea);
			suma2=suma2+valor2;
			n=n+1;
		}
		if (suma1>suma2)
		{
			Console.WriteLine("Lista 1 mayor");
		}else if(suma1==suma2)
		{
			Console.WriteLine("Listas iguales");
		}else 
		{
			Console.WriteLine("Lista 2 mayor");
		}
		
		
		//.Cargar n números enteros e informar cuántos valores fueron pares e impares
		//Emplear el operador % en la condicion de la estructura condicional:
		// if(valor%2==0) es decir, si el if es verdadero es par
		int n, cantidad, valor, valorPar, valorImpar;
		n=1;
		valorPar=0;
		valorImpar=0;
		string linea;
		Console.WriteLine("Cantidad de números enteros a ingresar");
		linea=Console.ReadLine();
		cantidad=Int32.Parse(linea);
		
		while(n<=cantidad)
		{
			Console.WriteLine("Ingresar número entero");
			linea=Console.ReadLine();
			valor=Int32.Parse(linea);
			if(valor%2==0)
			{
				valorPar=valorPar+1;
			}else
			{
				valorImpar=valorImpar+1;
			}
			n=n+1;
		}
		Console.WriteLine("Hay un total de " +valorPar+ " números pares ingresados");
		Console.WriteLine("Hay un total de " +valorImpar+ " números impares ingresados");
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
}