﻿using System;

namespace Leccion10_Ejercicio01
{
    /*
     * Confeccionar un programa que lea n pares de datos, cada par de datos corresponde a la medida de la base y la altura de un triángulo. El programa deberá informar:
     * a) De cada triángulo la medida de su base, su altura y su superficie. 
     * b) La cantidad de triángulos cuya superficie es mayor a 12.
     */
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("¿Cuántos triángulos va a introducir?");
            string linea = Console.ReadLine();
            int n = Int32.Parse(linea);

            int baseT, altura, superficie;
            int superfMayor=0;
            for(int i = 0; i < n; i++)
            {
                Console.WriteLine("Introduce la base");
                linea = Console.ReadLine();
                baseT = Int32.Parse(linea);
                Console.WriteLine("Introduce la altura");
                linea = Console.ReadLine();
                altura = Int32.Parse(linea);

                superficie = baseT * altura/2;
                Console.WriteLine("La base, la altura y la superficie de este triángulo son: " + baseT + ", " + altura + " y " + superficie);

                if (superficie > 12)
                {
                    superfMayor = superfMayor + 1;
                }
            }
            Console.WriteLine("Hay " + superfMayor + " triángulos con una superficie mayor a 12");
        }
    }
}
