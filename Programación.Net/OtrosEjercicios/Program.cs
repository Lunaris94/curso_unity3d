﻿using System;

namespace OtrosEjercicios
{
    class Program
    {
        static void Main(string[] args)
        {
            //Ejercicio02();
            //Ejercicio03();
            //Ejercicio04();
            Ejercicio05();    //TODO   
            //Ejercicio06();
            //Ejercicio09();
            //Ejercicio10();
        }

        public static void Ejercicio02()
        {
            //Pedir por consola un nombre de persona y el nombre de una ciudad y mostrar por pantalla, el siguiente mensaje “Hola ” <nombre> ” bienvenido a ” <ciudad>

            Console.WriteLine("Ingrese nombre");
            string nombre = Console.ReadLine();
            Console.WriteLine("Ingrese ciudad");
            string ciudad = Console.ReadLine();

            Console.WriteLine("Hola " + nombre + " bienvenida/o a " + ciudad);

            Console.ReadKey();
        }

        public static void Ejercicio03()
        {
            //Pedir por consola tu nombre y tu edad y mostrar el siguiente mensaje: “Te llamas ” <nombre> ” y tienes ” <años> ” años”

            Console.WriteLine("Ingrese nombre");
            string nombre = Console.ReadLine();
            Console.WriteLine("Ingrese edad");
            string linea = Console.ReadLine();
            int edad = Int32.Parse(linea);

            Console.WriteLine("Te llamas " + nombre + " y tienes " + edad + " años");

            Console.ReadKey();
        }

        public static void Ejercicio04()
        {
            // Pedir dos números al usuario por teclado y decir que número es el mayor.

            int num1, num2;
            string linea;

            Console.WriteLine("Ingrese primer número");
            linea = Console.ReadLine();
            num1 = Int32.Parse(linea);
            Console.WriteLine("Ingrese primer número");
            linea = Console.ReadLine();
            num2 = Int32.Parse(linea);

            if (num1 > num2)
            {
                Console.WriteLine(num1 + " es mayor que " + num2);
            }
            else
            {
                Console.WriteLine(num2 + " es mayor que " + num1);
            }
            Console.ReadKey();
        }

        public static void Ejercicio05()
        {
            //Pedir el nombre de la semana al usuario y decirle si es fin de semana o no.  En caso de error, indicarlo.

            string nombre;
            Console.WriteLine("Ingrese nombre del día de la semana");
            nombre = Console.ReadLine();

            //se hace con un switch PREGUNTAR A GERMÁN
            
        }

        public static void Ejercicio06()
        {
            //Pedir al usuario el precio de un producto (valor positivo) y la forma de pagar (efectivo o tarjeta) si la forma de pago es mediante tarjeta, pedir el numero de cuenta (inventado)

            int valor;
            string linea;

            Console.WriteLine("Ingrese valor del producto");
            linea = Console.ReadLine();
            valor = Int32.Parse(linea);
            Console.WriteLine("Ingrese el método de pago:");
            Console.WriteLine("Para EFECTIVO pulse 0, para TARJETA pulse 1");
            linea = Console.ReadLine();
            bool tecla = linea == "1";


            if (tecla == true)
            {
                Console.WriteLine("Ha elegido tarjeta");
                Console.WriteLine("Introduzca un número de cuenta");
                string cuenta = Console.ReadLine();
            }
            else
            {
                Console.WriteLine("Ha elegido efectivo");
            }
            Console.ReadKey();
        }

        public static void Ejercicio09()
        {
            //Recorre los números del 1 al 100. Muestra los números pares. Usar el tipo de bucle que quieras.

            for (int i = 0; i <= 100; i=i+2)
            {
                Console.WriteLine(i);
            }
        }

        public static void Ejercicio10()
        {
            //Recorre los números del 1 al 100. Muestra los números pares o divisibles entre 3.

            for (int i = 0; i <= 100; i = i + 3)
            {
                Console.WriteLine(i);
            }
        }
    }
}
