﻿using System;

namespace Leccion10_Ejercicio03
{
    /*
     * Desarrollar un programa que muestre la tabla de multiplicar del 5 (del 5 al 50) 
     */
    class Program
    {
        static void Main(string[] args)
        {
            int n = 0;
            for(int i = 0; i <= 10; i++)
            {
                Console.WriteLine(n);
                n = n + 5;
            }
        }
    }
}
