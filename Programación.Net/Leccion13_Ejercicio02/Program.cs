﻿using System;

namespace Leccion13_Ejercicio02
{
    /*
     * Implementar la clase operaciones. Se deben cargar dos valores enteros, calcular su suma, resta, multiplicación y división, cada una en un método, imprimir dichos resultados. 
     */
    class Operaciones
    {
        private int valor1;
        private int valor2;

        static void Main(string[] args)
        {
            Operaciones operacion = new Operaciones();

            operacion.CargarDatos();
            operacion.Suma();
            operacion.Resta();
            operacion.Producto();
            operacion.Division();
        }
        public void CargarDatos()
        {
            Console.WriteLine("Introduzca dos numeros enteros");
            valor1 = Int32.Parse(Console.ReadLine());
            valor2= Int32.Parse(Console.ReadLine());
        }

        public void Suma()
        {
            int suma = valor1 + valor2;
            Console.WriteLine("El resultado de la suma es: " + suma);
        }
        public void Resta()
        {
            int resta = valor1 - valor2;
            Console.WriteLine("El resultado de la resta es: " + resta);
        }
        public void Producto()
        {
            int producto = valor1 * valor2;
            Console.WriteLine("El resultado del producto es: " + producto);
        }
        public void Division()
        {
            int division = valor1 / valor2;
            Console.WriteLine("El resultado de la división es: " + division);
        }
    }
}
