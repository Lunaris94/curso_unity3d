﻿using System;
using System.Collections.Generic;

namespace Leccion11_Ejemplo04Copia
{
    class Program
    {
        static string[] nombresEnemigos =
            {
                    "Lata 1",
                    "Lata 2 (Clone)",
                    "Lata 22 (Clone)",
                    "Lata 32 (Clone)",
                    "Lata 2",
                    "Basura 11",
                    "Basura 2",
                    "Basura 3"
            };
        static void Main(string[] args)
        {

            string dosPrimeros = "Los dos primeros enemigos son " + nombresEnemigos[0] + ", " + nombresEnemigos[1];
            Console.WriteLine(dosPrimeros);

            SepararPorNumeroGeneral();
        }
        
        static bool SaberSiEsTipo(String nombreEnemigo, String num)
        {
            if (nombreEnemigo.IndexOf(num)>=0)
            {
                int posChar = nombreEnemigo.IndexOf(num);
                if (nombreEnemigo.Substring(posChar - 1, 1) == " " )
                {
                    if (posChar == nombreEnemigo.Length - 1)
                    {
                        return true;
                    }
                    else
                    {
                        string sigChar = nombreEnemigo.Substring(posChar + 1, 1);
                        if (!int.TryParse(sigChar, out int numDelChar))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        

        static bool SaberSiEsTipo_Reducido(String nombreEnemigo, String num)
        {
            //MÉTODO MÁS REDUCIDO PERO CON EL MISMO CÓDIGO

            int posChar = nombreEnemigo.IndexOf(num);
            string sigChar = posChar >=0 && posChar < nombreEnemigo.Length -1 ? nombreEnemigo.Substring(posChar + 1, 1) : " ";
            bool transNum = int.TryParse(sigChar, out int numDelChar);
            if (posChar >= 0 && nombreEnemigo.Substring(posChar - 1, 1) == " " && (posChar == nombreEnemigo.Length - 1 || !transNum))
            {
                return true;
            }
            return false;
        }
        static void SepararPorNumeroGeneral()
        {
            List<string> obj_1 = new List<string>();
            List<string> obj_2 = new List<string>();
            List<string> obj_3 = new List<string>();
            List<string> obj_resto = new List<string>();

            for (int i = 0; i < nombresEnemigos.Length; i++)
            {
                string enemigoActual = nombresEnemigos[i];
                if (SaberSiEsTipo(enemigoActual, "1"))
                {
                    obj_1.Add(enemigoActual);
                }
                else if (SaberSiEsTipo(enemigoActual, "2"))
                {
                    obj_2.Add(enemigoActual);
                }
                else if (SaberSiEsTipo(enemigoActual, "3"))
                {
                    obj_3.Add(enemigoActual);
                }
                else
                {
                    obj_resto.Add(enemigoActual);
                }

            }

            Console.WriteLine("TIPOS 1: " + obj_1.Count);
            for (int i = 0; i < obj_1.Count; i++)
            {
                string enemigoActual = obj_1[i];
                Console.WriteLine( enemigoActual);
            }
            Console.WriteLine("TIPOS 2: " + obj_2.Count);
            foreach (string enemAc in obj_2)
            {
                Console.WriteLine(enemAc);
            }
            Console.WriteLine("TIPOS 3: " + obj_3.Count);
            foreach (string enemAc in obj_3)
            {
                Console.WriteLine(enemAc);
            }
            Console.WriteLine("RESTO DE TIPOS: " + obj_resto.Count);
            foreach (string enemAc in obj_resto)
            {
                Console.WriteLine(enemAc);
            }
        }
        
        /*
        static string SaberSiEsTipo(String num)
        {
            string valor = "0";
            if (num.IndexOf("1") >= 0)
            {
                valor = "1";
            }
            else if (num.IndexOf("2") >= 0)
            {
                valor = "2";
            }
            else if (num.IndexOf("3") >= 0)
            {
                valor = "3";
            }
            return valor;
        }
        static void SepararPorNumeroGeneral()
        {
            List<string> obj_1 = new List<string>();
            List<string> obj_2 = new List<string>();
            List<string> obj_3 = new List<string>();

            for (int i = 0; i < nombresEnemigos.Length; i++)
            {
                string enemigoActual = nombresEnemigos[i];
                string valor = SaberSiEsTipo(enemigoActual);
                if (enemigoActual.IndexOf(valor) >= 0)
                {
                    int posChar = enemigoActual.IndexOf(valor);
                    if (enemigoActual.Substring(posChar - 1, 1) == " ")
                    {
                        if (posChar == enemigoActual.Length - 1)
                        {
                            if (valor == "1")
                            {
                                obj_1.Add(enemigoActual);
                            }
                            else if (valor == "2")
                            {
                                obj_2.Add(enemigoActual);
                            }
                            else if (valor == "3")
                            {
                                obj_3.Add(enemigoActual);
                            }
                        }
                        else
                        {
                            string sigChar = enemigoActual.Substring(posChar + 1, 1);
                            if (!int.TryParse(sigChar, out int numDelChar))
                            {
                                if (valor == "1")
                                {
                                    obj_1.Add(enemigoActual);
                                }
                                else if (valor == "2")
                                {
                                    obj_2.Add(enemigoActual);
                                }
                                else if (valor == "3")
                                {
                                    obj_3.Add(enemigoActual);
                                }
                            }
                        }
                    }
                }
            }

            Console.WriteLine("TIPOS 1: " + obj_1.Count);
            for (int i = 0; i < obj_1.Count; i++)
            {
                string enemigoActual = obj_1[i];
                Console.WriteLine("Tipo 1: " + enemigoActual);
            }
            Console.WriteLine("TIPOS 2: " + obj_2.Count);
            foreach (string enemAc in obj_2)
            {
                Console.WriteLine("Tipo 2: " + enemAc);
            }
            Console.WriteLine("TIPOS 3: " + obj_3.Count);
            foreach (string enemAc in obj_3)
            {
                Console.WriteLine("Tipo 3: " + enemAc);
            }
        }
        */
    }
}
