﻿using System;
using System.Globalization;

namespace Leccion23_Ejercicio01
{
    /*
     * Se desea saber la temperatura media trimestral de cuatro paises. Para ello se tiene como dato las temperaturas medias mensuales de dichos paises.
     * Se debe ingresar el nombre del país y seguidamente las tres temperaturas medias mensuales.
     * Seleccionar las estructuras de datos adecuadas para el almacenamiento de los datos en memoria.
     *      a - Cargar por teclado los nombres de los paises y las temperaturas medias mensuales.
     *      b - Imprimir los nombres de las paises y las temperaturas medias mensuales de las mismas.
     *      c - Calcular la temperatura media trimestral de cada país.
     *      c - Imprimr los nombres de las provincias y las temperaturas medias trimestrales.
     *      b - Imprimir el nombre de la provincia con la temperatura media trimestral mayor. 
     */
    class Program
    {
        string[] paises;
        float[,] tempMedMens;
        static void Main(string[] args)
        {
            Program matrizTem = new Program();
            matrizTem.CargarDatos();
            matrizTem.ImprimirTemp();
            matrizTem.TemMediaTri();
            matrizTem.MediaMayor();
        }
        public void CargarDatos()
        {
            paises = new string[4];
            tempMedMens = new float[4, 3];
          
            for(int p=0; p < paises.Length; p++)
            {
                Console.WriteLine("Introduzca nombre país");
                paises[p] = Console.ReadLine();
                for(int c = 0; c < tempMedMens.GetLength(1); c++)
                {
                    Console.WriteLine("Introduzca termperatura media mensual");
                    tempMedMens[p, c] = float.Parse(Console.ReadLine());
                }
            }
        }
        public void ImprimirTemp()
        {
            Console.WriteLine("Temperaturas: ");
            for (int p = 0; p < paises.Length; p++)
            {
                Console.Write(paises[p]+" ");
                for (int c = 0; c < tempMedMens.GetLength(0)-1; c++)
                {
                    Console.Write(tempMedMens[p, c]+" ");
                }
                Console.WriteLine();
            }
        }
        public void TemMediaTri()
        {
            float suma = 0f;
            float promedio;
            float mayor = 0f;
            string posf="aux";
            Console.WriteLine("Temperaturas medias de cada país:");
            for(int f = 0; f < tempMedMens.GetLength(0) - 1; f++)
            {
                for(int c = 0; c < tempMedMens.GetLength(1) - 1; c++)
                {
                    suma = suma + tempMedMens[f, c];
                }
                promedio = suma / tempMedMens.GetLength(0);
                Console.WriteLine(paises[f]+": " + promedio);
                if (promedio > mayor)
                {
                    mayor = promedio;
                    posf = paises[f];
                }
            }
            Console.WriteLine("El país con una media mayor es:");
            Console.WriteLine(posf + ": " + mayor);
        }
        public void MediaMayor()
        {
            float mayor = 0f;
            int posf=0;
            for(int f = 0; f < tempMedMens.GetLength(0) - 1; f++)
            {
                for(int c = 0; c < tempMedMens.GetLength(1) - 1; c++)
                {
                    if (tempMedMens[f, c] > mayor)
                    {
                        mayor = tempMedMens[f, c];
                        posf = f;
                    }
                }
            }
            Console.WriteLine("País con temperatura mayor:");
            Console.WriteLine(paises[posf] + ": " + mayor);
        }
    }
}
