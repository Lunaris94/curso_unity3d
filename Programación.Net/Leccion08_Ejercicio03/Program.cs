﻿using System;

namespace Leccion08_Ejercicio03
{
    /*
     * Se ingresan por teclado tres números, si todos los valores ingresados son menores a 10, imprimir en pantalla la leyenda "Todos los números son menores a diez". 
     */
    class Program
    {
        static void Main(string[] args)
        {
            int num1, num2, num3;
            string linea;

            Console.WriteLine("Ingrese el primer valor");
            linea = Console.ReadLine();
            num1 = Int32.Parse(linea);
            Console.WriteLine("Ingrese el segundo valor");
            linea = Console.ReadLine();
            num2 = Int32.Parse(linea);
            Console.WriteLine("Ingrese el tercer valor");
            linea = Console.ReadLine();
            num3 = Int32.Parse(linea);

            if (num1<10 && num2 < 10 && num3 < 10)
            {
                Console.WriteLine("Todos los numeros son menores a diez");
            }
            Console.ReadKey();
        }
    }
}
