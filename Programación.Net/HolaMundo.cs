//AL principio se ponen las importaciones, que en este caso 
//se utiliza la palabra using
//using System sirve para utilizar una libreria de comandos concreta
//mcs y el nombre del archivo en.cs para complicar
//luego el nombre del archivo en.exe para ver lo que hace
using System;

public class HolaMundo 
{
	static  void Main() {
		Console.Beep();
		{
		Console.WriteLine("Hola Mundo!");
		Console.WriteLine("¿Qué tal?");
		Console.ReadKey();
		}
		Console.WriteLine("Pues bien");
		Console.Beep();
		//tipo nombre;
		byte unNumero; //byte de 0 a 255 //unNumero sería la variable que hemos creado
		
		unNumero = 10;
		Console.WriteLine("El byte es " + unNumero);
		
		//comillas simples para caracteres
		//comillas dobles para textos de varios caracteres
		char unCaracter; //O bien 1 ó 2 bytes
		unCaracter = 'A'; 
		Console.WriteLine ("El caracter es " + unCaracter);
		//un caracter es también un número
		unCaracter = (char) (65 + 4); //un char es un número, pero para hacer	
								//la conversión hay que hacer "casting"
		Console.WriteLine("El caracter es " + unCaracter);
	}
}