using System;

/*Estos son los tipos primitivos de C# (igual que en otros lenguajes de progr.)
*/

public class TiposDeDatos {
	public static void Main() {
		
		byte unByte = 255;	//Nº entero entre 0-255, 8 bits,2^8
		char unCaracter = 'A';	//Un caracter tb es un num
		
		int numEntero = 1000;	//Numero entero entre -2000000000 y +2000000000
								//porque ocupa 4 bytes. 2^32=4000000000
		Console.WriteLine("Byte: " + unByte + "char: " + unCaracter);
		Console.WriteLine("Un char ocupa " + sizeof(char) + "bytes");
		Console.WriteLine("El entero vale " + numEntero);
		Console.WriteLine("Y ocupa "+sizeof(int) + "bytes");
		
		//Cuando los numeros son largos habr?a que poner una L al final
		//del numero, a no ser que sea un numero claramente largo
		numEntero = 1000000000; //mil millones
		//numEntero=1000000000L;
		Console.WriteLine("Ahora el entero vale " + numEntero);
		
		//para guaqrdar n?meros m?s largos:
		long enteroLargo=5000000000;
		Console.WriteLine("El entero largo vale "+ enteroLargo);
		
		//hay dos tipos de numeros decimales: float (4 bytes) y 
		//double (8bytes) precisi?n es de : en float es entre 7-8 cifras 
		//y en double entre 15-16 cifras en TOTAL
		float numDecimal=1.23456789f;
		Console.WriteLine("El numero decimal float vale "+ numDecimal);
		//para mayor precisi?n podemos utilizar double
		double numDoblePrecision=12345.6789123456789;
		Console.WriteLine("Numero decimal de doble precis?n vale "+numDoblePrecision);
		
		//Para guardar Si/No, Verdad/Falso, Cero/Uno... Booleano
		bool variableBooleana;
		variableBooleana=true; //1 ?nico byte
		Console.WriteLine("La variable Booleana vale "+ variableBooleana);
		//podemos guardar comparaciones, condiciones, etc...
		variableBooleana=numDecimal>1000;
		Console.WriteLine("La variableBooleana ahora es " + variableBooleana); //esta ser? falsa
		variableBooleana=numDecimal<=1000;
		Console.WriteLine("La variableBooleana ahora es "+variableBooleana); //esta ser? verdad
	
		string cadenaDeTexto="Pues eso, una cadena de texto";
		Console.WriteLine(cadenaDeTexto);
		Console.WriteLine(cadenaDeTexto +" que" +" permite concatenacion");
		//Lo que no permite son conversines inv?lidas:
			//int otroNumero = 5.234f;
			//bool otroNumero=1;
			//string otroTxt=""+1;

	}
}