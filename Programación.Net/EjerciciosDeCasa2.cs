using System;

public class EjerciciosDeCasa2
{
	static void Main()
	{
		/*//.Ingresar el sueldo de una persona, si éste supera los 300€, monstrar en pantalla que debe pagar impuestos
		float sueldo;
		string linea;
		Console.WriteLine("Ingrese el sueldo: ");
		linea = Console.ReadLine();
		sueldo = float.Parse(linea);
		
		if (sueldo > 300)
		{
			Console.WriteLine("Debe pagar impuestos");
		}
		
		
		//.Solicitar dos números distintos y que despues muestre por pantalla cual es el mayor de los dos
		float numA, numB;
		string linea;
		Console.WriteLine("Introduzca el primer valor");
		linea = Console.ReadLine();
		numA = float.Parse(linea);
		Console.WriteLine("Introduzca el segundo valor");
		linea = Console.ReadLine();
		numB = float.Parse(linea);
		if (numA > numB)
		{
			Console.WriteLine("El valor mayor es " + numA);
		} else
		{
			Console.WriteLine("El valor mayor es: " + numB);
		}
		
		
		
		//.Ingresar dos numeros. Si el primero es mayor que el segundo, informar de su suma y diferencia
		//en caso contrario, informar de su producto y de su división
		float numA, numB, suma, resta, producto, division;
		string linea;
		Console.WriteLine("Ingrese un primer número");
		linea = Console.ReadLine();
		numA = Int32.Parse(linea);
		Console.WriteLine("Ingrese un segundo número");
		linea = Console.ReadLine();
		numB = Int32.Parse(linea);
		
		if (numA > numB)
		{
			suma = numA + numB;
			resta = numA - numB;
			Console.WriteLine("El resultado de la suma es: " + suma + " y el resultado de la diferencia es " + resta);
		}else
		{
			producto = numA * numB;
			division = numA / numB;
			Console.WriteLine("El producto es: " + producto + " y la división es " + division);
		}
		
		
		//.Ingresar 3 notas de un  alumno. Si el promedio es mayor o igual a 7, nostrar mensaje "Promocionado"
		float notaA, notaB, notaC, promedio;
		string linea;
		Console.WriteLine("Ingrese la primera nota");
		linea = Console.ReadLine();
		notaA = float.Parse(linea);
		Console.WriteLine("Ingrese la segunda nota");
		linea = Console.ReadLine();
		notaB = float.Parse(linea);
		Console.WriteLine("Ingrese la tercera nota");
		linea = Console.ReadLine();
		notaC = float.Parse(linea);
		
		promedio = (notaA + notaB + notaC)/3;
		if (promedio >= 7)
		{
			Console.WriteLine("Promocionado");
		}*/
		
		
		//.Ingresar un número positivo de uno o dos dígitos. Mostrar mensaje si el número tiene uno o dos digitos
		//Tener en cuenta que condición debe cumplirse para tener dos digitos, un numero entero
		
		Console.WriteLine("Ingrese numero entero de uno o dos dígitos");
		string linea = Console.ReadLine();
		int numero = Int32.Parse(linea);
		
		if (numero >= 10)
		{
			Console.WriteLine("El número introducido tiene dos dígitos");
		}else
		{
			Console.WriteLine("El número introducido tiene un dígito");
		}
	}
	
}