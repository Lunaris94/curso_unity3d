﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata;
using System.Text;

namespace Leccion13_EjemploPOO
{
    class Unidad
    {
        //La encapsulación de la POO es ésto:
        //usar la posibilidad de poner todas las propiedades en private 
        private string nombre;
        private int vida;
        private int vidaMax;
        private int vidaMin;
        private int ataque;
        private int pocion;

        #region Constructores
        // Constructores: Son métodos especiales para construir, No devuelven nada concreto, sólo un objeto del tipo de la propia clase
        // Constructor por defecto:
        public Unidad()
        {
            this.nombre = "Unidad sin nombre";
            this.vida = 100;
            this.vidaMax = this.vida;
            this.vidaMin = 0;
            this.ataque = 10;
            this.pocion = 10;
        }

        // Constructor con parámetros
        public Unidad(String nombre, int nuevaVida, int ataque, int pocion)
        {
            this.nombre = nombre;
            this.vida = nuevaVida;
            this.vidaMax = this.vida;
            this.vidaMin = 0;
            this.ataque = ataque;
            this.pocion = pocion;
        }
        public Unidad(String nombre, int ataque)
        {
            this.nombre = nombre;
            this.vida=30;
            this.vidaMax = this.vida;
            this.vidaMin = 0;
            this.ataque = ataque;
            this.pocion=10;
        }
        #endregion
        #region Getters y Setters
        public int GetVidaMaxima()
        {
            return this.vidaMax;
        }   //end GetVidaMaxima()
        public void SetVidaMaxima(int vm)
        {
            //Si lo necesitamos, en los Getters y Setters podemos cambiar el comportamiento de acceso a la propiedad, por ejemplo, validando los datos
            if (this.vida > this.GetVidaMaxima())
            {
                this.vida = this.GetVidaMaxima();
            }
            if (vm < 0)
            {
                vm = 0;
                this.vidaMax = vm;
            }
        }   //end SetVidaMaxima(int vm)
        public string GetNombre()
        {
            return this.nombre;
        }   //end GetNombre()
        public void SetNombre(string nombre)
        {
            if (nombre == "" || nombre == null)
            {
                //Provocar un error
                throw new Exception("Eh! Te has colado, no puede ser vacío");
            }   //en if
            this.nombre = nombre;
        }   //end SetNombre(string nom)
        public int Ataque   //Una propiedad en .Net es un par de métodos get y set PERO que se usan como campos(variables miembro)
        {
            get
            {
                return this.ataque;
            }
            set //En el setter, value corresponde al valor asignado
            {
                if (value < 0)
                {
                    value = 0;
                    this.ataque = value;
                }
            }
        }   //end Ataque
        public int Pocion
        {
            get
            {
                return this.pocion;
            }
            set
            {
                if (value < 0)
                {
                    value = 0;
                    this.pocion = value;
                }
            }
        }   //end Pocion
        public int Vida
        {
            get
            {
                return this.vida;
            }
            set
            {
                if (value < 0)
                {
                    value = 0;
                    this.vida = value;
                }
            }
        }   //end Vida
        #endregion
        #region Métodos de visualización
        public string EnTexto()
        {
            return " * Nombre: " + this.nombre + "\nVida/Ataque: " + this.vida + "/"+ this.ataque;
        }
        public void Mostrar(string tipoUnidad)
        {
            Console.WriteLine("\n * Unidad " + tipoUnidad + " : " + this.nombre);
            Console.WriteLine("Vida/Ataque: " + this.vida + "/" + this.ataque);
        }
        #endregion
        //#region y #endregion son dos comentarios que se utilizan para dividir el código en diferentes regiones, de forma que se pueden colapsar varias funciones o métodos dentro de una region

        #region Métodos de juego
        //Crear un método RecibirAtaque() que quite x vida (parámetro) a la vida del objeto (variable miembro)

        /*
        public void RecibirAtaque(Unidad atacado)
        {
            atacado.vida = atacado.vida - this.ataque;
            //Se puede resumir en:
            //atacado.vida -= this.ataque;
        }
        */
        //Crear un método AtacaA() que quite X vida a una unidad (parámetro) según el ataque del propio objeto(this.ataque)
        public void AtacaA(Unidad unidRecibe)
        {
            if (this.EstaVivo())
            {
                if (unidRecibe.EstaVivo())
                {
                    Console.WriteLine("\n" + this.nombre + " sigue vivo");
                    unidRecibe.vida = unidRecibe.vida - this.ataque;
                    Console.WriteLine(this.nombre + " ataca a " + unidRecibe.nombre /*+ " como método de clase"*/);
                    if (!unidRecibe.EstaVivo())
                    {
                        unidRecibe.vida = unidRecibe.vidaMin;
                    }
                }
                else
                {
                    Console.WriteLine(this.nombre + " no puede atacar a " + unidRecibe.nombre + "porque ya está muerto");
                }
            }
            else
            {
                Console.WriteLine("\n"+this.nombre + " no está vivo, no puede atacar");
            }
        }
        public void CurarA(Unidad unidRecibe)
        {
            if (this.EstaVivo())
            {
                if (unidRecibe.EstaVivo())
                {
                    unidRecibe.vida = unidRecibe.vida + this.pocion;
                    Console.WriteLine("\n>>>>" + this.nombre + " cura a " + unidRecibe.nombre + " " + unidRecibe.vida);
                    //Console.WriteLine(unidRecibe.nombre + " vida máxima: " + unidRecibe.vidaMax);
                    if (unidRecibe.vida > unidRecibe.vidaMax)
                    {
                        unidRecibe.vida = unidRecibe.vidaMax;
                    }
                }
                else
                {
                    Console.WriteLine(this.nombre + " no puede curar a " + unidRecibe.nombre + " porque está muerto");
                }
            }
            else
            {
                Console.WriteLine("\n" + this.nombre + " no puede curar porque está muerto");
            }
        }
        //TODO: Si la vida llega a 0 que se muestre un booleano que te diga si estás vivo o no, o que has matado al mostruo o no
        //Programación Orientada a Objetos
        public bool EstaVivo()
        {
            if (this.vida>0)
            {
                return true;
            }
            return false;
        }
        #endregion
    }

}
