﻿using System;
// Mini juego:
//Fase 1: Como jugadores tenemos una unidad, y luchamos contra 1 enemigo

namespace Leccion13_EjemploPOO
{        
    //La calse Program la hacemos static para que No se pueda instanciar ni usar como objeto
    static class Program
    {
        /*
         * Ejercicios:
         * 1) A la hora de atacar, comprobar quien está vivo (si está muerto no puedo atacar). 
         *          Unidad->AtacaA()
         *Extra: No atacar si a quién ataca ya está muerto, es decir, si cualquiera está muerto(quién ataca o a quien se ataca) no se puede volver a atacar o no puede atacar.
         *Extra: Y que la vida nunca puede ser negativa.
         * 2)Meter un segundo enemigo: El jugador ataca a los 2 enemigos, y éstos atacan al jugador. 
         *          Juego->Varios métodos
         * 3)Nuevo campo en Unidad pocion de tipo entero, habrá que añadir en los constructores, dar un valor, etc...
         *          En Unidad
         * 4)Crear una vidaMax, que guarde la vida inicial. 
         *          En Unidad
         * 5)Un nuevo método CurarA(Un) para que una unidad cure a otra, en  función del valor que tenga como poción.
         *          En Unidad
         * 6)Hacer que cuando cure, no sobrepase la vidaMax. Sólo si está vivo.
         *          En Unidad -> CurarA()
         * 7)Hacer que el enemigo 1 cure al 2, por ejemplo, dos veces.
         *          En Juego
         * 8)Que el juego se repita hasta que ganes o pierdas. 
         *          Main()
         * 9)Hacer que haya 3 enemigos con opción a que haya 30: Tiene que ser un array (o lista) de Unidades
         * 10)Poner todo en privado y usar getters y setters
         */
        static void Main(string[] args)
        {
            Console.WriteLine("Juego!");
            Juego miJuego = new Juego();
            miJuego.Inicializar();
            miJuego.MostrarUnidades();
            do
            {
                miJuego.RealizarAtaques();
                miJuego.RealizarCurasEnemigos();
            }
            //while (miJuego.jugador.EstaVivo() && miJuego.enemigo.EstaVivo()&&miJuego.enemigoDos.EstaVivo());
            while (miJuego.jugador.EstaVivo() && miJuego.SiGameWin() == false);
            //En el while también podríamos poner:
            //while(!miJuego.SiGameOver()&&!miJuego.SiGameWin())
            miJuego.GameOver();
            //miJuego.ComprobarGameOver();
        }
    }
}
