﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Leccion13_EjemploPOO
{
    class Juego
    {
        public Unidad jugador = null;

        //Comentar estas dos unidades para poder hacer el array, despues saldrán muchos errores que habrá que solucionar
        public Unidad enemigo = null;
        public Unidad enemigoDos = null;
        public Unidad enemigoTres = null;
        //TODO: Hacer una lista o un array que sea una lista de enemigos:
        public Unidad[] enemigos;
        //List<Unidad> enemigos;
        public Random numRandom = new Random();

        #region Inicialización y visualización
        public void Inicializar()
        {
            this.enemigo = new Unidad("Ctchulu", 50, 10, 10);
            this.enemigoDos = new Unidad("Zombie", 5);
            this.enemigoTres = new Unidad("Dragón", 10);

            this.jugador = new Unidad();
            //Console.WriteLine("Dí tu nombre: ");
            this.jugador.SetNombre("Neo"); //Console.ReadLine();
            this.jugador.Vida = 30;
            //Console.WriteLine("Indica tu ataque: ");
            this.jugador.Ataque = 20; //Int32.Parse(Console.ReadLine());

            //TODO: Ejercicio 9: Inicializar lista o array
            this.enemigos = new Unidad[3];
            this.enemigos[0] = enemigo;
            this.enemigos[1] = enemigoDos;
            this.enemigos[2] = enemigoTres;
            //this.enemigos = new List<Unidad>();
        }
        public void MostrarUnidades()
        {
            jugador.Mostrar("JUGADOR");
            //Console.WriteLine(jugador.EnTexto());
            //enemigo.Mostrar("ENEMIGO");
            //Console.WriteLine(enemigo.EnTexto());
            //enemigoDos.Mostrar("ENEMIGO DOS");
            //Console.WriteLine(enemigoDos.EnTexto());

            //TODO: Mostrar en bucle todos los enemigos
            for(int i=0; i<enemigos.Length; i++)
            {
                enemigos[i].Mostrar(enemigos[i].GetNombre());
            }
        }
        #endregion
        #region Lógica de juego
        public void RealizarAtaques()
        {
            for(int i=0; i < enemigos.Length; i++)
            {
                Console.ReadKey();
                jugador.AtacaA(enemigos[i]);
                enemigos[i].Mostrar(enemigos[i].GetNombre());
            }
            for(int i = 0; i < enemigos.Length; i++)
            {
                enemigos[i].AtacaA(jugador);
            }
            jugador.Mostrar("JUGADOR");

            /*
            Console.ReadKey();
            jugador.AtacaA(enemigo);
            enemigo.Mostrar("ENEMIGO");
            Console.ReadKey();
            jugador.AtacaA(enemigoDos);
            enemigoDos.Mostrar("ENEMIGO DOS");
            Console.ReadKey();
            
            enemigo.AtacaA(jugador);
            enemigoDos.AtacaA(jugador);
            jugador.Mostrar("JUGADOR");
            */
        }
        public int NumeroRandom(int min, int max)
        {
            return numRandom.Next(min, max);
        }

        public void RealizarCurasEnemigos()
        {
            int num;
            for(int i = 0; i < enemigos.Length; i++)
            {
                num = NumeroRandom(0, 2);
                enemigos[i].CurarA(enemigos[num]);
                Console.WriteLine(enemigos[i].GetNombre() + " cura a " + enemigos[num].GetNombre());
                enemigos[num].Mostrar("ENEMIGO " + num);
                Console.ReadKey();
            }
            
            /*Console.ReadKey();
            enemigo.CurarA(enemigoDos);
            enemigo.CurarA(enemigoDos);
            enemigoDos.Mostrar("ENEMIGO DOS");
            */
            //enemigoDos.CurarA(enemigo);
            //enemigo.Mostrar("ENEMIGO");
            //Console.ReadKey();
            
            //TODO: Hacer que un enemigo aleatorio cure a otro enemigo aleatorio
            //"Random int c#"
        }
        public void GameOver()
        {
            if (!jugador.EstaVivo())
            {
                Console.WriteLine("\nHas perdido contra los monstruos\nGAME OVER!");
                //ComprobarGameOver();
                Console.ReadKey();
                Environment.Exit(0);    //En Unity Application.Quit();
            }
            else if(!enemigo.EstaVivo() && !enemigoDos.EstaVivo())
            {
                Console.WriteLine("\nHas ganado a los monstruos\nGAME WIN!");
                //ComprobarGameWin();
                Console.ReadKey();
                Environment.Exit(0);
            }
        }
        
        #endregion
        #region Lógica de ganar/perder
        public bool SiGameOver()
        {
            //TODO ejercicio 9: Pista, Usar una variable booleana para que cambie en el bucle que toca usar
            if (jugador.EstaVivo())
            {
                Console.WriteLine("El jugador sigue vivo");
                return false;
            }
            else
            {
                Console.WriteLine("Game Over!");
                return true;
            }
        }
        public bool SiGameWin()
        {
            bool siEnemMuerto = false;
            for(int i=0; i < enemigos.Length; i++)
            {
                if (!enemigos[i].EstaVivo())
                {
                    siEnemMuerto = true;
                }
            }
            if (siEnemMuerto == false)
            {
                Console.WriteLine("Todavía enemigo vivo, seguimos luchando");
                return false;
            }
            else
            {
                Console.WriteLine("Game Win!");
                return true;
            }
            /*
             * if (enemigo.EstaVivo()||enemigoDos.EstaVivo())
            {
                Console.WriteLine("Todavía enemigo vivo, seguimos luchando");
                return false;
            }
            else
            {
                Console.WriteLine("Game Win!");
                return true;
            }
            */
        }
        #endregion
        #region Métodos estáticas, No Usar
        //Programación funcional
        public static bool UnidadEstaViva(Unidad unidad)
        {
            if (unidad.Vida > 0)
            {
                return true;
            }
            return false;
        }
        #endregion
    }
}
