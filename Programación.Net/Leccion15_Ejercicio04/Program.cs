﻿using System;

namespace Leccion15_Ejercicio04
{
    /*
     * Cargar un vector de 10 elementos y verificar posteriormente si el mismo está ordenado de menor a mayor. 
     */
    class Program
    {
        int[] nuevoVector;
        static void Main(string[] args)
        {
            Program verificarOrden = new Program();
            verificarOrden.CargarDatos();
            verificarOrden.Ordenacion();
        }
        public void CargarDatos()
        {
            int vector;
            Console.WriteLine("Cargar datos");
            nuevoVector = new int[5];
            for (int i = 0; i < nuevoVector.Length; i++)
            {
                vector = Int32.Parse(Console.ReadLine());
                nuevoVector[i] = vector;
            }
        }
        public void Ordenacion()
        {
            for(int i = 1; i < nuevoVector.Length; i++)
            {
                if (nuevoVector[i] < nuevoVector[i - 1])
                {
                    Console.WriteLine("La lista está desordenada");
                    return;
                }
            }
            Console.WriteLine("La lista está ordenada");
        }
    }
}
