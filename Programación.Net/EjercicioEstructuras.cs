﻿using System;

struct Dueño
{
    public string nombre;
    public int edad;
    public string mascota;
    public float dinero;

    public Dueño(string nombre, int edad, string mascota, float dinero)
    {
        this.nombre = nombre;
        this.edad = edad;
        this.mascota = mascota;
        this.dinero = dinero;

    }

    public void Mostrar()
    {
        Console.WriteLine("Nombre:" + this.nombre + " Edad:" + this.edad + " Mascota:" + this.mascota + " Tiene ahorrado:" + this.dinero + " euros");
    }
}

struct Gastos
{
    public string objeto;
    public float coste;

    public Gastos(string objeto, float coste)
    {
        this.objeto = objeto;
        this.coste = coste;
    }

    public void Mostrar()
    {
        Console.WriteLine("Ha comprado: " + this.objeto + " Ha costado:" + this.coste + " euros");
    }

    public void Comprando(ref Dueño dueño)
    {
        Console.WriteLine("Se ha realizado una compra");
        dueño.dinero = dueño.dinero - this.coste;
        dueño.Mostrar();
    }

    public void TodosComprando(ref Dueño[] dueños)
    {
        for (int i = 0; i < dueños.Length; i++)
        {
            dueños[i].Mostrar();
            Console.WriteLine(dueños[i].nombre + " ha comprado " + this.objeto);
            dueños[i].dinero = dueños[i].dinero - this.coste;
            dueños[i].Mostrar();
        }
    }


}

public class EjercicioEstructuras
{
    static public void Main()
    {
        Dueño dueño1 = new Dueño("Pedro", 32, "Gato", 120f);
        Dueño dueño2 = new Dueño("Marta", 25, "Hámster", 100f);
        Dueño dueño3 = new Dueño("Pilar", 50, "Perro", 200f);

        dueño1.Mostrar();
        dueño2.Mostrar();
        dueño3.Mostrar();

        Gastos gasto1 = new Gastos("Collar", 15.90f);
        Gastos gasto2 = new Gastos("Pienso", 49.90f);
        Gastos gasto3 = new Gastos("Bebedero", 20.50f);

        gasto1.Mostrar();
        gasto2.Mostrar();
        gasto3.Mostrar();

        //gasto1.Comprando(ref dueño1);
        //dueño1.Mostrar();

        Dueño[] dueños = new Dueño[3];
        dueños[0] = dueño1;
        dueños[1] = dueño2;
        dueños[2] = dueño3;

        gasto1.TodosComprando(ref dueños);
        gasto2.TodosComprando(ref dueños);

    }
}
