﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Leccion13_Ejemplo_POO
{
    class Juego
    {
        public Unidad jugador = null;


        //TODO Ejercicio 9: Quitar estas dos variables para hacer el array
        public Unidad enemigo = null;
        public Unidad otroEnem = null;
        //TODO Ejercicio 9: Hacer un array o lista de enemigos:   Unidad[] enemigos;
        // List<Unidad> enemigos;
        List<Unidad> enemigos;

        #region Inicialización y visualización
        public void Inicializar()
        {
            enemigos = new List<Unidad>();

            this.enemigos.Add(new Unidad("Ctchulu", 50, 10, 25));
            this.enemigos.Add(new Unidad("Furia", 18, 25));
            otroEnem = new Unidad("Loky", 18, 25);
            this.enemigos.Add(otroEnem);
            

            for(int i = 0; i < 3; i++)
            {
                int nuevaVida = new Random().Next(10, 25);
                int nuevoAtaque = new Random().Next(5, 10);
                int nuevaPocion = new Random().Next(0, 2);
                otroEnem = new Unidad("Enem" + i, nuevaVida, nuevoAtaque, nuevaPocion);
                this.enemigos.Add(otroEnem);
            }
            // Crear otro enemigo, cuya vida sea 100, con otro nombre y ataque. DEPURAR para COMPROBAR
            this.jugador = new Unidad();
            // Console.WriteLine("Dí tu nombre:");
            this.jugador.SetNombre("Neo"); // Console.ReadLine();
            // Console.WriteLine("Indica tu ataque:");
            this.jugador.Ataque = 30;// int.Parse(Console.ReadLine());
            this.jugador.Vida = 50;
            //TODO Ejercicio 9: Inicializar array / lista
        }
        public void MostrarUnidades()
        {
            Console.WriteLine("");
            // jugador.Mostrar("JUGADOR");
            Console.WriteLine(jugador.EnTexto());

            for(int i = 0; i < enemigos.Count; i++)
            {
                enemigos[i].Mostrar(enemigos[i].GetNombre());
            }

            //enemigo.Mostrar("ENEMIGO");
            //otroEnem.Mostrar("ENEMIGO");
            //TODO Ejercicio 9: Mostrar en bucles todos los enemigos
            // for (int i = 0; i < enemigos.Length / Count; i++ {
            // enemigos[i].Mostrar ("ENEMIGO")
            // }

            Console.WriteLine("");
            // Console.WriteLine(enemigo.EnTexto());
        }
        #endregion

        #region Lógica de juego
        // Programar un método: ComenzarAtaques(), que haga que enemigo ataque a jugador, y jugador a enemigo.
        public void RealizarAtaques()
        {
            for(int i=0; i<enemigos.Count; i++)
            {
                enemigos[i].AtacaA(this.jugador);
                jugador.AtacaA(enemigos[i]);
            }
            //this.enemigo.AtacaA(this.jugador);
            //this.otroEnem.AtacaA(this.jugador);
            //this.jugador.AtacaA(this.enemigo);
            //this.jugador.AtacaA(this.otroEnem);

            // Es equivalente a:
            // Unidad_1_Ataca_A_Unidad_2(jugador, enemigo);
        }
        public void RealizarCurasEnemigos()
        {
            //enemigo.CurarA(otroEnem);

            //TODO Ejercicio 9: Que un enemigo aleatorio cure a otro enem aletorio
            // Buscar en Internet   "Random int C#"
            int idxCurandero, idxCurado;
            do
            {
                idxCurandero = new Random().Next(0, enemigos.Count);
                idxCurado = new Random().Next(0, enemigos.Count);

            } while (idxCurandero == idxCurado);

            Unidad curandero = this.enemigos[idxCurandero];
            Unidad curado = this.enemigos[idxCurado];
            curandero.CurarA(curado);
        }
        public bool SiGameOver()
        {
            // if (UnidadEstaViva(this.jugador))
            if (this.jugador.EstaVivo())
            {
                Console.WriteLine("Jugador sigue vivo");
                return false;
            }
            else
            {
                Console.WriteLine("GAME OVER!");
                return true;
            }
        }
        public bool SiGameWIN()
        {
            //TODO Ejercicio 9: Pista: Usar una variable booleana para 
            // que cambie en el bucle que toca hacer
            bool siEnemMuerto = false;
            for (int i = 0; i < enemigos.Count; i++)
            {
                if (!enemigos[i].EstaVivo())
                {
                    siEnemMuerto = true;
                }
            }
            if (siEnemMuerto == false)
            {
                Console.WriteLine("Todavía enemigo vivo, seguimos luchando");
                return false;
            }
            else
            {
                Console.WriteLine("Game Win!");
                return true;
            }

            /*
             * if (this.enemigo.EstaVivo() || this.otroEnem.EstaVivo())
            {
                Console.WriteLine("Todavía enemigo vivo, seguimos luchando");
                return false;
            }
            else // if ( ! this.enemigo.EstaVivo() && ! this.otroEnemigo.EstaVivo())
            {
                Console.WriteLine("GAME WIN!");
                return true;
            }
            */
        }
        /* Tmabién se puede hacer así:
         * public void ComprobarGameWIN()
        {
            // 
            if ( ! this.enemigo.EstaVivo() && ! this.otroEnem.EstaVivo())
            {
                Console.WriteLine("GAME WIN!");
            }
            else 
            {
                Console.WriteLine("Todavía enemigo vivo, seguimos luchando");
            }
        }*/
        #endregion

        #region Métodos estáticos: NO USAR
        /* public static void Unidad_1_Ataca_A_Unidad_2(Unidad unidadQueAtaca, Unidad unidadQueRecibe)
         {
             unidadQueRecibe.vida = unidadQueRecibe.vida - unidadQueAtaca.ataque;


             Console.WriteLine(unidadQueAtaca.nombre + " ataca a " + unidadQueRecibe.nombre + " desde el JUEGO");
         }
         public static bool UnidadEstaViva(Unidad unidad)
         {
             if (unidad.vida > 0)
             {
                 return true;
             } else
             {
                 return false;
             }
         }
        */
        #endregion

    }
}