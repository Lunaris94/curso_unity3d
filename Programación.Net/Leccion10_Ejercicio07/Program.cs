﻿using System;

namespace Leccion10_Ejercicio07
{
    /*
     * Se realiza la carga de 10 valores enteros por teclado. Se desea conocer:
     * a) La cantidad de valores ingresados negativos.
     * b) La cantidad de valores ingresados positivos.
     * c) La cantidad de múltiplos de 15.
     * d) El valor acumulado de los números ingresados que son pares. 
     */
    class Program
    {
        static void Main(string[] args)
        {
            int valor;
            int valorNeg = 0;
            int valorPos = 0;
            int valorMult = 0;
            int valorPar = 0;
            string linea;

            for(int i = 0; i < 10; i++)
            {
                Console.WriteLine("Introduzca un valor entero");
                linea = Console.ReadLine();
                valor = Int32.Parse(linea);

                if (valor < 0)
                {
                    valorNeg = valorNeg + 1;
                } 
                else if (valor > 0)
                {
                    valorPos = valorPos + 1;
                }

                if (valor % 15==0)
                {
                    valorMult = valorMult +1;
                }

                if (valor % 2==0)
                {
                    valorPar = valorPar +1;
                }
            }
            Console.WriteLine("Total valores negativos: " + valorNeg);
            Console.WriteLine("Total valores positivos: " + valorPos);
            Console.WriteLine("Total valores múltiplos de 15: " + valorMult);
            Console.WriteLine("Total valores pares: " + valorPar);
        }
    }
}
