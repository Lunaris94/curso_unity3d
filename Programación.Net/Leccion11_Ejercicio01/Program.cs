﻿using System;

namespace Leccion11_Ejercicio01
{
    /*
     * Realizar un programa que acumule (sume) valores ingresados por teclado hasta ingresar el 9999 (9) (no sumar dicho valor, indica que ha finalizado la carga). Imprimir el valor acumulado e informar si dicho valor es cero, mayor a cero o menor a cero. 
     */
    class Program
    {
        static void Main(string[] args)
        {
            string linea;
            int valor;
            int suma = 0;
            do
            {
                Console.WriteLine("Ingrese valor");
                linea = Console.ReadLine();
                valor = Int32.Parse(linea);
                if (valor != 9)
                {
                    suma = suma + valor;
                }
            } while (valor != 9);

            Console.WriteLine("El valor acumulado es: " + suma);
            if (valor == 0)
            {
                Console.WriteLine("El valor acumulado es 0");
            }else if (valor < 0)
            {
                Console.WriteLine("El valor acumulado es menor que 0");
            }
            else
            {
                Console.WriteLine("El valor acumulado es mayor que 0");
            }
        }
    }
}
