﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Leccion13_EjemploPOOElAhorcado
{
    class Palabra
    {
        //TODO: Poner un tema general para englobar la palabra como pista para el jugador que adivina la palabra
        public string palabra;
        public char[] matrizPalabra;
        public string[] matrizAdivinar;

        public Palabra()
        {
            this.palabra = "";
            this.matrizPalabra = new char[0];
            this.matrizAdivinar = new string[0];
        }
        
        public void MostrarArrayChar()
        {
            for(int i = 0; i < matrizPalabra.Length; i++)
            {
                Console.WriteLine(matrizPalabra[i]);
            }
        }
        public void MostrarArray()
        {
            for (int i = 0; i < matrizAdivinar.Length; i++)
            {
                Console.Write(matrizAdivinar[i] + " ");
            }
            Console.WriteLine();
        }
        public bool SiPalabraCompleta()
        {
            int cont = 0;
            for(int i = 0; i < matrizAdivinar.Length; i++)
            {
                if (matrizAdivinar[i] != "_")
                {
                    cont++;
                }
            }
            if (cont == matrizAdivinar.Length)
            {
                return true;
            }
            return false;
        }
    }
}
