﻿using System;

namespace Leccion13_EjemploPOOElAhorcado
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("     _____    ");
            Console.WriteLine("    |     |   ");
            Console.WriteLine("    O     |   ");
            Console.WriteLine("   /|     |   ");
            Console.WriteLine("   _/     |   ");

            Juego miJuego = new Juego();
            Console.WriteLine("----¡¡EL AHORCADO!!----");
            /*
             * TODO: Preguntar si quieres que una persona introduzca una palabra o si quieres jugar con una palabra que genere el ordenador
             * Para eso necesito crear varios arrays con grupos de palabras del cual escoger de forma random y que el ordenador le proponga esa palabra que ha escogido de forma aleatoria
             */
            miJuego.Inicializar();
            miJuego.IntroducirPalabra();
            Console.Clear();
            //Console.WriteLine("La pantalla debería haberse limpiado y solo aparecer este mensaje");
            Console.WriteLine("--¡¡HA ADIVINAR!!--");
            miJuego.Jugando();
        }
    }
}
