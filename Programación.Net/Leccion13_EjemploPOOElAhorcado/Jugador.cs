﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Leccion13_EjemploPOOElAhorcado
{
    class Jugador
    {
        public string nombre;
        public bool siAdivina;
        public int vidasMax;
        public int vidas;
        public Jugador()
        {
            this.nombre = "";
            this.siAdivina = false;
            this.vidasMax = 5;
            this.vidas = this.vidasMax;
        }
        public void EstablecerPalabra(Palabra palabraSecreta)
        {
            do
            {
                Console.WriteLine("\n" + this.nombre + ", introduce la palabra secreta sin que la vea el otro jugador");
                palabraSecreta.palabra = Console.ReadLine();
                Console.WriteLine("\nHas introducido: " + palabraSecreta.palabra + "\nComprueba que está bien escrita y pulsa Enter si quieres continuar, si no, pulsa cualquier otra tecla para reescribir la palabra");
            } while (Console.ReadKey().Key!=ConsoleKey.Enter);
            palabraSecreta.palabra = palabraSecreta.palabra.ToUpper();
            palabraSecreta.matrizPalabra = palabraSecreta.palabra.ToCharArray();
            palabraSecreta.matrizAdivinar = new string[palabraSecreta.matrizPalabra.Length];

            for (int i = 0; i < palabraSecreta.matrizAdivinar.Length; i++)
            {
                palabraSecreta.matrizAdivinar[i] = "_";
            }
        }
        public bool SiSinVidas()
        {
            if (this.vidas <= 0)
            {
                return true;
            }
            return false;
        }
    }
}
