﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Leccion13_EjemploPOOElAhorcado
{
    class Juego
    {
        Jugador jugador1 = new Jugador();
        Jugador jugador2 = new Jugador();
        Palabra palabraSecreta = new Palabra();

        public void Inicializar()
        {
            Console.WriteLine("\nIntroduce nombre del jugador 1");
            this.jugador1.nombre = (Console.ReadLine()).ToUpper();
            Console.WriteLine("\nIntroduce nombre del jugador 2");
            this.jugador2.nombre = (Console.ReadLine()).ToUpper();

            string aux = "";
            do
            {
                Console.WriteLine("\n¿Cuál de los dos va a adivinar la palabra?");
                aux = (Console.ReadLine()).ToUpper();
                if (aux == "1"||aux==this.jugador1.nombre)
                {
                    jugador1.siAdivina = true;
                    Console.WriteLine("\n" + jugador1.nombre + " va a adivinar la palabra secreta");
                }
                else if (aux == "2"||aux==this.jugador2.nombre)
                {
                    jugador2.siAdivina = true;
                    Console.WriteLine("\n" + jugador2.nombre + " va a adivinar la palabra secreta");
                }
                else
                {
                    Console.WriteLine("\nIntroduce 1 si va a adivinar el jugador 1\nIntroduce 2 si va a adivinar el jugador 2");
                }
            } while (aux != "1" && aux != "2" && aux != this.jugador1.nombre && aux != this.jugador2.nombre);
        }
        public void Jugando()
        {
            Console.WriteLine("\nIntroduce una letra");
            do
            {
                if (jugador1.siAdivina == true && palabraSecreta.SiPalabraCompleta() != true)
                {
                    AdivinarPalabra(jugador1);
                }
                else if(jugador2.siAdivina == true && palabraSecreta.SiPalabraCompleta() != true)
                {
                    AdivinarPalabra(jugador2);
                }
                else
                {
                    Console.WriteLine("\n--¡¡GAME WIN!!--\nEnorabuena, has logrado acertar la palabra");
                    Environment.Exit(0);
                }
            } while (jugador1.SiSinVidas()!=true && jugador2.SiSinVidas()!=true);
            Console.WriteLine("\n--¡¡GAME OVER!--\nTe has equivocado muchas veces, no has logrado adivinar la palabra");
            Environment.Exit(0);
        }
        public void IntroducirPalabra()
        {
            if (jugador1.siAdivina==true)
            {
                jugador2.EstablecerPalabra(palabraSecreta);
            }else if (jugador2.siAdivina==true)
            {
                jugador1.EstablecerPalabra(palabraSecreta);
            }
        }
        public void AdivinarPalabra(Jugador jugador)
        {
            //palabraSecreta.MostrarArrayChar();
            palabraSecreta.MostrarArray();

            //Console.WriteLine("\n" + jugador.nombre + " introduce una letra");
            string strLetra = (Console.ReadLine()).ToUpper();
            char charLetra = char.Parse(strLetra);
            bool alMenosUna = false;
            for(int i = 0; i < palabraSecreta.matrizPalabra.Length; i++)
            {
                if (charLetra == palabraSecreta.matrizPalabra[i])
                {
                    palabraSecreta.matrizAdivinar[i] = strLetra;
                    alMenosUna = true;

                }
            }
            if (alMenosUna == false)
            {
                Console.WriteLine("\n--Ohhh!! La palabra no contiene esa letra");
                jugador.vidas = jugador.vidas - 1;
                if (jugador.vidas > 0)
                {
                    Console.WriteLine("\nHas perdido una vida.\nÁnimo, prueba otra vez, te quedan " + jugador.vidas + " oportunidades");
                }
                else
                {
                    Console.WriteLine("\nNo te quedan más oportunidades");
                }
            }
            Console.WriteLine();
        }
    }
}
