﻿using System;

namespace Leccion08_Ejercicio02
{
    /**
     * Se ingresan tres valores por teclado, si todos son iguales se imprime la suma del primero con el segundo y a este resultado se lo multiplica por el tercero. 
     */
    class Program
    {
        static void Main(string[] args)
        {
            int valor1, valor2, valor3;
            string linea;

            Console.WriteLine("Ingrese el primer valor");
            linea = Console.ReadLine();
            valor1 = Int32.Parse(linea);
            Console.WriteLine("Ingrese el segundo valor");
            linea = Console.ReadLine();
            valor2 = Int32.Parse(linea);
            Console.WriteLine("Ingrese el tercero valor");
            linea = Console.ReadLine();
            valor3 = Int32.Parse(linea);

            if(valor1==valor2 && valor2 == valor3)
            {
                int suma = valor1 + valor2;
                int prod = suma * valor3;
                Console.WriteLine("La suma es: " + suma + " y el producto es: " + prod);
            }
            else
            {
                Console.WriteLine("Los valores no son iguales");
            }
            Console.ReadKey();
        }
    }
}
