﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Leccion05_Ejercicio03
{
    /**
     * Realizar un programa que lea cuatro valores numéricos e informar su suma y promedio. 
     */
    class Program
    {
        static void Main(string[] args)
        {
            List<int> numeros = new List<int>();
            int num1, num2, num3, num4;
            string linea;

            Console.WriteLine("Ingrese el primer valor");
            linea = Console.ReadLine();
            num1 = Int32.Parse(linea);
            numeros.Add(num1);

            Console.WriteLine("Ingrese el segundo valor");
            linea = Console.ReadLine();
            num2 = Int32.Parse(linea);
            numeros.Add(num2);

            Console.WriteLine("Ingrese el tercer valor");
            linea = Console.ReadLine();
            num3 = Int32.Parse(linea);
            numeros.Add(num3);

            Console.WriteLine("Ingrese el cuarto valor");
            linea = Console.ReadLine();
            num4 = Int32.Parse(linea);
            numeros.Add(num4);

            //int suma = num1 + num2 + num3 + num4;
            //int promedio = suma / 4;
            int suma = 0;
            for (int i = 0; i < numeros.Count; i++)
            {
                suma = suma + numeros[i];
            }
            int promedio = suma / numeros.Count;

            Console.WriteLine("La suma total es: " + suma);
            Console.WriteLine("El promedio es: " + promedio);
            Console.ReadKey();
        }
    }
}
