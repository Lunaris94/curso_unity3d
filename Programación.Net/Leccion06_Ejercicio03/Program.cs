﻿using System;

namespace Leccion06_Ejercicio03
{
    /**
     * Se ingresa por teclado un número positivo de uno o dos dígitos (1..99) mostrar un mensaje indicando si el número tiene uno o dos dígitos
     * (Tener en cuenta que condición debe cumplirse para tener dos dígitos, un número entero)
     */
    class Program
    {
        static void Main(string[] args)
        {
            int numero;
            string linea;

            Console.WriteLine("Ingresar número de uno o dos dígitos");
            linea = Console.ReadLine();
            numero = Int32.Parse(linea);

            if (numero < 10)
            {
                Console.WriteLine("El número tiene solo un dígito");
            }
            else
            {
                Console.WriteLine("El número tiene dos dígitos");
            }
        }
    }
}
