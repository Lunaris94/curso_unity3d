﻿using System;
using System.Collections.Generic;

namespace Leccion09_Ejercicio01
{
    class Program
    {
        /*
         * Escribir un programa que solicite ingresar 10 notas de alumnos y nos informe cuántos tienen notas mayores o iguales a 7 y cuántos menores. 
         */
        static void Main(string[] args)
        {
            List<int> notas = new List<int>();
            string linea;

            while (notas.Count < 10)
            {
                Console.WriteLine("Ingrese nota");
                linea = Console.ReadLine();
                int nota = Int32.Parse(linea);
                notas.Add(nota);
            }
            Console.WriteLine();
            Console.WriteLine("LISTA DE NOTAS");
            MostrarLista(notas);
            int totalMay=Mayores(notas);
            int totalMen=Menores(notas);

            Console.WriteLine();
            Console.WriteLine("Hay un total de " + totalMay + " de notas altas");
            Console.WriteLine("Hay un total de " + totalMen + " de notas bajas");
        }

        public static void MostrarLista(List<int> lista)
        {
            for(int i = 0; i < lista.Count; i++)
            {
                Console.WriteLine(lista[i]);
            }
        }

        public static int Menores(List<int> lista)
        {
            int menores=0;
            int i = 0;
            while (i < lista.Count)
            {
                if (lista[i] < 7)
                {
                    menores = menores + 1;
                }
                i++;
            }
            return menores;
        }

        public static int Mayores(List<int> lista)
        {
            int mayores=0;
            int i = 0;
            while (i < lista.Count)
            {
                if (lista[i] >= 7)
                {
                    mayores = mayores + 1;
                }
                i++;
            }

            return mayores;
        }
    }
}
