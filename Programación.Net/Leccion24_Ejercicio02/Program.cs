﻿using System;

namespace Leccion24_Ejercicio02
{
    /*
     * Confeccionar una clase para administrar los días que han faltado los 3 empleados de una empresa.
     * Definir un vector de 3 elementos de tipo string para cargar los nombres y una matriz irregular para cargar los días que han faltado cada empleado (cargar el número de día que faltó)
     * Cada fila de la matriz representan los días de cada empleado.
     * Mostrar los empleados con la cantidad de inasistencias.
     * Qué empleado faltó menos días. 
     */
    class Program
    {
        public string[] nombres;
        public int[][] diasFaltados;
        static void Main(string[] args)
        {
            Program matrizDias = new Program();

            matrizDias.CargarDatos();
            matrizDias.MostrarMatriz();
            matrizDias.EmpleadoFaltoMenos();
        }
        public void CargarDatos()
        {
            nombres = new string[3];
            diasFaltados = new int[3][];
            int n;
            for(int f = 0; f < nombres.Length; f++)
            {
                Console.WriteLine("Introduzca nombre:");
                nombres[f] = Console.ReadLine();
                Console.WriteLine("Introduzca la cantidad de días faltados:");
                n = Int32.Parse(Console.ReadLine());
                diasFaltados[f] = new int[n];
                for (int c = 0; c < n; c++)
                {
                    Console.WriteLine("Introduzca número de día:");
                    int dia = Int32.Parse(Console.ReadLine());
                    diasFaltados[f][c] = dia;
                }
            }
        }
        public void MostrarMatriz()
        {
            for(int f=0; f < nombres.Length; f++)
            {
                Console.Write(nombres[f]);
                for(int c=0; c < diasFaltados[f].Length; c++)
                {
                    Console.Write(" " + diasFaltados[f][c]);
                }
                Console.WriteLine();
            }
        }
        public void EmpleadoFaltoMenos()
        {
            int aux=0;
            int menos=10;
            string nombre="";
            for(int f=0; f < nombres.Length; f++)
            {
                for(int c = 0; c < diasFaltados[f].Length; c++)
                {
                    aux++;
                }
                if (aux < menos)
                {
                    menos = aux;
                    nombre = nombres[f];
                }
                aux = 0;
            }
            Console.WriteLine("El empleado que faltó menos días es: " + nombre);
        }
    }
}
