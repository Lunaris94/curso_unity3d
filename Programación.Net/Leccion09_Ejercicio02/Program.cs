﻿using System;

namespace Leccion09_Ejercicio02
{
    /*
     * Se ingresan un conjunto de n alturas de personas por teclado. Mostrar la altura promedio de las personas. 
     */
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("¿Cuántas alturas va a introducir?");
            string linea = Console.ReadLine();
            int n = Int32.Parse(linea);

            float suma = 0;
            int contador = 0;
            int i = 0;
            while (i < n)
            {
                Console.WriteLine("Ingrese altura");
                linea = Console.ReadLine();
                float altura = float.Parse(linea);

                contador = contador + 1;
                suma = suma + altura;
                i++;
            }
            Console.WriteLine("N:" + n + " Contador:" + contador);
            Console.WriteLine("El total es: " + suma);

            float media = suma / contador;
            Console.WriteLine("La media es: " + media);
        }
    }
}
