﻿using System;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;

namespace Clases_Ejemplos_Patri
{
    /*
     * Realizar un programa que imprima 25 términos de la serie 11 - 22 - 33 - 44, etc. (No se ingresan valores por teclado) 
     * PATRI
     */
    class CrearSerie
    {

        static void Main(string[] args)
        {
            Progresion nuevaSerie = new Progresion();
            //nuevaSerie.CargarDatos();
            //nuevaSerie.GenerarSerie();
            Progresion.GenerarSerieAuto(11, 25);

            nuevaSerie.x = 7;
            nuevaSerie.y = 3;
            nuevaSerie.GenerarSerie();

            nuevaSerie.x = 13;
            nuevaSerie.y = 9;
            nuevaSerie.GenerarSerie();

            nuevaSerie.x = Int32.Parse(Console.ReadLine());
            nuevaSerie.y = Int32.Parse(Console.ReadLine());
            nuevaSerie.GenerarSerie();

            Progresion secuenciasPares = new Progresion();
            secuenciasPares.x = 2;
            secuenciasPares.y = 5;
            secuenciasPares.GenerarSerie();
            secuenciasPares.x = 4;
            secuenciasPares.GenerarSerie();
            secuenciasPares.x = 6;
            secuenciasPares.GenerarSerie();
            secuenciasPares.x = 8;
            secuenciasPares.GenerarSerie();
        }
    }
}
