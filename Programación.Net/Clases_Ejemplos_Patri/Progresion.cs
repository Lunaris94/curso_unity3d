﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;

namespace Clases_Ejemplos_Patri
{
    class Progresion
    {
        public int x;
        public int y;
       /* public int ValorX
        {
            get { return x;}
            set { x = value; }
        }
        public int ValorY
        {
            get { return y; }
            set { x = value; }
        }*/


        public void CargarDatos()
        {
            Console.WriteLine("Introduzca el número que se va a multiplicar");
            x = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Introduzca cuantas veces se va a repetir el bucle");
            y = Int32.Parse(Console.ReadLine());
        }
        public void GenerarSerie()
        {
            Console.WriteLine("\n" + x + " veces " + y);
            for (int contador = 1; contador <= x; contador++)
            {
                Console.Write(contador * y + " - ");
            }
        }
        public static void GenerarSerieAuto(int x, int y)
        {
            Console.WriteLine("\n" + x + " veces " + y);
            for (int contador = 1; contador <= x; contador++)
            {
                Console.Write(contador * y + " - ");
            }
        }
    }
}
