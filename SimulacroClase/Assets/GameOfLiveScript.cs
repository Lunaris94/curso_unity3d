﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOfLiveScript : MonoBehaviour
{
    //PATRICIA
    //Imagen original o semilla enlazada en Unity
    public Texture2D planoSemilla;
    //Imagen generada por código
    public Texture2D planoSemilla_2;
    public Texture2D planoSemilla_3;
    //Plano en la escena 3D
    public GameObject quad;

    // Start is called before the first frame update
    void Start()
    {
        //Si tenemos una imagen enlazada
        if (planoSemilla != null)
        {
            planoSemilla.SetPixel(planoSemilla.width - 2, planoSemilla.height - 2, Color.white);

            planoSemilla.Apply();

            planoSemilla_2 = new Texture2D(planoSemilla.width, planoSemilla.height, TextureFormat.RGB24, false);
            planoSemilla_2.anisoLevel = 0;
            planoSemilla_2.alphaIsTransparency = false;
            planoSemilla_2.Apply();

            //Recorremos en bucle toda la imagen, de izq a der
            for (int x = 0; x < planoSemilla_2.width; x++)
            {
                //Y de arriba a abajo
                for (int y = 0; y < planoSemilla_2.height; y++)
                {
                    //Y la pintamos de negro
                    planoSemilla_2.SetPixel(x, y, Color.black);
                }
            }
            planoSemilla_2.Apply();

            quad.GetComponent<Renderer>().material.mainTexture = planoSemilla_2;
        }

        int v = CuantosVecinosVivos(planoSemilla, 0, 0);
        Debug.Log("Vivos: " + v);
    }

    // Update is called once per frame
    void Update()
    {
        //ahora, por cada frame, generamos una nueva imagen (planoSemilla_2)
        //que generamos a partir de plano_semilla, aplicando las reglas
        //del juego de la vida de J.H.Conway
        for (int x = 0; x < planoSemilla_2.width; x++)
        {
            //Y de arriba a abajo
            for (int y = 0; y < planoSemilla_2.height; y++)
            {

                //Y la pintamos de negro
                planoSemilla_2.SetPixel(x, y, Color.black);

                //Si miramos un pixel que tiene una célula no viva (negro)
                if (planoSemilla.GetPixel(x, y) == Color.black)
                {
                    //y si al rededor de ese pixel o célula nos encontramos
                    //con 3 pixels o células vivas (blancas)
                    if (CuantosVecinosVivos(planoSemilla, x, y) == 3)
                    {
                        //hacemos que el pixel en el que estabamos al principio
                        //reviva o nazca, de forma que nuestro pixel, que en un
                        //principio estaba negro, se transforme en blanco
                        planoSemilla_2.SetPixel(x, y, Color.white);
                    }
                }

                if (planoSemilla.GetPixel(x, y) == Color.white)
                {
                    if (CuantosVecinosVivos(planoSemilla, x, y) == 2 || CuantosVecinosVivos(planoSemilla, x, y) == 3)
                    {
                        //Mantenemos viva la célula
                        planoSemilla_2.SetPixel(x, y, Color.white);
                    }
                }
            }
        }
        planoSemilla_2.Apply();

        /*Si queremos añadir otro plano distinto donde guardarlo:
        Color[] pix = planoSemilla_2.GetPixels();
        planoSemilla_3 = new Texture2D(planoSemilla_2.width, planoSemilla_2.height, TextureFormat.RGB24, false);
        planoSemilla_3.SetPixels(pix);
        planoSemilla.Apply();*/

        //Esto se resume en:
        planoSemilla.SetPixels(planoSemilla_2.GetPixels());
        planoSemilla.Apply();
                
        /*También se podría volver a utilizar otro bucle for
          for (int x = 0; x < planoSemilla_2.width; x++)
        {
            for (int y = 0; y < planoSemilla_2.height; y++)
            {
                planoSemilla.SetPixel(x, y, plano_2.GetPixel(x,y));
            }
         
         */
    }

    //"px" e "py" son parámetros de la función, es decir, variables
    //locales que se reciben cuando se llama (invoca) a la función
    int CuantosVecinosVivos(Texture2D tex, int px, int py)
    {

        //tiene que ir desde el -1 al 1, osea, pasar, tanto
        //x como y por -1, 0, 1

        int cuentaVivos = 0;
        for (int x=px -1; x <= px +1; x++)
        {
            for (int y = py - 1; y <= py +1; y++)
            {
                //Preguntamos si el pixel está activo siempre y cuando
                //no sea el propio pixel recibido
                if (px != x || py != y)
                {
                    if (tex.GetPixel(x, y) == Color.white)
                    {
                        cuentaVivos = cuentaVivos + 1;
                    }
                }
            }
        }

        return cuentaVivos;
    }
}
