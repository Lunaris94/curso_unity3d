﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PruebaOrdenamiento : MonoBehaviour
{
    public int[] numeros;
    public int[] numOrdenados;
    // Start is called before the first frame update
    void Start()
    {
        numOrdenados = new int[numeros.Length];

        int masPeque = 90;
        int masGrande = 0;

        //1er PASO: encontrar el más pequeño y el más grande, solo lo queremos hacer una vez
        for (int i = 0; i < numeros.Length; i++)
        {
            if (numeros[i] > masGrande)
            {
                masGrande = numeros[i];
            }
        }

         for (int i = 0; i < numeros.Length; i++)
         {
             if (numeros[i]<masPeque)
             {
                 masPeque = numeros[i];
             }
         }
       
        numOrdenados[0] = masPeque;
        //2º PASO
        for (int j = 0; j < numeros.Length -1; j++)
        {
            //masPeque++;
            masPeque = 90;

            for (int i = 0; i < numeros.Length; i++)
            {   //Este if es para que pase el numero que hemos encontrado antes y no lo lea, pero esto 
                //no serviría si tenemos varios numeros iguales, es solo para cuando no se repiten
               if (numeros[i] > numOrdenados[j])
               {
                    if (numeros[i] < masPeque)
                    {
                        masPeque = numeros[i];
                        //TODO: Hay que comprobar si el numero está repetido, y en ese caso de estarlo
                        //repetirlo en el array final
                    }
               }
            }
            numOrdenados[j + 1] = masPeque;
            
        }

        //habría que hacer que entrara en otro bucle for, de forma que empiece el bucle justo 
        //después del numero que está comprobando, para ver si hay alguna repetición del
        //número más adelatnte, para que no repita todo el rato el mismo numero que está en la 
        //misma posición que hemos visto antes       
        
        /*
        for (int f = numeros[i + 1]; f < numeros.Length; f++)
        {
            if (numeros[f] == masPeque)
            {
                int mismoNumb = numeros[f];
            }
        }


                //esta es la forma en la que yo he solucionado lo del orden de los números
                //pero como es una estructura que se repite, podemos hacer que entre en un
                //bucle for justo cuando hemos encontrado el primer numero, que sería el menor
                //y cuando hemos encontrado también el numero mayor, que ese irá al final 
                //del nuevo array
                for (int i = 0; i < numeros.Length; i++)
                {
                    if (numeros[i] > numOrdenados[1])
                    {
                        if (numeros[i] == masPeque)
                        {
                            masPeque = numeros[i];
                        }
                    }


                    numOrdenados[2] = masPeque;
                }

                masPeque++;

                for (int i = 0; i < numeros.Length; i++)
                {
                    if (numeros[i] > numOrdenados[2])
                    {
                        if (numeros[i] == masPeque)
                        {
                            masPeque = numeros[i];
                        }
                    }


                    numOrdenados[3] = masPeque;
                }

                masPeque++;

                for (int i = 0; i < numeros.Length; i++)
                {
                    if (numeros[i] > numOrdenados[3])
                    {
                        if (numeros[i] == masPeque)
                        {
                            masPeque = numeros[i];
                        }
                    }


                    numOrdenados[4] = masPeque;
                }

                masPeque++;
                */
    }
}
