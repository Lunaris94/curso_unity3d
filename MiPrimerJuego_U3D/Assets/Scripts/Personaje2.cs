﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Personaje2 : MonoBehaviour
{
	public float velocidad = 40;

	void Update()
	{
		float velocidad;
		velocidad = 40;

		if (Input.GetKey(KeyCode.A))
		{
            Vector3 vectorMov = velocidad * Vector3.left * Time.deltaTime;
            this.GetComponent<Transform>().Translate(vectorMov);

			if (this.GetComponent<Transform>().position.x < -10)
			{
				this.GetComponent<Transform>().position = new Vector3(-10, 0, 0);
				Debug.Log("Choque a la izquierda");
			}
		}

		if (Input.GetKey(KeyCode.D))
        {
            Vector3 vectorMov = velocidad * Vector3.right * Time.deltaTime;

            this.GetComponent<Transform>().Translate(vectorMov);

            if (this.GetComponent<Transform>().position.x > 10)
            {
                this.GetComponent<Transform>().position = new Vector3(10, 0, 0);
                Debug.Log("Choque a la derecha");
            }
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            this.GetComponent<Transform>().Translate(Vector3.up * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            this.GetComponent<Transform>().Translate(Vector3.down * Time.deltaTime);
        }
    }
}
