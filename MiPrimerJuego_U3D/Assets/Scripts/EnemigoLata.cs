﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoLata : MonoBehaviour
{
    public float velocidad; // Por defecto, cero
    GameObject jugador; //Creamos aquí fuera la variable juagdor para que sea publica y tenga siempre acceso a ella

    //Start se llama a la primera vez, al primer frame

    void Start()
    {
        //cuando arranque queremos una posición x aleatoria
        float posInicioX = Random.Range(-10, 10);
        //le decimos cual es la posición actual de nuestro componente
        this.transform.position = new Vector3(posInicioX, 12, 1);
        //para que colisionen ambos objetos, lo primero es obtener el script del otro personaje, pero antes hay que indicarle el objeto juagdor
        
        //siempre ponemos que busque el GameObject en el void Start, porque si lo ponemos en update lo ralentizará
        jugador = GameObject.Find("Jugador-Caballito");
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 movAbajo = velocidad
            * new Vector3(0, -1, 0)
            * Time.deltaTime;

        //en este caso no es como si utilizaramos Translate, porque aquí se va a seguir desplazando
        //hacia abajo aunque rotemos la imagen del personaje, en cambio, con Trasnlate se va a 
        //mover en relación a esa rotación que añadimos
        this.GetComponent<Transform>().position = 
            this.GetComponent<Transform>().position
            + movAbajo;

        if (this.GetComponent<Transform>().position.y < 0)
        {
            //para que no se posicione ahora en la posición (0,0,1), tenemos que cambiarle 
            //la posición de las x, de forma que ahora habrá que ponerle la posición x 
            //random que nos da la variable de posInicioX
            this.GetComponent<Transform>().position = new Vector3(this.transform.position.x, 0, 1);

            //aquí añadimos la colisión de los dos objetos en una misma posición x, el unico problema es que 
            //tendrá que ser en la posición x concreta
            //if (this.transform.position.x == jugador.transform.position.x)
            if(this.transform.position.x>=jugador.transform.position.x-3.2f/2
                &&this.transform.position.x<= jugador.transform.position.x+3.2f/2)
                //3.2 porque son 32 pixeles entre 10 (porque tenemos puesto 10 pixeles por unidad) 
                //y es entre dos porque lo dividimos por la mitad (su centro se situa en mitad del cuadrado)
            {
               Destroy(this.gameObject);
            } else
            {
                /*GameObject.Find("Controlador-Juego").GetComponent<ControladorJuego>().vidas -= 1;*/
                GameObject.Find("Controlador-Juego").GetComponent<ControladorJuego>().CuandoPerdemosEnemigo();
                Destroy(this.gameObject);
            }

            if (this.transform.position.x >= jugador.transform.position.x - 3.2f / 2
                && this.transform.position.x <= jugador.transform.position.x + 3.2f / 2) 
            {
                /*GameObject.Find("Controlador-Juego").GetComponent<ControladorJuego>().puntos += 50;*/
                GameObject.Find("Controlador-Juego").GetComponent<ControladorJuego>().CuandoCapturamosEnemigo();
            }
        }
    }
}
