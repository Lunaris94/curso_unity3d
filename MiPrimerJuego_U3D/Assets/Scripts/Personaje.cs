﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Personaje: MonoBehaviour
{
        //float es un numero decimal
    public float velocidad = 40;    //40 es el valor de velocidad por defecto; publico significa que es el valor que siempre va a aparecer
   
    // Update is called once per frame 
    void Update()
    {
        float velocidad;  //float es un numero decimal; añadimos las dos líneas anteriores de código para darle esa velocidad general a nuestro personaje
        velocidad = 40;   //le damos una velocidad con un valor concreto 

        //para reducir o aumentar la velocidad de nuestro personaje escribimos estas dos lineas de codigo

        //Vector3 vectorMov = velocidad * Vector3.left * Time.deltaTime;  
        //this.GetComponent<Transform>().Translate(vectorMov);

        //en lugar del siguiente codigo

        //this.GetComponent<Transform>().Translate(Vector3.right*Time.deltaTime);

        //esto serviría para darle una velocidad distinta a cada botón, siempre de forma independiente

        //Cuando se pulsa la flecha izquierda
        if (Input.GetKey(KeyCode.LeftArrow))  //Si lo que preguntamos aquí es cierto, entonces ejecuta el this de debajo
        {
            //Creamos el vector de movimiento, a partir del cálculo que
            //influye la velocidad (40), el vector hacia la izquierda (-1,0,0): (-40,0,0)
            //Para que se mueva -40 unidades por segundo en vez de por cada frame,
            //multiplicamos por el incremento del tiempo (aprox. 0.02 seg, 50 FPS)  (0.8,0,0)
            Vector3 vectorMov = velocidad * Vector3.left * Time.deltaTime;  
                    //unid / seg * 1 * seg/frame

            this.GetComponent<Transform>().Translate(vectorMov);

            //si la posicion en el eje x es menor que -10 (respecto al margen izquierdo)
            if (this.GetComponent<Transform>().position.x < -10 )
            {
                //entonces recolocamos en el margen izquierdo
                this.GetComponent<Transform>().position = new Vector3(-10, 0, 0);
                Debug.Log("Choque a la izquierda");
            }
        }

      //Cuando se pulsa la flecha derecha

      if (Input.GetKey(KeyCode.RightArrow))
        {
            Vector3 vectorMov = velocidad * Vector3.right * Time.deltaTime;

            this.GetComponent<Transform>().Translate(vectorMov);

            if(this.GetComponent<Transform>().position.x > 10)
            {
                this.GetComponent<Transform>().position = new Vector3(10, 0, 0);
                Debug.Log("Choque a la derecha");
            }
        }

      if (Input.GetKey(KeyCode.UpArrow)) 
        {
            this.GetComponent<Transform>().Translate(Vector3.up*Time.deltaTime);
        } 

      if (Input.GetKey(KeyCode.DownArrow))
        {
            this.GetComponent<Transform>().Translate(Vector3.down*Time.deltaTime);
        }
    }
}
