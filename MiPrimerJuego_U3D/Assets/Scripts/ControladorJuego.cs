﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorJuego : MonoBehaviour
{
    public GameObject[] enemigos; //para crear el array en Unity
    public AparicionEnemigo[] aparicionesEnem;
    
    public int vidas = 7;
    public int puntos = 0;
    public int gameover;
    public GameObject textoVidas;
    public GameObject textoPuntos;
    public GameObject textoGameOver;

    private float timeIni;
    private int enemActual;
    private int puntosAnt; //puntos del frame anterior
    

    // Start is called before the first frame update
    void Start()
    {

        timeIni = Time.time;
        enemActual = 0;
        /*el codigo instantiate sirve para crear un nuevo objeto, en estos casos, latas y botellas
        Aprox. mitad de probabilidades
        this.InstaciarEnemigo();*/
    }

    // Update is called once per frame
    void Update()
    {
        textoVidas.GetComponent<UnityEngine.UI.Text>().text = "Vidas: " + this.vidas;

        textoPuntos.GetComponent<UnityEngine.UI.Text>().text = "Puntos: " + this.puntos;
        /*podemos utilizar los puntos como una forma de detectar que hemos acabado con el enemigo y 
        así nos puede salir otro enemigo nuevo
        necesitamos guardar los puntos del frame anterior, para saber si hay modificaciones
        y así poder lanzar un nuevo enemigo o no

        if(this.puntos != this.puntosAnt) 
        { 
            this.InstaciarEnemigo();
        }
        //Con esto anterior podriamos hacer que salga un enemigo mientras tu hayas ganado puntos en el frame anterior
        this.puntosAnt = this.puntos;*/

        //Para saber si un enem tiene que aparecer, tenemos que saber cuanto
        //tiempo ha psado desde el inicio del nivel hasta el momento actual
        //(frame actual) y si es superior al tiempo configurado en la aparicion
        float tiempoActual = Time.time - timeIni;

        if (tiempoActual > aparicionesEnem[enemActual].tiempoInicio)
        {
            //si NO ha aparecido...
            if (! aparicionesEnem[enemActual].yaHaAparecido)
            {
                this.InstaciarEnemigo();
                aparicionesEnem[enemActual].yaHaAparecido = true;
                enemActual++;
            }
        }
    }

    public void CuandoCapturamosEnemigo() 
    {
        //Estamos asignando un nuevo valor a la puntuación,
        //que es los puntos actuales +10/+50 ptos.
        this.puntos = this.puntos + 50;        //formula reducida: this.puntos+=10;
        //this.InstaciarEnemigo();
    }

    //Esto de debajo es un nuevo método 
    //pudiendo ser cualquiera de los siguientes: 
    //acción, conjunto de instrucciones, función, procedimiento, mensaje...
    public void CuandoPerdemosEnemigo() 
    {
        this.vidas = this.vidas - 1;    //this.vidas-=1;    this.vidas--;
        //this.InstaciarEnemigo();
    }
   
    /*public void InstaciarEnemigo()
    {
        this.vidas=this.vidas
        GameObject.Instantiate(prefabLata);
    }*/ //Este sería el método para crear un enemigo si siguieramos la orden que tenemos más arriba en verde

    public void InstaciarEnemigo() 
    {
        int numEnemigo = Random.Range(0, enemigos.Length);
        
        GameObject.Instantiate(enemigos[numEnemigo]);

    }

   /* public void CuandoVidasAcaban () 
    { 
        if (this.vidas == this.vidas -7) 
        {
            textoGameOver.GetComponent<UnityEngine.UI.Text>().text = "GAME OVER";
        }
    }*/
}
