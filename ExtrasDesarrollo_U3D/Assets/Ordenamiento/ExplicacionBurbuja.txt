Tenemos una lista de numeros que queremos ordenar usando el método de la burbuja
1-Primero tenemos que comparar el primer numero de la lista con el siguiente 
numero de la lista (lista[0] y lista[1])
2-Si el segundo numero es menor que el primer numero, se hace un intercambio
en sus posiciones, de forma que el primero pasar a ser el segundo, y el segundo
pasa a estar en la primera posición (si lista[1]<lista[0] - lista[0] pasaría a 
lista[1] y lista [1] pasaría a lista[0])
3-Una vez hemos hecho esto con los des primeros numeros, pasamos a comparar de la
misma forma el segundo numero (que ya se ha comparado) con el tercero (lista[2]<lista[1])
y en el caso de que se cumpla esta posición se intercambiarían sus posiciones como
en el caso anterior, si no, se quedan en sus porsiciones iniciales.
4-Esto tiene que ocurrir hasta el final de la lista, además de repetirsde tantas veces
hasta que la lista esté ordenada y todos los casos den false.

DESORDENAR LA LISTA
1-Tenemos una lista con los números ordenados y queremos desordenarla
2-Lo primero que queremos hacer es que tome uno de los números de la lista al azar y
lo coloque en el primer lugar de la lista
3-Después queremos que tome otro número al azar, pero sin que pueda tomar el primer
numero que hemos colocado antes, y así sucesivamente
4-Generando así una nueva lista con los números desordenados

-Hago un bucle para que pase por todos los numeros de la lista
-En la primera pasada, le digo que me de un número aleatorio, relacionado con la cantidad
de posiciones o index que hay en mi lista, por lo que me da una posición aleatoria
-Esa posición aleatoria hace referencia a un número, por lo que ya tenemos el número 
aleatorio localizado.
-Después le decimos que ese número aleatorio de esa posición lo elimine de la lista principal
-Finalmente le decimos que nos 