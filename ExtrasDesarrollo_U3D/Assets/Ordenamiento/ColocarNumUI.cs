﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColocarNumUI : MonoBehaviour
{
    public List<Text> numeros;
    List<int> listaNum;
    List<int> listaNumDes;
    int menorDeTodos;
    public Vector2 vPosInicial;

    const int ANCHO_TEXTO = 100;

    void Start()
    {
        listaNum = new List<int>();
        foreach (Text txtNum in numeros )   //por cada uno de los  elementos de la lista "numeros" crea un texto
        {
            int num = Int32.Parse(txtNum.text);
            listaNum.Add(num);
        }
        menorDeTodos = OrdenarComoHumano.BuscarMenor(listaNum);
        //listaNum = OrdenarComoHumano.Ordenar(listaNum);
        listaNum = OrdenarComoHumano.OrdenarBurbuja(listaNum);
        //listaNum = OrdenarComoHumano.OrdenarInsert(listaNum);
        listaNumDes = OrdenarComoHumano.Desordenar(listaNum);
        //listaNumDes = OrdenarComoHumano.DesordenarDos(listaNum);

        //PREGUNTA: CUANDO ORDENO Y DESPUÉS DESORDENO, SOLO ME APARECE LA LISTA DESORDENADA EN UNITY????

        //ColocarSeguidos();
        ColocarSeguidos2();
    }

    public void ColocarSeguidos()   //COLOCACIÓN EQUIDISTANTE
    {
        this.vPosInicial = this.numeros[0].GetComponent<RectTransform>().localPosition;

        /*Vector2 nuevaPos2 = new Vector2(this.vPosInicial.x + ANCHO_TEXTO, this.vPosInicial.y);
        this.numeros[1].GetComponent<RectTransform>().localPosition = nuevaPos2;

        Vector2 nuevaPos3 = new Vector2(nuevaPos2.x + ANCHO_TEXTO, this.vPosInicial.y);
        this.numeros[2].GetComponent<RectTransform>().localPosition = nuevaPos3;

        Vector2 nuevaPos4 = new Vector2(nuevaPos3.x + ANCHO_TEXTO, this.vPosInicial.y);
        this.numeros[3].GetComponent<RectTransform>().localPosition = nuevaPos4;*/

        for (int i = 1; i < this.numeros.Count; i++)
        {
            float nuevaPosX = this.numeros[i - 1].rectTransform.localPosition.x + ANCHO_TEXTO;
            float nuevaPosY = this.vPosInicial.y;
            Vector2 nuevaPos = new Vector2(nuevaPosX, nuevaPosY);
            this.numeros[i].rectTransform.localPosition = nuevaPos;
        }
    }

    public void ColocarSeguidos2() //COLOCACIÓN SEGÚN LOS ANCHOS
    {
        /*this.vPosInicial = this.numeros[0].GetComponent<RectTransform>().localPosition;

        float anchoTexto1 = (this.numeros[0].rectTransform.rect.width)/2;
        float anchoTexto2 = (this.numeros[1].rectTransform.rect.width)/2;
        float anchoTextoTotal = anchoTexto1 + anchoTexto2;
        float nuevaPosx = this.vPosInicial.x + anchoTextoTotal;
        float nuevaPosY = this.vPosInicial.y;
        Vector2 nuevaPos = new Vector2(nuevaPosx, nuevaPosY);
        this.numeros[1].rectTransform.localPosition = nuevaPos;
        */

        for (int i = 1; i < this.numeros.Count; i++)
        {
            this.vPosInicial = this.numeros[i-1].GetComponent<RectTransform>().localPosition;

            float anchoTexto1 = (this.numeros[i-1].rectTransform.rect.width)/2;
            float anchoTexto2 = (this.numeros[i].rectTransform.rect.width)/2;
            float anchoTextoTotal = anchoTexto1 + anchoTexto2;

            float nuevaPosx = this.vPosInicial.x + anchoTextoTotal;
            float nuevaPosY = this.vPosInicial.y;
            //Debug.Log("El ancho del primer texto es: " + anchoTexto1);
            Vector2 nuevaPos = new Vector2(nuevaPosx, nuevaPosY);
            this.numeros[i].rectTransform.localPosition = nuevaPos;

        }

    }

    /*public void ColocarSeguidos() //PRUEBA
    {
        List<int> listaPosX = new List<int>();

        foreach (Text txtNum in this.numeros)
        {
            int posX = (int) txtNum.rectTransform.position.x; //el (int) es para que realice una conversión de float a int 
            listaPosX.Add(posX);
        }

        int posMenorX = OrdenarComoHumano.BuscarMenor(listaPosX);
        //this.vPosInicial = new Vector2(posMenorX, this.numeros[0].rectTransform.position.y);
        this.vPosInicial = new Vector2(posMenorX, this.numeros[0].rectTransform.rect.position.y);
        //rectTransform hace referencia al inspector, y despues el rect hace referencia al marco donde se situa el texto o el numero

        //this.numeros[0].rectTransform.position = this.vPosInicial;
        this.numeros[0].rectTransform.localPosition = this.vPosInicial;


        for (int i = 1; i < this.numeros.Count; i++)
        {
            int posSiguienteX = posMenorX + ANCHO_TEXTO;
            this.vPosSiguiente = new Vector2(posSiguienteX, this.numeros[i].rectTransform.position.y);

        }
    }*/
}
