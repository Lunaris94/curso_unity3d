﻿using System;
using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using System.Security.Cryptography;

public class OrdenarComoHumano
{
    // Controlar qué entra y qué sale en las funciones

	public static void Main()
	{
		List<int> numeros = new List<int>();
		numeros.Add(4);		// numeros[0] = 4
		numeros.Add(5);
		numeros.Add(70);
		numeros.Add(-2);
        numeros.Add(60);
        numeros.Add(-3);
        numeros.Add(2);
		numeros.Add(10);
        numeros.Add(20);

        Debug.Log("La lista es: " + numeros);
		MostrarLista(numeros);
        //BuscarMenor(numeros);
        //Ordenar(numeros);
        //MostrarLista(numOrdenados);
        List <int> numOrd = Ordenar(numeros);
        MostrarLista(numOrd);

    }
    public static List<int> Ordenar(List<int> lista)
    {
        //TODO: Ordenar, buscando el más pequeño, sacándolo y poniendo la lista ordenada

        int menor = 0;
        List<int> numOrdenados = new List<int>();

        /* menor = BuscarMenor(lista);
         numOrdenados.Add(menor);
         MostrarLista(numOrdenados);
         lista.Remove(menor);
         MostrarLista(lista);*/

        while (0 < lista.Count)
        {
            Debug.Log("El tamaño de la lista es de: " + lista.Count);
            menor = BuscarMenor(lista);
            numOrdenados.Add(menor);
            MostrarLista(numOrdenados);
            lista.Remove(menor);
            MostrarLista(lista);

            //TODO:guardar lista.Count actualizado

            Debug.Log("FIN DE LA ORDENACIÓN");

        }
        MostrarLista(numOrdenados);
        return numOrdenados;
    }
    public static List<int> OrdenarBurbuja(List<int> lista)
    {
        for (int j =1; j < lista.Count;j++)
        {
            for (int i = 0; i < lista.Count; i++)
            {
                //Debug.Log("Valor de j: " + lista[j] + " j: " + j);
                //Debug.Log("Valor de i: " + lista[i] + " i: " + i);

                Debug.Log("Empieza la comparación");

                if (lista[j] < lista[i])
                {
                    int aux = lista[j];
                    lista[j] = lista[i];
                    lista[i] = aux;
                    //Debug.Log("Acaba de compararse, j ahora vale: " + lista[j] + " e i vale " + lista[i]);
                }
                Debug.Log("Termina la comparación");
                //Debug.Log("Valor de j: " + lista[j]);
                //Debug.Log("Valor de i: " + lista[i]);
            }
        }
        MostrarLista(lista);
        return lista;
    }
    public static List<int> OrdenarInsert(List<int> lista)
    {
        
        for (int j = 1; j < lista.Count; j++)
        {
            for (int i = 0; i < lista.Count; i++)
            {
                //Debug.Log("Valor de j: " + lista[j] + " j: " + j);
                //Debug.Log("Valor de i: " + lista[i] + " i: " + i);

                //Debug.Log("Empieza la comparación");

                if (lista[j] < lista[i])
                {
                    int aux = lista[j];
                    lista.RemoveAt(j);
                    lista.Insert(i, aux);
                    //Debug.Log("Acaba de compararse, j ahora vale: " + lista[j] + " e i vale " + lista[i]);
                    //MostrarLista(lista);
                }
                //Debug.Log("Termina la comparación");
                //Debug.Log("Valor de j: " + lista[j]);
                //Debug.Log("Valor de i: " + lista[i]);   
            }
        }
        MostrarLista(lista);
        return lista;
    }
    public static List<int> Desordenar(List<int> lista)
    {
        List<int> listaNumDes = new List<int>(); 

        while (0 < lista.Count)
        { 
            int posAleatoria;
            posAleatoria = UnityEngine.Random.Range(0, lista.Count);
            int numAleatorio = lista[posAleatoria];
            Debug.Log("El número aleatorio es: " + numAleatorio);

            listaNumDes.Add(numAleatorio);
            Debug.Log("El tamaño de la lista desordenada es de: " + listaNumDes.Count);
            MostrarLista(listaNumDes);

            lista.Remove(lista[posAleatoria]);
            Debug.Log("El tamaño de la lista ordenada es de: " + lista.Count);
            MostrarLista(lista);
        }
        return listaNumDes;
    }
    public static List<int> DesordenarDos(List<int> lista)
    {
        //List<int> listaNumDes = new List<int>();

        for (int i = 0; i < lista.Count; i++)
        {
            int posAleatoria;
            posAleatoria = UnityEngine.Random.Range(i, lista.Count);
            int numAleatorio = lista[posAleatoria];
            Debug.Log("El número aleatorio es: " + numAleatorio);

            lista.Insert(i, numAleatorio);
            //Debug.Log("El tamaño de la lista desordenada es de: " + listaNumDes.Count);
            //MostrarLista(listaNumDes);

            lista.Remove(numAleatorio);
            //Debug.Log("El tamaño de la lista ordenada es de: " + lista.Count);
            MostrarLista(lista);
        }
        return lista;
    }

    public static void MostrarLista(List<int> lista)
    {
        Debug.Log("Lista: ");
		for (int i = 0; i < lista.Count; i++)
        {
            Debug.Log(lista[i]);	// numeros[3]
        }
        Debug.Log("  FIN");
    }

    public static int BuscarMenor(List<int> lista)
    {

        int menor = lista[0];
        for (int i = 0; i < lista.Count; i++)
        {
            if (lista[i] < menor)
            {
                menor = lista[i];
            }
        }
        Debug.Log("Número menor: " + menor);
        return menor;
    }

}
