# curso_unity3d

Códigos fuente de proyectos, prototipos, ejercicios de Unity3D. C#, Visual Studio, Paint, Gimp y Blender.

Aquí explicamos el setup del proyecto. Pasos: 
1-Instala Unity.
2-Abre el project: MiPrimerJuego_U3D.
3-Dale al play.
4-Dale al stop.

Comandos para el flujo de trabajo:
-git --help
-git clone   https://gitlab.com/Lunaris94/curso_unity3d.git  \\esta dirección es la que tenemos subida en gitlab, es decir, la copia que tenemos subida en el servidor, por eso siempre que abramos en un nuevo ordenador tenemos que indicarle esta dirección
-git add .  \\si añadimos un punto significa que vamos a subir todos los archivos, si no tendríamos que añadir concretamente los que queremos subir
-git commit  \\con este comando aceptamos el fichero o los archivos que hemos modificado, pero aún no estaría subido a gitlab
-git push  \\con este fichero sí que terminamos de subir los ficheros que hemos seleccionado antes que estaban modificados, por lo que despues de este comando ya estarían subidos al gitlab
