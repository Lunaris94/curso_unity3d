﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.ConstrainedExecution;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GermanSTS
{
    public class ControladorEscenas : MonoBehaviour
    {
        [Header("Porpiedades ctrl escenas:")]
        public string nombreEscena;

        private void Start()
        {
            if (nombreEscena != "") //Si cuando se inicia el nombre de escena es diferente a vacío
            {
                this.CargarEscena(nombreEscena); //Carga la escena de manera automática
            }
            //Funcionaría el vincular el método CerrarAplicacion si no fuera porque está desactivado en la escena al arrancar
            //GameObject botonSalir=GameObject.Find("Boton_Salir_Si");
            //botonSalir.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(this.CerrarAplicacion);

            AsignarCerrarAlBotonSalir();
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                CerrarAplicacion();
            }
        }
        public void CargarEscena(string escena)
        {
            SceneManager.LoadScene(escena);
        }
        //Vamos a centralizar la funcionalidad aquí de cuando se cierre la aplicacion
        public void CerrarAplicacion()
        {
            Application.Quit();
        }
        void AsignarCerrarAlBotonSalir()
        {
            GameObject objCanvas = GameObject.Find("Canvas");
            if (objCanvas != null)
            {
                Transform canvas = GameObject.Find("Canvas").GetComponent<Transform>();
                Transform botonSalirSi=null;
                FindByNameInactives(canvas, "Boton_Salir_Si", ref botonSalirSi);
                if (botonSalirSi != null)
                {
                    botonSalirSi.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(this.CerrarAplicacion);
                }
            }
        }
        
        void FindByNameInactives(Transform raiz, string nombre, ref Transform objetoEncontrado)
        {
            for (int i = 0; i < raiz.childCount; i++)
            {
                Transform objTrnasform = raiz.GetChild(i);
                if (objTrnasform.name == nombre)
                {
                    objetoEncontrado = objTrnasform;
                    return;
                }
                else
                {
                    FindByNameInactives(objTrnasform, nombre, ref objetoEncontrado);
                }
            }
        }
    }
}
