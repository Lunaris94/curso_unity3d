﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorBoton : MonoBehaviour
{
    public int direccion=0;
    public GameObject jugador;
    public bool pulsando;

    // Update is called once per frame
    void Update()
    {
        if (pulsando)
        {
            jugador.GetComponent<MovimientoJugador>().Mover(direccion);
            if (direccion>0)
            {
                jugador.GetComponent<AnimacionJugador>().HaciaLaDerecha();
            }else if (direccion<0)
            {
                jugador.GetComponent<AnimacionJugador>().HaciaLaIzquierda();
            }
        }
    }
    private void OnMouseDown()
    {
        pulsando = true;
    }
    private void OnMouseUp()
    {
        pulsando = false;
    }
}
