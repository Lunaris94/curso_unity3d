﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorCamara : MonoBehaviour
{
    public GameObject jugador;
    public Transform limiteCamIzq;
    public Transform limiteCamDer;
    public void Start()
    {
        //Find cuesta trabajo: Sólo usar una vez por objeto y por script, es decir, nunca usarlo en el Update (o usarlo solo una vez), porque recorre todos los objetos de la escena y va buscando por nombre
        //Sólo encuentra los GameObject activos.
        jugador = GameObject.Find("Jugador");
        limiteCamDer = GameObject.Find("Limite_Der").transform;
        limiteCamIzq = GameObject.Find("Limite_Izq").GetComponent<Transform>();

    }
    public void Update()
    {
        Vector3 posCam = this.transform.position;
        //posCam.x = this.jugador.transform.position.x;

        //Lerp hace que se acerque poco a poco al límite que le hemos delimitado, cada vez un 0.05 (5%) de lo que le queda por recorrer hasta el límite
        float x = Mathf.Lerp(this.transform.position.x, jugador.transform.position.x, 0.05f);
        //x=this.transform.position.x + (jugador.transform.position.x - this.transform.position.x) * 5%
        posCam.x = x;

        if (posCam.x > limiteCamDer.position.x)
        {
            posCam.x = limiteCamDer.position.x;
        }
        if (posCam.x < limiteCamIzq.position.x)
        {
            posCam.x = limiteCamIzq.position.x;
        }
        this.transform.position = posCam;
    }
}
