﻿using UnityEngine;
using System.Collections;
using JetBrains.Annotations;
using UnityEditor;

public class ControladorJuego: MonoBehaviour
{
    [Header("Vidas:")]
    public int vidas = 7;
    [Header("Prefabs y obj juego:")]
    public GameObject[] prefabsEnemigos;
    //public GameObject prefabEnemigo;
    public GameObject esqSuperior;
    [Header("Objetos UI:")]
    public GameObject panelFinJuego;
    public GameObject panelGanador;
    public GameObject panelPerdedor;
    public UnityEngine.UI.Text textoVidas;
    public UnityEngine.UI.Text textoPuntos;
    [Header("Listado de enemigos:")]


    public AparicionEnemigo[] apariciones;
    int enemigoActual;
    float tiempoInicio;
    int puntos = 0;

    // Use this for initialization
    void Start()
    {
        textoPuntos.text = "" + this.puntos;
        textoVidas.text = "" + this.vidas;
        /*Unity se encarga de hacer esto en el Inspector, lo pone a [0] por defecto y nosotros podemos cambiarlo para que salgan todos los enemigos que queramos
         * this.apariciones = new AparicionEnemigo[3];
        this.apariciones[0] = new AparicionEnemigo(2, 1);
        this.apariciones[1] = new AparicionEnemigo(-3, 6);
        this.apariciones[2] = new AparicionEnemigo(7, 10);*/
        enemigoActual = 0;
        tiempoInicio = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        //int numRandom = Random.Range(0,prefabsEnemigos.Length);
        float tiempoActual = Time.time - tiempoInicio;
        if(enemigoActual < apariciones.Length)
        {
            //Si  el tiempo que ha pasado es mayor que cuando se supone que debe aparecer el enemigo actual, entonces...
            if(tiempoActual > apariciones[enemigoActual].tiempoInicio)
            {
                int indiceEnemigo = apariciones[enemigoActual].indiceEnemigo;
                //Instanciamos el enemigo
                GameObject enemigo = GameObject.Instantiate(prefabsEnemigos[indiceEnemigo]);
                float posX = apariciones[enemigoActual].posInicioX;
                enemigo.transform.position = new Vector3(posX, esqSuperior.transform.position.y, 0);

                enemigo.GetComponent<Enemigo>().controlJuego = this;
                enemigo.GetComponent<Enemigo>().numEnemigo = enemigoActual;
                enemigoActual++;

            }
        }
    }
    public void SumarPuntos(int puntos, int numEnem)
    {
        this.puntos += puntos;
        //Mostramos puntos
        textoPuntos.text = "" + this.puntos;
        ComprobarFinJuego(numEnem);
    }

    public void RestarVida(int numEnem)
    {
        //Restar vidas
        this.vidas--;
        //Y si hemos perdido, mostrar el panel correspondiente
        ComprobarFinJuego(numEnem);
        //Mostramos vidas
        textoVidas.text = "" + this.vidas;
    }
    private void ComprobarFinJuego(int numEnem)
    {
        if (this.vidas <= 0)
        {
            panelFinJuego.SetActive(true);
            panelPerdedor.SetActive(true);
            panelFinJuego.SetActive(true);
            this.enabled = false;
            this.vidas = 0;
        }else if (numEnem == this.apariciones.Length - 1)
        {
            panelFinJuego.SetActive(true);
            panelGanador.SetActive(true);
            panelFinJuego.SetActive(true);
            this.enabled = false;
        }
    }
}
