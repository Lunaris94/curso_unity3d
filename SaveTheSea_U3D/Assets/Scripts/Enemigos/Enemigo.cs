﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo : MonoBehaviour
{
    public ControladorJuego controlJuego;
    public float velocidad;
    public int numEnemigo = 0;
    //Dos maneras de enlazar objetos:
    //private ControladorJuego ctrlJuego;    //
    private GameObject jugador;
    public AudioClip audioChocarSuelo;
    AudioSource audioCogerLata;
    //Audioclip toma el archivo directamente, mientras que el audiosource busca el componente donde está el archivo de audio

    // Start is called before the first frame update
    void Start()
    {
        //Start sería el "sustituto" del constructor 
        jugador = GameObject.Find("Jugador");
        audioCogerLata = GameObject.Find("tragar_lata").GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 movAbajo = velocidad * new Vector3(0, -1, 0) * Time.deltaTime;

        this.GetComponent<Transform>().position += movAbajo;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (this.enabled)
        {
            Debug.Log("Enemigo colisionado con " + collision.gameObject.name);
            if (collision.gameObject.name.ToLower().Contains("final"))
            {
                //el script se deshabilita
                this.enabled = false;
                this.GetComponent<Animator>().speed = 0;
                AudioSource.PlayClipAtPoint(audioChocarSuelo, Vector3.zero);
                controlJuego.RestarVida(this.numEnemigo);
            }
            if (collision.gameObject.name.ToLower().Contains("cuerpo"))
            {
                audioCogerLata.Play();
                Destroy(this.gameObject);
                controlJuego.SumarPuntos(100,this.numEnemigo);
            }
        }
        
    }
}
