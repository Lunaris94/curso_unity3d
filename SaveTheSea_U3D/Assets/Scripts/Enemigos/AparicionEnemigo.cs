﻿
[System.Serializable]
public class AparicionEnemigo
{
    //Que enemigo vamos a instanciar
    public float posInicioX = 0;
    public float tiempoInicio;
    public int indiceEnemigo;

    public AparicionEnemigo(float px, float ti)
    {
        this.posInicioX = px;
        this.tiempoInicio = ti;
    }
}
