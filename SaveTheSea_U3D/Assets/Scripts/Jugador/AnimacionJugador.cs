﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimacionJugador : MonoBehaviour
{

    //Cambiar la escala en el eje X a +1 ó -1
    public Transform trnasfSpritesCaballito;
    //Cambiar el parámetro dirección
    public Animator animCtrlCuerpo;
    //Cambiar la velocidad
    public Animator animCtrlAlas;
    //Cambiar el clip de animación Legacy (antigua, heredada)
    public Animation animCabeza;

    public void HaciaLaDerecha()
    {
        animCtrlCuerpo.SetInteger("direccion", +1);
        trnasfSpritesCaballito.localScale = new Vector3(1, 1, 1);
        animCabeza.CrossFade("Nadar_Cabeza");
        //Play cambia bruscamente una animacion y CrossFade elimina esa brusquedad
        animCtrlAlas.speed = 3;
    }
    public void HaciaLaIzquierda()
    {
        animCtrlCuerpo.SetInteger("direccion", -1);
        trnasfSpritesCaballito.localScale = new Vector3(-1, 1, 1);
        animCabeza.CrossFade("Nadar_Cabeza");
        animCtrlAlas.speed = 3;
    }
    public void SinMovimiento()
    {
        animCtrlCuerpo.SetInteger("direccion", 0);
        animCabeza.CrossFade("Idle_Cabeza");
        animCtrlAlas.speed = 1;
        //speeed lo que hace es multiplicar la velociadad, es un multiplicador
    }
}
