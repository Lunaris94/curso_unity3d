﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class MovimientoJugador : MonoBehaviour
{
    public float velocidad = 0.05f;
    public Transform esqIzq;
    public Transform esqDer;



    /*Si dirección vale
     * +1   mueve derecha
     * -1   mueve izquierda
     * 0    no se mueve */

    /* private void Update()
     {
         Vector3 posicion = GetComponent<Transform>().position;
         posicion.x = velocidad * Time.time;
         transform.position = posicion;
     } si queremos que se mueva solo de forma constante
     */
    public void Mover(int direccion)
    {
        /*Para poder escoger el espacio, tenemos que pensar en el vector de Unity que vamos a utilizar para que nuestro jugador
        se mueva, en este caso queremos que sea en el eje de las x, por eso primero creamos una nueva variable en la que determinamos
        el movimiento en el eje de las x, cogiendolo directamente del transform de Unity mediante la funcion de GetComponent<>().

        Vector3 posicion = GetComponent<Transform>().position;
        posicion.x = posicion.x + velocidad * Time.deltaTime; //* Time.time;
        //Con ésto, si le añadimos el tiempo, lo que vamos a conseguir es una aceleración
        //ahora le tenemos que decir que cambie la posicion por la posicion.x
        transform.position = posicion;*/


        Vector3 posicion = GetComponent<Transform>().position;

        posicion.x = posicion.x + direccion * velocidad * Time.deltaTime;

        if (posicion.x > esqDer.position.x)
        {
            //transform.position= esqDer.position;
            posicion.x = esqDer.position.x;
        }
        if (posicion.x < esqIzq.position.x)
        {
            //transform.position= esqIzq.position;
            posicion.x = esqIzq.position.x;
        }
        transform.position = posicion;

        //Camera.main.transform.position.x = posicion.x;
        //Camera.main.transform.position = posicion;




        //El tiempo en Unity es Time.time
        /*Debug.Log(
            "Time = " + Time.time *1000 + " , dT = " + Time.deltaTime *1000 + ", X = " + posicion.x);

        EmularRetrasoCPU_GPU();*/
        //Así llamamos a un método privado que hemos desarrollado más abajo
    }
    private void EmularRetrasoCPU_GPU()
    {
        int vueltas = Random.Range(1, 30000000);
        for (int v = 0; v < vueltas; v++)
        {
            v = v * 1 / 1;
            //Le hacemos que recorra un bucle for solo para generar un retraso
        }
    }

}
