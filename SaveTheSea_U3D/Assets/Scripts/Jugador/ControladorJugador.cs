﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorJugador : MonoBehaviour
{

    public AnimacionJugador animJug;

    public void Start()
    {
        animJug = GetComponent<AnimacionJugador>();
    }
    // Update is called once per frame
    void Update()
    {

        //animCtrlCuerpo.SetInteger("direccion", 0);
        //Si se pulsa la flecha derecha...
        if (Input.GetKey(KeyCode.RightArrow))
        {
            //Invocamos al movimiento que está en el otro script
            GetComponent<MovimientoJugador>().Mover(1);
            //Invocamos al método que está en el otro script
            this.animJug.HaciaLaDerecha();
        }
        //Si se pulsa la flecha izquierda...
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            GetComponent<MovimientoJugador>().Mover(-1);
            this.animJug.HaciaLaIzquierda();

        }
        else //Cuando no se pulsa ninguna tecla, paramos y ponemos Idle
        {
            this.animJug.SinMovimiento();
        }
        
    }

}
